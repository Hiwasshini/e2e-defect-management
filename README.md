# E2E Defect Management API Project

## Brief Description
1. The aim is to provide a set of data service to manage and retrieve the data store for defects found during the aircraft maintenance process.
	
2. Things to improve/to be implemented
	- Forget Password API
	- Change Password
	- Complete suite of API
	- An upload file (i.e. photo, PDF, drawings etc) feature for Inspection Finding

	
3. Folder structure
	- resources
		- contain the API documentation, SQL scripts, API restpoint test kit
	- documentation

4. How to run this project?
	- Development
		- VS Code terminal(first time run): npm install
		- VS Code terminal: npm run dev
	- Staging
		- AWS Lambda: set environment variables
			- NODE_ENV = staging
			- TZ = Asia/Singapore
	- Production
		- AWS Lambda: set environment variables
			- NODE_ENV = prod
			- TZ = Asia/Singapore

5. Other matters?
	- Sequelize
		- A customize code is added to node_modules\sequelize\lib\dialects\abstract\query-generator.js to allow existing rows to retain the existing values if there is no value supplied to the column key for the updates
		- Line 310: const valueKeys = options.updateOnDuplicate.map(attr => `${this.quoteIdentifier(attr)}=IF(VALUES(${this.quoteIdentifier(attr)}) IS NOT NULL,VALUES(${this.quoteIdentifier(attr)}), ${this.quoteIdentifier(attr)})`);//Willy modified code		

6. To be completed
	- Inspection Area
		- The troubleshooting needs to be done @ addUpdateInspectionFinding (Other addUpdate functions are working)
		- Need to work on GET and Remove functions
	- Inspection Finding 
		- Need to work on GET function
	- Inspection Area Finding Type
		- Need to double-check on GET function
			