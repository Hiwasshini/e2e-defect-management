"use strict";

module.exports = function(sequelize, DataTypes) {

    //for table specific values like data length, not null, unique values, pls use MySQL Workbench to view the values
    var e2e_inspection_request = sequelize.define('E2E_Inspection_Request', {
        ID: { field: 'ID', type: DataTypes.INTEGER, notEmpty: true, primaryKey: true,autoIncrement: true},
        Program : {field: 'Program',type: DataTypes.STRING(100),notEmpty: true, unique:false},
        Aircraft_Type : {field: 'Aircraft_Type',type: DataTypes.STRING(100),notEmpty: true, unique:false},
        Applicable_MSN_Range : {field: 'Applicable_MSN_Range',type: DataTypes.STRING(100), default: 'all', notEmpty: true, unique:false},
        Priority : {field: 'Priority',type: DataTypes.STRING(100), notEmpty: false, unique:false},
        Stress_Leader : {field: 'Stress_Leader',type: DataTypes.STRING(100), notEmpty: false, unique:false},
        NDI_compiler : {field: 'NDI_compiler',type: DataTypes.STRING(100), notEmpty: false, unique:false},
        Test_Leader : {field: 'Test_Leader',type: DataTypes.STRING(100), notEmpty: false, unique:false},
        Requestor : {field: 'Requestor',type: DataTypes.STRING(100), notEmpty: false, unique:false},
        NDT_Story_ID : {field: 'NDT_Story_ID',type: DataTypes.INTEGER, notEmpty: true, unique:true}
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,

        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,

        // define the table's name
        tableName: 'E2E_Inspection_Request'
        
    });

    return e2e_inspection_request;

};
