"use strict";

module.exports = function(sequelize, DataTypes) {

    //for table specific values like data length, not null, unique values, pls use MySQL Workbench to view the values
    var e2e_inspection_finding = sequelize.define('E2E_Inspection_Finding', {
        ID: { field: 'ID', type: DataTypes.INTEGER, notEmpty: true, primaryKey: true,autoIncrement: true},
        IP_Name : {field: 'IP_Name',type: DataTypes.STRING(255),default: 'IP',notEmpty: true, unique:false},
        Finding_Date : {field: 'Finding_Date',type: DataTypes.DATE, notEmpty: true, unique:false},
        Flying_Cycle_Real : {field: 'Flying_Cycle_Real',type: DataTypes.INTEGER, default: 0,notEmpty: true, unique:false},
        Finding_Category_ID : {field: 'Finding_Category_ID',type: DataTypes.INTEGER,  default: 1, notEmpty: true, unique:false},
        Number_Spot : {field: 'Number_Spot',type: DataTypes.INTEGER ,default: 1, notEmpty: true, unique:false},
        Length : {field: 'Length',type: DataTypes.DECIMAL(10,2) ,default: 0.00, notEmpty: true, unique:false},
        Width : {field: 'Width',type: DataTypes.DECIMAL(10,2), default: 0.00, notEmpty: true, unique:false},
        Surface : {field: 'Surface',type: DataTypes.DECIMAL(10,2), default: 0.00, notEmpty: true, unique:false},
        Orientation : {field: 'Orientation',type: DataTypes.DECIMAL(5,2),default: 0.00,notEmpty: true, unique:false},
        Angle_Reference : {field: 'Angle_Reference',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Depth : {field: 'Depth',type: DataTypes.DECIMAL(10,2), default: 0.00, notEmpty: true, unique:false},
        Starting_Depth : {field: 'Starting_Depth',type: DataTypes.DECIMAL(10,2), notEmpty: true, unique:false},
        Ending_Depth : {field: 'Ending_Depth',type: DataTypes.DECIMAL(10,2), notEmpty: true, unique:false},
        Layer : {field: 'Layer',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Remaining_Thickness : {field: 'Remaining_Thickness',type: DataTypes.DECIMAL(10,2), default: 0.00, notEmpty: true, unique:false},
        Rotation_Angle : {field: 'Rotation_Angle',type: DataTypes.DECIMAL(5,2), default: 0.00, notEmpty: true, unique:false},
        Comments : {field: 'Comments',type: DataTypes.TEXT('long'), notEmpty: false, unique:false},
        Repair_Type_ID : {field: 'Repair_Type_ID',type: DataTypes.INTEGER, default: 1, notEmpty: true, unique:false},
        Repair_Reference : {field: 'Repair_Reference',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Part_Number : {field: 'Part_Number',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Part_Serial_Number : {field: 'Part_Serial_Number',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Environment_Category_ID : {field: 'Environment_Category_ID',type: DataTypes.INTEGER, notEmpty: false, unique:false},
        EASA_AMC_2020_Level_ID : {field: 'EASA_AMC_2020_Level_ID',type: DataTypes.INTEGER, notEmpty: false, unique:false},
        SSI_Reference : {field: 'SSI_Reference',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        RMT_File : {field: 'RMT_File',type: DataTypes.STRING(255), notEmpty: true, unique:false},
        RDAS_Reference : {field: 'RDAS_Reference',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Corrosion_Material_1 : {field: 'Corrosion_Material_1',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Corrosion_Material_2 : {field: 'Corrosion_Material_2',type: DataTypes.STRING(255), notEmpty: true, unique:false},
        Corrosion_Fastener_Specification : {field: 'Corrosion_Fastener_Specification',type: DataTypes.STRING(255), notEmpty: false, unique:false},
        Part_Criticity_ID : {field: 'Part_Criticity_ID',type: DataTypes.INTEGER, notEmpty: false, unique:false},
        Inspection_Area_Cycle_ID : {field: 'Inspection_Area_Cycle_ID',type: DataTypes.INTEGER, notEmpty: true, unique:false} , 
        Finding_Type_ID : {field: 'Finding_Type_ID',type: DataTypes.INTEGER, notEmpty: true, unique:false}
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,

        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,

        // define the table's name
        tableName: 'E2E_Inspection_Finding'
        
    });

    return e2e_inspection_finding;

};