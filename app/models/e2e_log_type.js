
"use strict";

module.exports = function(sequelize, DataTypes) {

    //for table specific values like data length, not null, unique values, pls use MySQL Workbench to view the values
    var e2e_log_type = sequelize.define('E2E_Log_Type', {
        ID: { field: 'ID', type: DataTypes.INTEGER, notEmpty: true, primaryKey: true,autoIncrement: true},
        Name: { field: 'Name', type: DataTypes.STRING(200), notEmpty: true, unique:false},
        Alias : {field: 'Alias',type: DataTypes.STRING(200),notEmpty: true, unique:true},
        Description: {field: 'Description',type: DataTypes.TEXT('long'),notEmpty: false, unique:false},
        Template: {field:'Template', type: DataTypes.JSON, notEmpty: false, unique:false }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,

        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,

        // define the table's name
        tableName: 'E2E_Log_Type'
        
    });

    return e2e_log_type;

};
