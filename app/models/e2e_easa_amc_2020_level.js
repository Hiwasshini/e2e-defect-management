
"use strict";

module.exports = function(sequelize, DataTypes) {

    //for table specific values like data length, not null, unique values, pls use MySQL Workbench to view the values
    var e2e_easa_amc_2020_level = sequelize.define('E2E_EASA_AMC_2020_Level', {
        ID: { field: 'ID', type: DataTypes.INTEGER, notEmpty: true, primaryKey: true,autoIncrement: true},
        Value : {field: 'Value',type: DataTypes.STRING(11),notEmpty: true, unique:true}
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current date (when deletion was done). paranoid will only work if
        // timestamps are enabled
        paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,

        // disable the modification of tablenames; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,

        // define the table's name
        tableName: 'E2E_EASA_AMC_2020_Level'
        
    });

    return e2e_easa_amc_2020_level;

};