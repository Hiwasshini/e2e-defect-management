'use strict';

const FindingCategory = require('./lib');

const findingcategory = new FindingCategory();
const classfilepath = 'app/controllers/api/e2e/findingcategory/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   FindingCategory:
 *     type: object
 *     required:
 *       - findingcategoryid
 *       - findingcategoryvalue
 *     properties:
 *       findingcategoryid:
 *         type: integer
 *       findingcategoryvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/findingcategory:
   *   post:
   *     summary: Add a new Finding Category
   *     description: Add a new Finding Category as a JSON object
   *     tags:
   *       - FindingCategory
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Finding Category object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/FindingCategory"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/findingcategory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/findingcategory');
 
      return(
        findingcategory
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/findingcategory: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/findingcategory: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/findingcategory: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/findingcategory: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/findingcategory/all:
   *   get:
   *     summary: List all Finding Category (not recommended)
   *     description: List all Finding Category as an JSON array
   *     tags:
   *       - FindingCategory
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/FindingCategory"
   */
  app.get('/api/e2e/findingcategory/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/findingcategory/all');

      findingcategory
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/findingcategory/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/findingcategory/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/findingcategory:
   *   get:
   *     summary: Get a Finding Category based on parameters
   *     description: Get a Finding Category based on parameters.
   *     tags:
   *       - FindingCategory
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: findingcategoryid
   *         in: path
   *         description: "Finding Category ID"
   *         required: true
   *         type: integer
   *       - name: findingcategoryvalue
   *         in: path
   *         description: "Finding Category Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/findingcategory', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/findingcategory');

     findingcategory
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/findingcategory: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/findingcategory: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/findingcategory: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/findingcategory/{id}:
   *   delete:
   *     summary: Removes a Finding Category
   *     description: Removes a Finding Category
   *     tags:
   *       - FindingCategory
   *     parameters:
   *       - name: findingcategoryid
   *         in: path
   *         description: "Finding Category id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/findingcategory/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/findingcategory');

		  return findingcategory
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/findingcategory: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/findingcategory: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/findingcategory: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/findingcategory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/findingcategory: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/findingcategory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/findingcategory:
   *   patch:
   *     summary: Update a Finding Category
   *     description: Update a Finding Category
   *     tags:
   *       - FindingCategory
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Finding Category object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/FindingCategory"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/findingcategory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/findingcategory');

		  return findingcategory
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/findingcategory: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/findingcategory: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/findingcategory: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/findingcategory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/findingcategory: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/findingcategory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
