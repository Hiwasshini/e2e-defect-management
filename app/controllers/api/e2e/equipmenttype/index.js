'use strict';

const EquipmentType = require('./lib');

const equipmenttype = new EquipmentType();
const classfilepath = 'app/controllers/api/e2e/equipmenttype/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   EquipmentType:
 *     type: object
 *     required:
 *       - equipmenttypeid
 *       - equipmenttypevalue
 *     properties:
 *       equipmenttypeid:
 *         type: integer
 *       equipmenttypevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/equipmenttype:
   *   post:
   *     summary: Add a new Equipment Type
   *     description: Add a new Equipment Type as a JSON object
   *     tags:
   *       - EquipmentType
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Equipment Type object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/EquipmentType"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/equipmenttype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/equipmenttype');
 
      return(
        equipmenttype
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/equipmenttype: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/equipmenttype: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/equipmenttype: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/equipmenttype: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/equipmenttype/all:
   *   get:
   *     summary: List all Equipment Type (not recommended)
   *     description: List all Equipment Type as an JSON array
   *     tags:
   *       - EquipmentType
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/EquipmentType"
   */
  app.get('/api/e2e/equipmenttype/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/equipmenttype/all');

      equipmenttype
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/equipmenttype/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/equipmenttype/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/equipmenttype:
   *   get:
   *     summary: Get a Equipment Type based on parameters
   *     description: Get a Equipment Type based on parameters.
   *     tags:
   *       - EquipmentType
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: equipmenttypeid
   *         in: path
   *         description: "Equipment Type ID"
   *         required: true
   *         type: integer
   *       - name: equipmenttypevalue
   *         in: path
   *         description: "Equipment Type Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/equipmenttype', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/equipmenttype');

     equipmenttype
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/equipmenttype: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/equipmenttype: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/equipmenttype: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/equipmenttype/{id}:
   *   delete:
   *     summary: Removes a Equipment Type
   *     description: Removes a Equipment Type
   *     tags:
   *       - EquipmentType
   *     parameters:
   *       - name: equipmenttypeid
   *         in: path
   *         description: "Equipment Type id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/equipmenttype/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/equipmenttype');

		  return equipmenttype
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/equipmenttype: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/equipmenttype: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/equipmenttype: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/equipmenttype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/equipmenttype: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/equipmenttype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/equipmenttype:
   *   patch:
   *     summary: Update a Equipment Type
   *     description: Update a Equipment Type
   *     tags:
   *       - EquipmentType
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Equipment Type object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/EquipmentType"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/equipmenttype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/equipmenttype');

		  return equipmenttype
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/equipmenttype: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/equipmenttype: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/equipmenttype: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/equipmenttype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/equipmenttype: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/equipmenttype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
