'use strict';

const AssemblyCondition = require('./lib');

const assemblycondition = new AssemblyCondition();
const classfilepath = 'app/controllers/api/e2e/assemblycondition/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   AssemblyCondition:
 *     type: object
 *     required:
 *       - assemblyconditionid
 *       - assemblyconditionvalue
 *     properties:
 *       assemblyconditionid:
 *         type: integer
 *       assemblyconditionvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/assemblycondition:
   *   post:
   *     summary: Add a new Assembly Condition
   *     description: Add a new Assembly Condition as a JSON object
   *     tags:
   *       - AssemblyCondition
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Assembly Condition object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/AssemblyCondition"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/assemblycondition', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/assemblycondition');
 
      return(
        assemblycondition
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/assemblycondition: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/assemblycondition: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/assemblycondition: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/assemblycondition: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/assemblycondition/all:
   *   get:
   *     summary: List all Assembly Condition (not recommended)
   *     description: List all Assembly Condition as an JSON array
   *     tags:
   *       - AssemblyCondition
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/AssemblyCondition"
   */
  app.get('/api/e2e/assemblycondition/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/assemblycondition/all');

      assemblycondition
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/assemblycondition/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/assemblycondition/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/assemblycondition:
   *   get:
   *     summary: Get a Assembly Condition based on parameters
   *     description: Get a Assembly Condition based on parameters.
   *     tags:
   *       - AssemblyCondition
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: assemblyconditionid
   *         in: path
   *         description: "Assembly Condition ID"
   *         required: true
   *         type: integer
   *       - name: assemblyconditionvalue
   *         in: path
   *         description: "Assembly Condition Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/assemblycondition', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/assemblycondition');

     assemblycondition
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/assemblycondition: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/assemblycondition: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/assemblycondition: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/assemblycondition/{id}:
   *   delete:
   *     summary: Removes a Assembly Condition
   *     description: Removes a Assembly Condition
   *     tags:
   *       - AssemblyCondition
   *     parameters:
   *       - name: assemblyconditionid
   *         in: path
   *         description: "Assembly Condition id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/assemblycondition/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/assemblycondition');

		  return assemblycondition
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/assemblycondition: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/assemblycondition: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/assemblycondition: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/assemblycondition: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/assemblycondition: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/assemblycondition: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/assemblycondition:
   *   patch:
   *     summary: Update a Assembly Condition
   *     description: Update a Assembly Condition
   *     tags:
   *       - AssemblyCondition
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Assembly Condition object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/AssemblyCondition"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/assemblycondition', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/assemblycondition');

		  return assemblycondition
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/assemblycondition: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/assemblycondition: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/assemblycondition: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/assemblycondition: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/assemblycondition: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/assemblycondition: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
