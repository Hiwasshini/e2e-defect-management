'use strict';

const appRoot = require('app-root-path');

const db      = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/assemblycondition/lib/index.js';
const  mergeJSON  = require ("merge-json") ;
var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

/**
 * Class that represents Assembly Condition through database
 */
class AssemblyCondition {

    /**
     * Assist the add() to insert/update records into E2E_Assembly_Condition table
     * @param {JSON} assemblycondition 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    addUpdateAssemblyCondition(assemblycondition,transact, transactionid){
		return new Promise((resolve, reject) => {
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid)');

			var objects = assemblycondition;
			//====START======================Handle the Assembly Condition======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
	  		var combine_insert = [];
	  		var prom_update;
	  		var prom_insert;
	  		var map_record_update = [];
	  		var map_record_insert = [];
	  		
	  		var str = JSON.stringify(objects);
	  		                          		
		  //find all section that requires update only
			  var parentcounter =-1;
			  
	    		var json_obj = objects;
				var tempval;
				
				//for update & insert
				json_obj.map(a =>{
	    			parentcounter++;
	    			if (a.Assembly_Condition_ID != null){	
	    				var index_obj = {};
	    				index_obj.parentindex = parentcounter;
						map_record_update.push(index_obj);
						
						var assemblycondition_obj = {};
						if (a.Assembly_Condition_ID != null){
							assemblycondition_obj.ID = a.Assembly_Condition_ID;
						}
						if (a.Assembly_Condition != null){
							assemblycondition_obj.Value = a.Assembly_Condition;
						}
						if (a.delete != null){
							assemblycondition_obj.deleted_at = commonModule.currentDateTime();
						}
						combine_update.push(assemblycondition_obj);
					}else if (a.Assembly_Condition_ID == null){	          							
	    				var index_obj = {};
	    				index_obj.parentindex = parentcounter;
						map_record_insert.push(index_obj);
						
						var assemblycondition_obj = {};
						if (a.Assembly_Condition_ID != null){
							assemblycondition_obj.ID = a.Assembly_Condition_ID;
						}
						if (a.Assembly_Condition != null){
							assemblycondition_obj.Value = a.Assembly_Condition;
						}
						combine_insert.push(assemblycondition_obj);
					}
				});

			  if (combine_update.length > 0){
				prom_update = new Promise(function(resolved, rejected) {
					db.E2E_Assembly_Condition
	                .bulkCreate(combine_update, { 
	                		updateOnDuplicate: ["Name","Value","deleted_at"], 
			  	        	transaction: transact,
							raw: true,
							useMaster: true
			  	        })
	                .then((result) => {
	                	var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Assembly_Condition_ID\"")
                    			.replace(/"Value"/g, "\"Assembly_Condition\"")); 
	                	for (var i=0;i<json_objs.length;i++){
		            		json_objs[i].message = 'Assembly Condition updated successfully.';
		            	}
		            	resolved(json_objs);
	                }).catch((errors) => {
	                	rejected(errors);
    			    });
				});
				promises_inner.push(prom_update);
			  }//end if (combine_part_update.length > 0)
				
			  if (combine_insert.length > 0){
				prom_insert = new Promise(function(resolved, rejected) {
				db.E2E_Assembly_Condition
	            .bulkCreate(combine_insert, { 
	            	individualHooks: true, 
	            	transaction: transact,
					raw: true,
					useMaster: true })
		                .then((result) => {
		                	var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Assembly_Condition_ID\"")
							.replace(/"Value"/g, "\"Assembly_Condition\""));    
		                	for (var i=0;i<json_objs.length;i++){
			            		json_objs[i].message = 'Assembly Condition inserted successfully.';
			            	}
			            	resolved(json_objs);
		                }).catch((errors) => {
		                	rejected(errors);
		  			    }); 
				  });
				promises_inner.push(prom_insert);
			  }//end if (combine_part_insert.length > 0)
			  
			if (promises_inner.length > 0){
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
				.then(function (ress) {
	  			
	  			if (combine_update.length > 0){
                    console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid): updated result returned');

	  				var resp = ress[0];   
	  				for (var j=0;j<resp.length;j++){  
                        var index_obj =  map_record_update[j]; 	
	  					var obj_new = JSON.parse(JSON.stringify(resp[j]));
                   
                        //update the obj_new to the index of the old element
	  					objects[index_obj.parentindex] = obj_new;
	  				}//end for (var j=0;j<resp.length;j++)
	  				ress.splice(0, 1);
	  			}
	  			if (combine_insert.length > 0){
                    console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid): inserted result returned');

	  				var resp = ress[0];   
	  				for (var j=0;j<resp.length;j++){  
                        var index_obj =  map_record_insert[j]; 		
	  					var obj_new = JSON.parse(JSON.stringify(resp[j]));
                          
                        //update the obj_new to the index of the old element
	  					objects[index_obj.parentindex] = obj_new;
	  				}//end for (var j=0;j<resp.length;j++)
	  				ress.splice(0, 1);
	  			}
                  
                console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid): returning result');
	  			resolve(objects);
	//====END======================Handle the Assembly Condition====================================================== 
	      		}).catch((errors) => {
					var error_code = errors.errors[0];
					error_code = error_code.type;

					var msg
					if (error_code == 'unique violation'){
					  msg = {'message':'Duplicate Assembly Condition value found. Pls ensure that Assembly Condition value is unqiue.'};
					}else{
						msg = errors;
					}
					console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid): returning error');
					reject(msg);
	  			}); //end return Promise.all(promises_inner)
			}else{
				console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: addUpdateAssemblyCondition(assemblycondition, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)
				
		});//end return new Promise((resolve, reject) =>
	};//end addUpdateAssemblyCondition
    
    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} assemblycondition 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    add(assemblycondition, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: add(assemblycondition, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(assemblycondition) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: add(assemblycondition, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateAssemblyCondition(assemblycondition,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: add(assemblycondition, transact, transactionid): returning result from addUpdateAssemblyCondition');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: add(assemblycondition, transact, transactionid): returning error from addUpdateAssemblyCondition: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: add(assemblycondition, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} assemblycondition 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    update(assemblycondition, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: update(assemblycondition, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(assemblycondition) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: update(assemblycondition, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateAssemblyCondition(assemblycondition,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: update(assemblycondition, transact, transactionid): returning result from addUpdateAssemblyCondition');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: update(assemblycondition, transact, transactionid): returning error from addUpdateAssemblyCondition: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: update(assemblycondition, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end update


    /**
	   * Get a specific Assembly Condition
	   *
	   * @param {Integer} Assembly_Condition_ID - E2E_Assembly_Condition ID
	   * @param {String} Assembly_Condition - E2E_Assembly_Condition Value
   	   * @param {Integer} assemblyconditionclone - Clone template to become another assembly condition (0) or clone to become finding (1)
   	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
    get(param,search,transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid)');
			console.log('('+ transactionid +'): parameter app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			if (param.index == null){
                param.index = 0;
            }else{
                if (param.index > 0){
                    param.index = param.index -1;
                }
            }

            if (param.row == null){
                param.row = 30;
            }
            
            if (param.order == null){
                param.order = 'DESC';
            }

            if (param.ordercolumn == null){
                param.ordercolumn = 'Assembly_Condition_ID';
            }

            //create the overall sql statements
			var sql_query =
			//' SELECT '+
			' ID as Assembly_Condition_ID, '+
			' Value as Assembly_Condition '+
			' FROM '+
			' E2E_Assembly_Condition '+			
			' WHERE deleted_at is null @assemblyconditionid @assemblyconditionvalue ' +
			'   ORDER BY ' + param.ordercolumn + ' ' + param.order  +
			'   LIMIT ' + param.index + ', ' + param.row +
			'	; ';

            var replace_query_assemblyconditionid = ' AND ID in (@replace) ';
            var replace_query_assemblyconditionvalue = ' AND lcase(Value) in (@replace) ';

            if (param.Assembly_Condition_ID != null){   
				sql_query = sql_query.replace('@assemblyconditionid', replace_query_assemblyconditionid.replace('@replace',param.Assembly_Condition_ID));
            }else{
				sql_query = sql_query.replace('@assemblyconditionid', '');
            }

            if (param.Assembly_Condition != null){
                var value = param.Assembly_Condition;
                if (value.split(',').length > 1){
                    value = '\'' + value.replace(/,/g, "\',\'") + '\'';
                }else{
                    value = '\'' + value + '\'';
                }    
                sql_query = sql_query.replace('@assemblyconditionvalue', replace_query_assemblyconditionvalue.replace('@replace',value.toLowerCase()));
            }else{
                sql_query = sql_query.replace('@assemblyconditionvalue', '');
			}
			
			if (search == 0){ //proceed to return the sql string
				sql_query = ' Select ' + sql_query ;
			resolve(sql_query);			 
			}else if (search == 1 || search == 2){ //proceed to search query with database
				if (search == 1){//used for actual search requested by user
					sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + 'SELECT FOUND_ROWS();';
				}else if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					sql_query = ' Select ' + sql_query;
				}
				
			console.log('('+ transactionid +'): query app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.sequelize.query(
                sql_query,
                {
					type: db.sequelize.QueryTypes.SELECT,
					useMaster: false
                })
                .then((res) => {
                    var jsonobj = null;
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					jsonobj = res;
    				//resolve(res);
    			}
    			else if (search == 1){//used for actual search requested by user
					//total number of rows from search result (without limit)
    				var total_rows = res[1][0]["FOUND_ROWS()"];
    				var loop_end_index;
    				
    				if (param.row < total_rows){
    					loop_end_index = param.row;
    				}else if (total_rows < param.row){
    					loop_end_index = total_rows;
    				}else{
    					loop_end_index = total_rows;
    				}
    				
    				var return_result = {};
					var final_result = [];
					
    				for (var i=0;i<loop_end_index;i++){
    					final_result.push(res[0][i]);
    				}
    				return_result.Result = final_result;
    				return_result.Actual_Total_Records_Found = total_rows;

    				jsonobj = return_result.Result;
    				//resolve(return_result);
				}
				
				//put each finding into a finding array
	        	var counter = -1;
	        	jsonobj.map(a =>{
	        		counter++;
	        		var assemblyconditionplaceholder = {};
	        		
	        		if (param.assemblyconditionclone != null){
	            		if (param.assemblyconditionclone == 1){
	            			assemblyconditionplaceholder.finding = [];
	            		}else{
	            			assemblyconditionplaceholder = [];
	            		}
	            	}else{
	            		assemblyconditionplaceholder = [];
	            	}
	        		
	        		    	    	    	        		
	        		 if (a.childassemblycondition != null){
	        		 	var inner_counter = -1;
	        		 	a.childassemblycondition.map(b =>{
	        		 		inner_counter++;
	        		 		var assemblyconditionplaceholder_inner = {};
	        		 		assemblyconditionplaceholder_inner.finding = [];
	        		 		assemblyconditionplaceholder_inner.finding.push(b);
	        		 		a.childassemblycondition[inner_counter] = assemblyconditionplaceholder_inner;	  
	        		 	});
	        		 }
	        		if (param.assemblyconditionclone != null){
	            		if ( (param.assemblyconditionclone == 0 || param.assemblyconditionclone == 1) && search != 2){
	            			delete a.assemblyconditionid;
	            		}
	        		}
	        		
					//assemblyconditionplaceholder.assemblycondition.push(a);
					//displays the data (ID and Value)
	        		if (param.assemblyconditionclone != null){
	            		 if (param.assemblyconditionclone == 1){
	            			assemblyconditionplaceholder.finding.push(a);
	            		}else{
	            			assemblyconditionplaceholder.push(a);
	            		}
	            	}else{
	            		assemblyconditionplaceholder.push(a);
	            	}
	        		
	        		jsonobj[counter] = assemblyconditionplaceholder;	  
	    		}).join(",");
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid): returning result');
					resolve(jsonobj);
    			}else if (search == 1){
					console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid): returning result');
					if (jsonobj != 0){
					return_result.Result = jsonobj;
					resolve(return_result);
					}
					else{
					console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid): returning result');
					return_result.Result = 'Not found';
					resolve(return_result);
					}
					}
    		})
                .catch((error) => {
					console.log(error);
					console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
                    reject(error);
                });
			}//end if (search == 0)
        });
	}//end get

	/**
	 * List all Assembly Condition from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {
        
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: list(transact, transactionid)');
			
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.E2E_Assembly_Condition
                .findAll({
						attributes: [['ID', 'Assembly_Condition_ID'], ['Value', 'Assembly_Condition']],
						useMaster: false
                    }
                )
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: list(transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: list(transact, transactionid): returning error: ' + error);
                    reject(error);
                });
            });
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    remove(id,transact, transactionid) {
        return new Promise((resolve, reject) => {

            console.log('('+ transactionid +'): entered app/controllers/api/e2e/assemblycondition/lib/index.js: remove(id, transact, transactionid)');
            console.log('('+ transactionid +'): parameter app/controllers/api/e2e/assemblycondition/lib/index.js: remove(id,transact, transactionid): ' + id);
            //convert id to array
            var array_id = id.split(',');
            db.E2E_Assembly_Condition
                .destroy({
					transaction: transact,
					useMaster: true,
                    where : {
                        ID : array_id
                    }
                })
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: remove(id, transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/assemblycondition/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
                    reject(error);
                });
        });
	}
	
	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} assemblycondition 
     * @param {String} transactionid 
     */
    checkEmptyFields(assemblycondition){
        return new Promise((resolve, reject) => {
            var anyemptyfields = false;

            assemblycondition.map(a =>{
                if (a.Assembly_Condition == null){
                    reject({'message':'Assembly_Condition is mandatory field'});
                }
            });

            resolve(0);
        });
    }
}

module.exports = AssemblyCondition;
