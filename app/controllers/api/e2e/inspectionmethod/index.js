'use strict';

const InspectionMethod = require('./lib');

const inspectionmethod = new InspectionMethod();
const classfilepath = 'app/controllers/api/e2e/inspectionmethod/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionMethod:
 *     type: object
 *     required:
 *       - inspectionmethodid
 *       - inspectionmethodvalue
 *     properties:
 *       inspectionmethodid:
 *         type: integer
 *       inspectionmethodvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionmethod:
   *   post:
   *     summary: Add a new Inspection Method
   *     description: Add a new Inspection Method as a JSON object
   *     tags:
   *       - InspectionMethod
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Method object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionMethod"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionmethod', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionmethod');
 
      return(
        inspectionmethod
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionmethod: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionmethod: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionmethod: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionmethod: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionmethod/all:
   *   get:
   *     summary: List all Inspection Method (not recommended)
   *     description: List all Inspection Method as an JSON array
   *     tags:
   *       - InspectionMethod
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionMethod"
   */
  app.get('/api/e2e/inspectionmethod/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionmethod/all');

      inspectionmethod
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionmethod/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionmethod/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionmethod:
   *   get:
   *     summary: Get a Inspection Method based on parameters
   *     description: Get a Inspection Method based on parameters.
   *     tags:
   *       - InspectionMethod
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionmethodid
   *         in: path
   *         description: "Inspection Method ID"
   *         required: true
   *         type: integer
   *       - name: inspectionmethodvalue
   *         in: path
   *         description: "Inspection Method Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionmethod', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionmethod');

     inspectionmethod
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionmethod: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionmethod: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionmethod: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionmethod/{id}:
   *   delete:
   *     summary: Removes a Inspection Method
   *     description: Removes a Inspection Method
   *     tags:
   *       - InspectionMethod
   *     parameters:
   *       - name: inspectionmethodid
   *         in: path
   *         description: "Inspection Method id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionmethod/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionmethod');

		  return inspectionmethod
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionmethod: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionmethod:
   *   patch:
   *     summary: Update a Inspection Method
   *     description: Update a Inspection Method
   *     tags:
   *       - InspectionMethod
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Method object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionMethod"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionmethod', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionmethod');

		  return inspectionmethod
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionmethod: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
