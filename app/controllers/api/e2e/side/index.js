'use strict';

const Side = require('./lib');

const side = new Side();
const classfilepath = 'app/controllers/api/e2e/side/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   Side:
 *     type: object
 *     required:
 *       - sideid
 *       - sidevalue
 *     properties:
 *       sideid:
 *         type: integer
 *       sidevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/side:
   *   post:
   *     summary: Add a new Side
   *     description: Add a new Side as a JSON object
   *     tags:
   *       - Side
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Side object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/Side"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/side', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/side');
 
      return(
        side
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/side: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/side: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/side: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/side: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/side/all:
   *   get:
   *     summary: List all Side (not recommended)
   *     description: List all Side as an JSON array
   *     tags:
   *       - Side
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/Side"
   */
  app.get('/api/e2e/side/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/side/all');

      side
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/side/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/side/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/side/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/side/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/side:
   *   get:
   *     summary: Get a Side based on parameters
   *     description: Get a Side based on parameters.
   *     tags:
   *       - Side
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: sideid
   *         in: path
   *         description: "Side ID"
   *         required: true
   *         type: integer
   *       - name: sidevalue
   *         in: path
   *         description: "Side Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/side', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/side');

     side
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/side: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/side: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/side: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/side: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/side: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/side: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/side/{id}:
   *   delete:
   *     summary: Removes a Side
   *     description: Removes a Side
   *     tags:
   *       - Side
   *     parameters:
   *       - name: sideid
   *         in: path
   *         description: "Side id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/side/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/side');

		  return side
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/side: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/side: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/side: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/side: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/side: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/side: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/side:
   *   patch:
   *     summary: Update a Side
   *     description: Update a Side
   *     tags:
   *       - Side
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Side object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/Side"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/side', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/side');

		  return side
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/side: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/side: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/side: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/side: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/side: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/side: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
