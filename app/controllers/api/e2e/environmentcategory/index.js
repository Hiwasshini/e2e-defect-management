'use strict';

const EnvironmentCategory = require('./lib');

const environmentcategory = new EnvironmentCategory();
const classfilepath = 'app/controllers/api/e2e/environmentcategory/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   EnvironmentCategory:
 *     type: object
 *     required:
 *       - environmentcategoryid
 *       - environmentcategoryvalue
 *     properties:
 *       environmentcategoryid:
 *         type: integer
 *       environmentcategoryvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/environmentcategory:
   *   post:
   *     summary: Add a new Environment Category
   *     description: Add a new Environment Category as a JSON object
   *     tags:
   *       - EnvironmentCategory
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Environment Category object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/EnvironmentCategory"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/environmentcategory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/environmentcategory');
 
      return(
        environmentcategory
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/environmentcategory: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/environmentcategory: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/environmentcategory: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/environmentcategory: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/environmentcategory/all:
   *   get:
   *     summary: List all Environment Category (not recommended)
   *     description: List all Environment Category as an JSON array
   *     tags:
   *       - EnvironmentCategory
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/EnvironmentCategory"
   */
  app.get('/api/e2e/environmentcategory/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/environmentcategory/all');

      environmentcategory
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/environmentcategory/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/environmentcategory/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/environmentcategory:
   *   get:
   *     summary: Get a Environment Category based on parameters
   *     description: Get a Environment Category based on parameters.
   *     tags:
   *       - EnvironmentCategory
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: environmentcategoryid
   *         in: path
   *         description: "Environment Category ID"
   *         required: true
   *         type: integer
   *       - name: environmentcategoryvalue
   *         in: path
   *         description: "Environment Category Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/environmentcategory', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/environmentcategory');

     environmentcategory
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/environmentcategory: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/environmentcategory: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/environmentcategory: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/environmentcategory/{id}:
   *   delete:
   *     summary: Removes a Environment Category
   *     description: Removes a Environment Category
   *     tags:
   *       - EnvironmentCategory
   *     parameters:
   *       - name: environmentcategoryid
   *         in: path
   *         description: "Environment Category id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/environmentcategory/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/environmentcategory');

		  return environmentcategory
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/environmentcategory: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/environmentcategory: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/environmentcategory: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/environmentcategory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/environmentcategory: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/environmentcategory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/environmentcategory:
   *   patch:
   *     summary: Update a Environment Category
   *     description: Update a Environment Category
   *     tags:
   *       - EnvironmentCategory
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Environment Category object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/EnvironmentCategory"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/environmentcategory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/environmentcategory');

		  return environmentcategory
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/environmentcategory: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/environmentcategory: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/environmentcategory: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/environmentcategory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/environmentcategory: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/environmentcategory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
