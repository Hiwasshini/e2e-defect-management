'use strict';

const InspectionFindingFindingType = require('./lib');

const inspectionfindingfindingtype = new InspectionFindingFindingType();
const classfilepath = 'app/controllers/api/e2e/inspectionfindingfindingtype/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionFindingFindingType:
 *     type: object
 *     required:
 *       - inspectionfindingfindingtypeid
 *       - inspectionfindingfindingtypevalue
 *     properties:
 *       inspectionfindingfindingtypeid:
 *         type: integer
 *       inspectionfindingfindingtypevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionfindingfindingtype:
   *   post:
   *     summary: Add a new Inspection Finding Finding Type
   *     description: Add a new Inspection Finding Finding Type as a JSON object
   *     tags:
   *       - InspectionFindingFindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding Finding Type object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFindingFindingType"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionfindingfindingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionfindingfindingtype');
 
      return(
        inspectionfindingfindingtype
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfindingfindingtype: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfindingfindingtype: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfindingfindingtype: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfindingfindingtype: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionfindingfindingtype/all:
   *   get:
   *     summary: List all Inspection Finding Finding Type (not recommended)
   *     description: List all Inspection Finding Finding Type as an JSON array
   *     tags:
   *       - InspectionFindingFindingType
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionFindingFindingType"
   */
  app.get('/api/e2e/inspectionfindingfindingtype/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfindingfindingtype/all');

      inspectionfindingfindingtype
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionfindingfindingtype:
   *   get:
   *     summary: Get a Inspection Finding Finding Type based on parameters
   *     description: Get a Inspection Finding Finding Type based on parameters.
   *     tags:
   *       - InspectionFindingFindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionfindingfindingtypeid
   *         in: path
   *         description: "Inspection Finding Finding Type ID"
   *         required: true
   *         type: integer
   *       - name: inspectionfindingfindingtypevalue
   *         in: path
   *         description: "Inspection Finding Finding Type Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionfindingfindingtype', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfindingfindingtype');

     inspectionfindingfindingtype
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingfindingtype: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionfindingfindingtype/{id}:
   *   delete:
   *     summary: Removes a Inspection Finding Finding Type
   *     description: Removes a Inspection Finding Finding Type
   *     tags:
   *       - InspectionFindingFindingType
   *     parameters:
   *       - name: inspectionfindingfindingtypeid
   *         in: path
   *         description: "Inspection Finding Finding Type id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionfindingfindingtype/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionfindingfindingtype');

		  return inspectionfindingfindingtype
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfindingfindingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionfindingfindingtype:
   *   patch:
   *     summary: Update a Inspection Finding Finding Type
   *     description: Update a Inspection Finding Finding Type
   *     tags:
   *       - InspectionFindingFindingType
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding Finding Type object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFindingFindingType"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionfindingfindingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionfindingfindingtype');

		  return inspectionfindingfindingtype
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfindingfindingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
