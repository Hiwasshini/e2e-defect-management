'use strict';

const appRoot = require('app-root-path');

const db = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/inspectionarea/lib/index.js';
const mergeJSON = require("merge-json");
const InspectionAreaCycle = require('../../inspectionareacycle/lib');
const InspectionAreaFindingType = require('../../inspectionareafindingtype/lib');
const InspectionFinding = require('../../inspectionfinding/lib');
var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

const NDT_Inspection_Area_Cycle = new InspectionAreaCycle();
const Expected_Finding_Type = new InspectionAreaFindingType();
const NDT_Inspection_Finding = new InspectionFinding();

/**
 * This is a sub-super API that help to insert the following: 
 * 1. 	|_Inspection_Area
 * 2. 		|_Inspection_Area_Cycle
 * 3. 			|_Inspection_Finding
 * 
 * There is a need to retrieve the ID for the following tables and append to the existing data before insertion into the database
 * 1. Inspection_Area
 *		a. Area_Inspectability
 *		b. Side
 *		c. Inspection_Type
 *		d. Assembly_Conditions
 *		e. Inspection_Method
 *		f. Finding_Type (multi-valued)
 * 2. Inspection_Area_Cycle
 *		a. Equipment_Type
 *		b. Airline
 * 3. Inspection_Finding
 *		a. Finding_Type
 *		b. Part_Criticity
 *		c. Repair_Type
 *		d. Finding_Category
 *		e. Environment_Category
 *		f. EASA_AMC_2020_Level
 */

//This API can only POST/PATCH data to itself, it is not able to fully function to POST/PATCH for subsequent tables.  
//The troubleshooting needs to be done @ addUpdateInspectionFinding
//Need to work on GET and Remove functions

/**
 * Class that represents Inspection Area through database
 */
class InspectionArea {

    /**
     * Assist the add() to insert/update records into E2E_Inspection_Area table
     * @param {JSON} inspectionarea 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	addUpdateInspectionArea(inspectionarea, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid)');

			var objects = inspectionarea;
			//====START======================Handle the Inspection Area======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;

			var json_obj = objects;
			var tempval;

			//for update & insert
			json_obj.map(a => {
				parentcounter++;
				if (a.Inspection_Area_ID != null) {
					var index_obj = {};
					index_obj.parentindex = parentcounter;
					map_record_update.push(index_obj);

					var inspectionarea_obj = {};
					if (a.Inspection_Area_ID != null) {
						inspectionarea_obj.ID = a.Inspection_Area_ID;
					}
					if (a.Area_Description != null) {
						inspectionarea_obj.Area_Description = a.Area_Description;
					}
					if (a.Area_Inspectability_ID != null) {
						inspectionarea_obj.Area_Inspectability_ID = a.Area_Inspectability_ID;
					}
					if (a.Side_ID != null) {
						inspectionarea_obj.Side_ID = a.Side_ID;
					}
					if (a.Assembly_Condition_ID != null) {
						inspectionarea_obj.Assembly_Condition_ID = a.Assembly_Condition_ID;
					}
					if (a.Inspection_Effort != null) {
						inspectionarea_obj.Inspection_Effort = a.Inspection_Effort;
					}
					if (a.Material != null) {
						inspectionarea_obj.Material = a.Material;
					}
					if (a.Inspection_Type_ID != null) {
						inspectionarea_obj.Inspection_Type_ID = a.Inspection_Type_ID;
					}
					if (a.Inspection_Method_ID != null) {
						inspectionarea_obj.Inspection_Method_ID = a.Inspection_Method_ID;
					}
					if (a.Inspection_Procedure != null) {
						inspectionarea_obj.Inspection_Procedure = a.Inspection_Procedure;
					}
					if (a.Inspection_Request_ID != null) {
						inspectionarea_obj.Inspection_Request_ID = a.Inspection_Request_ID;
					}
					if (a.delete != null) {
						inspectionarea_obj.deleted_at = commonModule.currentDateTime();
					}
					combine_update.push(inspectionarea_obj);
				} else if (a.Inspection_Area_ID == null) {
					var index_obj = {};
					index_obj.parentindex = parentcounter;
					map_record_insert.push(index_obj);

					var inspectionarea_obj = {};
					if (a.Inspection_Area_ID != null) {
						inspectionarea_obj.ID = a.Inspection_Area_ID;
					}
					if (a.Area_Description != null) {
						inspectionarea_obj.Area_Description = a.Area_Description;
					}
					if (a.Area_Inspectability_ID != null) {
						inspectionarea_obj.Area_Inspectability_ID = a.Area_Inspectability_ID;
					}
					if (a.Side_ID != null) {
						inspectionarea_obj.Side_ID = a.Side_ID;
					}
					if (a.Assembly_Condition_ID != null) {
						inspectionarea_obj.Assembly_Condition_ID = a.Assembly_Condition_ID;
					}
					if (a.Inspection_Effort != null) {
						inspectionarea_obj.Inspection_Effort = a.Inspection_Effort;
					}
					if (a.Material != null) {
						inspectionarea_obj.Material = a.Material;
					}
					if (a.Inspection_Type_ID != null) {
						inspectionarea_obj.Inspection_Type_ID = a.Inspection_Type_ID;
					}
					if (a.Inspection_Method_ID != null) {
						inspectionarea_obj.Inspection_Method_ID = a.Inspection_Method_ID;
					}
					if (a.Inspection_Procedure != null) {
						inspectionarea_obj.Inspection_Procedure = a.Inspection_Procedure;
					}
					if (a.Inspection_Request_ID != null) {
						inspectionarea_obj.Inspection_Request_ID = a.Inspection_Request_ID;
					}
					combine_insert.push(inspectionarea_obj);
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					db.E2E_Inspection_Area
						.bulkCreate(combine_update, {
							updateOnDuplicate: ["Name", "Area_Description", "Area_Inspectability_ID", "Side_ID", "Assembly_Condition_ID", "Inspection_Effort",
								"Material", "Inspection_Type_ID", "Inspection_Method_ID", "Inspection_Procedure", "Inspection_Request_ID", "deleted_at"],
							transaction: transact,
							raw: true,
							useMaster: true
						})
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Area_ID\"")
								.replace(/"Area_Description"/g, "\"Area_Description\"")
								.replace(/"Area_Inspectability_ID"/g, "\"Area_Inspectability_ID\"")
								.replace(/"Side_ID"/g, "\"Side_ID\"")
								.replace(/"Assembly_Condition_ID"/g, "\"Assembly_Condition_ID\"")
								.replace(/"Inspection_Effort"/g, "\"Inspection_Effort\"")
								.replace(/"Material"/g, "\"Material\"")
								.replace(/"Inspection_Type_ID"/g, "\"Inspection_Type_ID\"")
								.replace(/"Inspection_Method_ID"/g, "\"Inspection_Method_ID\"")
								.replace(/"Inspection_Procedure"/g, "\"Inspection_Procedure\"")
								.replace(/"Inspection_Request_ID"/g, "\"Inspection_Request_ID\""));
							for (var i = 0; i < json_objs.length; i++) {
								json_objs[i].message = 'Inspection Area updated successfully.';
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					db.E2E_Inspection_Area
						.bulkCreate(combine_insert, {
							individualHooks: true,
							transaction: transact,
							raw: true,
							useMaster: true
						})
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Area_ID\"")
								.replace(/"Area_Description"/g, "\"Area_Description\"")
								.replace(/"Area_Inspectability_ID"/g, "\"Area_Inspectability_ID\"")
								.replace(/"Side_ID"/g, "\"Side_ID\"")
								.replace(/"Assembly_Condition_ID"/g, "\"Assembly_Condition_ID\"")
								.replace(/"Inspection_Effort"/g, "\"Inspection_Effort\"")
								.replace(/"Material"/g, "\"Material\"")
								.replace(/"Inspection_Type_ID"/g, "\"Inspection_Type_ID\"")
								.replace(/"Inspection_Method_ID"/g, "\"Inspection_Method_ID\"")
								.replace(/"Inspection_Procedure"/g, "\"Inspection_Procedure\"")
								.replace(/"Inspection_Request_ID"/g, "\"Inspection_Request_ID\""));
							for (var i = 0; i < json_objs.length; i++) {
								json_objs[i].message = 'Inspection Area inserted successfully.';
								//insert a inspection area id to each object
								// var list = null;
								// if (json_objs[i].Inspection_Area != null) {
								// 	list = json_objs[i].Inspection_Area;
								// 	for (var j = 0; j < list.length; j++) {
								// 		list[j].Inspection_Area_ID = json_objs[i].Inspection_Area_ID;
								// 	}
								// 	json_objs[i].Inspection_Area = list;
								// }
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						});
				}); //console.log("hello")
				promises_inner.push(prom_insert);

			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						console.log(ress)
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): updated result returned');
							var resp = ress[0];
							parentcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								parentcounter++;
								//console.log(map_record_update[parentcounter])
								var index_obj = map_record_update[parentcounter];
								var ia_obj = ia_array[index_obj.parentindex].NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								for (var k = 0; k < ir_obj.length; k++) {
									ia_obj[k].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area cycle
								}
								var Expected_Finding_Type_array = ia_array[index_obj.parentindex].Expected_Finding_Type;
								for (var m = 0; m < Expected_Finding_Type_array.length; m++) {
									Expected_Finding_Type_array[m].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area finding type
								}
								objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								objects[index_obj.parentindex].Expected_Finding_Type = Expected_Finding_Type_array; //put back the array of inspection area finding type(including inspection area id) to inspection area object
								objects[index_obj.parentindex].NDT_Inspection_Area_Cycle = ia_obj; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}
						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): inserted result returned');
							// var resp = ress[0];   
							// for (var j=0;j<resp.length;j++){  
							//   var index_obj =  map_record_insert[j]; 		
							// 	var obj_new = JSON.parse(JSON.stringify(resp[j]));
								
							//   //update the obj_new to the index of the old element
							// 	objects[index_obj.parentindex] = obj_new;
							var resp = ress[0];
							parentcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								parentcounter++;
								//console.log(map_record_insert[parentcounter])
								var index_obj = map_record_insert[parentcounter];
								var ia_array;
								var ia_obj = objects[index_obj.parentindex].NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								for (var k = 0; k < ia_obj.length; k++) {
									ia_obj[k].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area cycle
								}
								var Expected_Finding_Type_array = objects[index_obj.parentindex].Expected_Finding_Type;
								for (var m = 0; m < Expected_Finding_Type_array.length; m++) {
									Expected_Finding_Type_array[m].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area finding type
								}
								objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								objects[index_obj.parentindex].Expected_Finding_Type = Expected_Finding_Type_array; //put back the array of inspection area finding type(including inspection area id) to inspection area object 
								objects[index_obj.parentindex].NDT_Inspection_Area_Cycle = ia_obj; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
							}//end for (var j=0;j<resp.length;j++)
							
								// //console.log(ress)
								// var resp = ress[0];
								// //childcounter = -1;
								// parentcounter = -1;
								// for (var j = 0; j < resp.length; j++) {
								// 	parentcounter++;
								// 	//console.log(map_record_insert[parentcounter])
								// 	var index_obj = map_record_insert[parentcounter];
								// 	var ir_obj = objects[index_obj.parentindex].NDT_Inspection_Area_Cycle; //inspection request object which contains an array of inspection area
								// 	for (var k = 0; k < ir_obj.length; k++) {
								// 		ir_obj[k].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection request id to each and every inspection area
								// 	}
								// 	objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								// 	objects[index_obj.parentindex].NDT_Inspection_Area_Cycle = ir_obj; //put back the array of inspection area(including inspection request id) into inspection request object
								// }//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}  //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): returning result');
						resolve(objects);
						//console.log(objects)
						//====END======================Handle the Inspection Area====================================================== 
					}).catch((errors) => {
						console.log(errors)
						var error_code = errors.errors[0];
						error_code = error_code.type;

						var msg
						if (error_code == 'unique violation') {
							msg = { 'message': 'Duplicate Inspection Area value found. Pls ensure that Inspection Area value is unqiue.' };
						} else {
							msg = errors;
						}
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): returning error');
						reject(msg);
					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionArea

	addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid)');
			//   console.log('Objects')
			//   console.log(JSON.stringify(inspectionareacycle))
			var objects = inspectionareacycle;
			//====START======================Handle the Inspection Area Cycle======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;

			var json_obj = objects;
			var tempval;

			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area_Cycle != null) {
					a.NDT_Inspection_Area_Cycle.map(b => {
						childcounter++; //increment the child(parent)
						if (b.Inspection_Area_Cycle_ID != null) {	//for update
							var index_obj = {};
							index_obj.parentindex = parentcounter;
							index_obj.childindex = childcounter;
							map_record_update.push(index_obj);
							combine_update.push(b);
						} else if (b.Inspection_Area_Cycle_ID == null) {	//for insert
							var index_obj = {};
							index_obj.parentindex = parentcounter;
							index_obj.childindex = childcounter;
							map_record_insert.push(index_obj);
							combine_insert.push(b);
							// console.log(JSON.stringify(index_obj))
						}
					});
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area_Cycle.update(combine_update, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area_Cycle.add(combine_insert, transact, transactionid)
						.then((res) => {
							//console.log(res)
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						//console.log(ress)

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): updated result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ia_obj = objects[index_obj.parentindex]; //inspection area object (parent)
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.childindex]; //each inspection area cycle object (child) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								//append all the ids from the resp[j] to iac_obj
								iac_obj.Equipment_Type_ID = resp[j].Equipment_Type_ID;
								iac_obj.Airline_ID = resp[j].Airline_ID;
								for (var k = 0; k < if_array.length; k++) {
									if_array[k].Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;//set the inspection area cycle id to each and every inspection finding
								}
								iac_obj.Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.childindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								objects[index_obj.parentindex] = ia_obj; //put back the inspection area object to the array of inspection area  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): inserted result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								var ia_obj = objects[index_obj.parentindex]; //inspection area object (parent)
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.childindex]; //each inspection area cycle object (child) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								//append all the ids from the resp[j] to iac_obj
								iac_obj.Equipment_Type_ID = resp[j].Equipment_Type_ID;
								iac_obj.Airline_ID = resp[j].Airline_ID;
								for (var k = 0; k < iac_obj.length; k++) {
									if_array[k].Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;//set the inspection area cycle id to each and every inspection finding
								}
								iac_obj.Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.childindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								objects[index_obj.parentindex] = ia_obj; //put back the inspection area object to the array of inspection area  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						} //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Area Cycle====================================================== 
					}).catch((errors) => {
						console.log(errors)
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning error');

					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionAreaCycle

	addUpdateInspectionFinding(inspectionfinding, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid)');
			//console.log(JSON.stringify(inspectionfinding))
			var objects = inspectionfinding;
			//====START======================Handle the Inspection Finding======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;
			var grandchildcounter = -1;
			var json_obj = objects;
			var tempval;

			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area_Cycle != null) {
					a.NDT_Inspection_Area_Cycle.map(b => {
						childcounter++; //increment the child(parent)
						grandchildcounter = -1; //reset back the grandchild to its child(parent)
						if (b.NDT_Inspection_Finding != null) {
							b.NDT_Inspection_Finding.map(c => {
								grandchildcounter++;
								if (c.Inspection_Finding_ID != null) {	//for update
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									map_record_update.push(index_obj);
									combine_update.push(c);

								} else if (c.Inspection_Finding_ID == null) {	//for insert
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									map_record_insert.push(index_obj);
									combine_insert.push(c);
								}
							});
						}
					});
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					NDT_Inspection_Finding.update(combine_update, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					NDT_Inspection_Finding.add(combine_insert, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			console.log("Objects")
			console.log(JSON.stringify(objects))

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						// console.log("ress")
						// console.log(ress)

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): updated result returned');

							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								var resp = ress[0];
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ia_obj = objects[index_obj.parentindex]; //inspection area object (parent)
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.childindex]; //each inspection area cycle object (child) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								var if_obj = if_array[index_obj.grandchildindex]; //each inspection finding object (grandchild) from the array of inspection finding type
								// append all the ids from the resp[j] to if_obj
								if_obj.Finding_Category_ID = resp[j].Finding_Category_ID;
								if_obj.Repair_Type_ID = resp[j].Repair_Type_ID;
								if_obj.Environment_Category_ID = resp[j].Environment_Category_ID;
								if_obj.EASA_AMC_2020_Level_ID = resp[j].EASA_AMC_2020_Level_ID;
								if_obj.Part_Criticity_ID = resp[j].Part_Criticity_ID;
								if_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								if_obj.Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								// for (var k = 0; k < if_array.length; k++) {
								// 	if_array[k].Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								// }
								if_array[index_obj.grandchildindex] = if_obj; //put back the inspection finding object to the array of inspection finding
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.childindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								objects[index_obj.parentindex] = ia_obj; //put back the inspection area object to the array of inspection area
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): inserted result returned');

							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								var resp = ress[0];
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								var ia_obj = objects[index_obj.parentindex]; //inspection area object (parent)
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.childindex]; //each inspection area cycle object (child) from the array of inspection area cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								var if_obj = if_array[index_obj.grandchildindex]; //each inspection finding object (grandchild) from the array of inspection finding type
								// append all the ids from the resp[j] to if_obj
								if_obj.Finding_Category_ID = resp[j].Finding_Category_ID;
								if_obj.Repair_Type_ID = resp[j].Repair_Type_ID;
								if_obj.Environment_Category_ID = resp[j].Environment_Category_ID;
								if_obj.EASA_AMC_2020_Level_ID = resp[j].EASA_AMC_2020_Level_ID;
								if_obj.Part_Criticity_ID = resp[j].Part_Criticity_ID;
								if_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								if_obj.Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								// for (var k = 0; k < if_array.length; k++) {
								// 	if_array[k].Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								// }
								if_array[index_obj.grandchildindex] = if_obj; //put back the inspection finding object to the array of inspection finding
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.childindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								objects[index_obj.parentindex] = ia_obj; //put back the inspection area object to the array of inspection area
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						} //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Finding====================================================== 
					}).catch((errors) => {
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning error');
						reject(errors)
					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionFinding

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionarea 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	add(inspectionarea, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid)');
			var self = this;

			self.checkEmptyFields(inspectionarea)
				.then(res => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning result from checkEmptyFields');

					self.addUpdateInspectionArea(inspectionarea, transact, transactionid)
						.then((res) => {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning result from addUpdateInspectionArea');
							//resolve(res);
							self.addUpdateInspectionAreaCycle(res, transact, transactionid)
								//self.addUpdateInspectionAreaFindingType(ress, transact, transactionid)
								.then((resp) => {
									console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
									//console.log(JSON.stringify(resp))
									self.addUpdateInspectionFinding(resp, transact, transactionid)
										.then((ressp) => {
											console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
											resolve(ressp);
										})
										.catch(error_0 => {
											console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_0);
											reject(error_0);
										});
								})
								.catch(error_1 => {
									console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
									reject(error_1);
								});

						}).catch((error_2) => {
							console.log(error_2);
							console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from addUpdateInspectionArea: ' + error_2);
							reject(error_2);
						});
				})
				.catch(error_3 => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_3);
					reject(error_3);
				});
		});
	}//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionarea 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	update(inspectionarea, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: update(inspectionarea, transact, transactionid)');
			var self = this;

			self.checkEmptyFields(inspectionarea)
				.then(res => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: update(inspectionarea, transact, transactionid): returning result from checkEmptyFields');

					self.addUpdateInspectionArea(inspectionarea, transact, transactionid)
						.then((res) => {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: update(inspectionarea, transact, transactionid): returning result from addUpdateInspectionArea');
							self.addUpdateInspectionAreaCycle(res, transact, transactionid)
								//self.addUpdateInspectionAreaFindingType(ress, transact, transactionid)
								.then((resp) => {
									console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
									//console.log(JSON.stringify(resp))
									self.addUpdateInspectionFinding(resp, transact, transactionid)
										.then((ressp) => {
											console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
											resolve(ressp);
										})
										.catch(error_0 => {
											console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_0);
											reject(error_0);
										});
								})
								.catch(error_1 => {
									console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
									reject(error_1);
								});

						}).catch((error_2) => {
							console.log(error_2);
							console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from addUpdateInspectionArea: ' + error_2);
							reject(error_2);
						});
				})
				.catch(error_3 => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: add(inspectionarea, transact, transactionid): returning error from checkEmptyFields: ' + error_3);
					reject(error_3);
				});
		});
	}//end update


    /**
	   * Get a specific Inspection Area
	   *
	   * @param {Integer} Inspection_Area_ID - E2E_Inspection_Area ID
	   * @param {String} Inspection_Area - E2E_Inspection_Area Value
   	   * @param {Integer} inspectionareaclone - Clone template to become another inspection area (0) or clone to become finding (1)
   	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
	get(param, search, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid)');
			console.log('(' + transactionid + '): parameter app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			if (param.index == null) {
				param.index = 0;
			} else {
				if (param.index > 0) {
					param.index = param.index - 1;
				}
			}

			if (param.row == null) {
				param.row = 30;
			}

			if (param.order == null) {
				param.order = 'DESC';
			}

			if (param.ordercolumn == null) {
				param.ordercolumn = 'Inspection_Area_ID';
			}

			//create the overall sql statements
			var sql_query =
				//' SELECT '+
				' ID as Inspection_Area_ID, ' +
				' Value as Inspection_Area ' +
				' FROM ' +
				' E2E_Inspection_Area ' +
				' WHERE deleted_at is null @inspectionareaid @inspectionareavalue ' +
				'   ORDER BY ' + param.ordercolumn + ' ' + param.order +
				'   LIMIT ' + param.index + ', ' + param.row +
				'	; ';

			var replace_query_inspectionareaid = ' AND ID in (@replace) ';
			var replace_query_inspectionareavalue = ' AND lcase(Value) in (@replace) ';

			if (param.Inspection_Area_ID != null) {
				sql_query = sql_query.replace('@inspectionareaid', replace_query_inspectionareaid.replace('@replace', param.Inspection_Area_ID));
			} else {
				sql_query = sql_query.replace('@inspectionareaid', '');
			}

			if (param.Inspection_Area != null) {
				var value = param.Inspection_Area;
				if (value.split(',').length > 1) {
					value = '\'' + value.replace(/,/g, "\',\'") + '\'';
				} else {
					value = '\'' + value + '\'';
				}
				sql_query = sql_query.replace('@inspectionareavalue', replace_query_inspectionareavalue.replace('@replace', value.toLowerCase()));
			} else {
				sql_query = sql_query.replace('@inspectionareavalue', '');
			}

			if (search == 0) { //proceed to return the sql string
				sql_query = ' Select ' + sql_query;
				resolve(sql_query);
			} else if (search == 1 || search == 2) { //proceed to search query with database
				if (search == 1) {//used for actual search requested by user
					sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + 'SELECT FOUND_ROWS();';
				} else if (search == 2) {//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					sql_query = ' Select ' + sql_query;
				}

				console.log('(' + transactionid + '): query app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
				//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
				db.sequelize.query(
					sql_query,
					{
						type: db.sequelize.QueryTypes.SELECT,
						useMaster: false
					})
					.then((res) => {
						var jsonobj = null;

						if (search == 2) {//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
							jsonobj = res;
							//resolve(res);
						}
						else if (search == 1) {//used for actual search requested by user
							//total number of rows from search result (without limit)
							var total_rows = res[1][0]["FOUND_ROWS()"];
							var loop_end_index;

							if (param.row < total_rows) {
								loop_end_index = param.row;
							} else if (total_rows < param.row) {
								loop_end_index = total_rows;
							} else {
								loop_end_index = total_rows;
							}

							var return_result = {};
							var final_result = [];

							for (var i = 0; i < loop_end_index; i++) {
								final_result.push(res[0][i]);
							}
							return_result.Result = final_result;
							return_result.Actual_Total_Records_Found = total_rows;

							jsonobj = return_result.Result;
							//resolve(return_result);
						}

						//put each finding into a finding array
						var counter = -1;
						jsonobj.map(a => {
							counter++;
							var inspectionareaplaceholder = {};

							if (param.inspectionareaclone != null) {
								if (param.inspectionareaclone == 1) {
									inspectionareaplaceholder.finding = [];
								} else {
									inspectionareaplaceholder = [];
								}
							} else {
								inspectionareaplaceholder = [];
							}


							if (a.childinspectionarea != null) {
								var inner_counter = -1;
								a.childinspectionarea.map(b => {
									inner_counter++;
									var inspectionareaplaceholder_inner = {};
									inspectionareaplaceholder_inner.finding = [];
									inspectionareaplaceholder_inner.finding.push(b);
									a.childinspectionarea[inner_counter] = inspectionareaplaceholder_inner;
								});
							}
							if (param.inspectionareaclone != null) {
								if ((param.inspectionareaclone == 0 || param.inspectionareaclone == 1) && search != 2) {
									delete a.inspectionareaid;
								}
							}

							//inspectionareaplaceholder.inspectionarea.push(a);
							//displays the data (ID and Value)
							if (param.inspectionareaclone != null) {
								if (param.inspectionareaclone == 1) {
									inspectionareaplaceholder.finding.push(a);
								} else {
									inspectionareaplaceholder.push(a);
								}
							} else {
								inspectionareaplaceholder.push(a);
							}

							jsonobj[counter] = inspectionareaplaceholder;
						}).join(",");

						if (search == 2) {//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid): returning result');
							resolve(jsonobj);
						} else if (search == 1) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid): returning result');
							if (jsonobj != 0) {
								return_result.Result = jsonobj;
								resolve(return_result);
							}
							else {
								console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid): returning result');
								return_result.Result = 'Not found';
								resolve(return_result);
							}
						}
					})
					.catch((error) => {
						console.log(error);
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
						reject(error);
					});
			}//end if (search == 0)
		});
	}//end get

	/**
	 * List all Inspection Area from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {

			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: list(transact, transactionid)');

			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
			db.E2E_Inspection_Area
				.findAll({
					attributes: [['ID', 'Inspection_Area_ID'], ['Value', 'Inspection_Area']],
					useMaster: false
				}
				)
				.then((res) => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: list(transact, transactionid): returning result');
					resolve(res);
				})
				.catch((error) => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: list(transact, transactionid): returning error: ' + error);
					reject(error);
				});
		});
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	remove(id, transact, transactionid) {
		return new Promise((resolve, reject) => {

			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionarea/lib/index.js: remove(id, transact, transactionid)');
			console.log('(' + transactionid + '): parameter app/controllers/api/e2e/inspectionarea/lib/index.js: remove(id,transact, transactionid): ' + id);
			//convert id to array
			var array_id = id.split(',');
			db.E2E_Inspection_Area
				.destroy({
					transaction: transact,
					useMaster: true,
					where: {
						ID: array_id
					}
				})
				.then((res) => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: remove(id, transact, transactionid): returning result');
					resolve(res);
				})
				.catch((error) => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionarea/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
					reject(error);
				});
		});
	}

	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} inspectionarea 
     * @param {String} transactionid 
     */
	checkEmptyFields(inspectionarea) {
		return new Promise((resolve, reject) => {
			var anyemptyfields = false;

			// inspectionarea.map(a =>{
			//     if (a.Inspection_Area == null){
			//         reject({'message':'Inspection_Area is mandatory field'});
			//     }
			// });

			resolve(0);
		});
	}
}

module.exports = InspectionArea;
