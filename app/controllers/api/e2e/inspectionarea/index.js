'use strict';

const InspectionArea = require('./lib');

const inspectionarea = new InspectionArea();
const classfilepath = 'app/controllers/api/e2e/inspectionarea/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionArea:
 *     type: object
 *     required:
 *       - inspectionareaid
 *       - inspectionareavalue
 *     properties:
 *       inspectionareaid:
 *         type: integer
 *       inspectionareavalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionarea:
   *   post:
   *     summary: Add a new Inspection Area
   *     description: Add a new Inspection Area as a JSON object
   *     tags:
   *       - InspectionArea
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionArea"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionarea', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionarea');
 
      return(
        inspectionarea
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionarea: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionarea: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionarea: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionarea: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionarea/all:
   *   get:
   *     summary: List all Inspection Area (not recommended)
   *     description: List all Inspection Area as an JSON array
   *     tags:
   *       - InspectionArea
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionArea"
   */
  app.get('/api/e2e/inspectionarea/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionarea/all');

      inspectionarea
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionarea/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionarea/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionarea:
   *   get:
   *     summary: Get a Inspection Area based on parameters
   *     description: Get a Inspection Area based on parameters.
   *     tags:
   *       - InspectionArea
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionareaid
   *         in: path
   *         description: "Inspection Area ID"
   *         required: true
   *         type: integer
   *       - name: inspectionareavalue
   *         in: path
   *         description: "Inspection Area Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionarea', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionarea');

     inspectionarea
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionarea: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionarea: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionarea: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionarea/{id}:
   *   delete:
   *     summary: Removes a Inspection Area
   *     description: Removes a Inspection Area
   *     tags:
   *       - InspectionArea
   *     parameters:
   *       - name: inspectionareaid
   *         in: path
   *         description: "Inspection Area id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionarea/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionarea');

		  return inspectionarea
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionarea: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionarea: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionarea: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionarea: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionarea: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionarea: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionarea:
   *   patch:
   *     summary: Update a Inspection Area
   *     description: Update a Inspection Area
   *     tags:
   *       - InspectionArea
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionArea"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionarea', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionarea');

		  return inspectionarea
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionarea: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionarea: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionarea: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionarea: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionarea: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionarea: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
