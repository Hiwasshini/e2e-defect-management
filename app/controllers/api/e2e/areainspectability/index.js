'use strict';

const AreaInspectability = require('./lib');

const areainspectability = new AreaInspectability();
const classfilepath = 'app/controllers/api/e2e/areainspectability/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   AreaInspectability:
 *     type: object
 *     required:
 *       - areainspectabilityid
 *       - areainspectabilityvalue
 *     properties:
 *       areainspectabilityid:
 *         type: integer
 *       areainspectabilityvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/areainspectability:
   *   post:
   *     summary: Add a new Area Inspectability
   *     description: Add a new Area Inspectability as a JSON object
   *     tags:
   *       - AreaInspectability
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Area Inspectability object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/AreaInspectability"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/areainspectability', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/areainspectability');
 
      return(
        areainspectability
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/areainspectability: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/areainspectability: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/areainspectability: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/areainspectability: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/areainspectability/all:
   *   get:
   *     summary: List all Area Inspectability (not recommended)
   *     description: List all Area Inspectability as an JSON array
   *     tags:
   *       - AreaInspectability
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/AreaInspectability"
   */
  app.get('/api/e2e/areainspectability/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/areainspectability/all');

      areainspectability
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/areainspectability/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/areainspectability/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/areainspectability:
   *   get:
   *     summary: Get a Area Inspectability based on parameters
   *     description: Get a Area Inspectability based on parameters.
   *     tags:
   *       - AreaInspectability
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: areainspectabilityid
   *         in: path
   *         description: "Area Inspectability ID"
   *         required: true
   *         type: integer
   *       - name: areainspectabilityvalue
   *         in: path
   *         description: "Area Inspectability Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/areainspectability', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/areainspectability');

     areainspectability
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/areainspectability: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/areainspectability: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/areainspectability: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/areainspectability/{id}:
   *   delete:
   *     summary: Removes a Area Inspectability
   *     description: Removes a Area Inspectability
   *     tags:
   *       - AreaInspectability
   *     parameters:
   *       - name: areainspectabilityid
   *         in: path
   *         description: "Area Inspectability id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/areainspectability/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/areainspectability');

		  return areainspectability
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/areainspectability: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/areainspectability: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/areainspectability: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/areainspectability: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/areainspectability: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/areainspectability: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/areainspectability:
   *   patch:
   *     summary: Update a Area Inspectability
   *     description: Update a Area Inspectability
   *     tags:
   *       - AreaInspectability
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Area Inspectability object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/AreaInspectability"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/areainspectability', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/areainspectability');

		  return areainspectability
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/areainspectability: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/areainspectability: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/areainspectability: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/areainspectability: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/areainspectability: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/areainspectability: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
