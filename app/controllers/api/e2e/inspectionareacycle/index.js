'use strict';

const InspectionAreaCycle = require('./lib');

const inspectionareacycle = new InspectionAreaCycle();
const classfilepath = 'app/controllers/api/e2e/inspectionareacycle/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionAreaCycle:
 *     type: object
 *     required:
 *       - inspectionareacycleid
 *       - inspectionareacyclevalue
 *     properties:
 *       inspectionareacycleid:
 *         type: integer
 *       inspectionareacyclevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionareacycle:
   *   post:
   *     summary: Add a new Inspection Area Cycle
   *     description: Add a new Inspection Area Cycle as a JSON object
   *     tags:
   *       - InspectionAreaCycle
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area Cycle object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionAreaCycle"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionareacycle', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionareacycle');
 
      return(
        inspectionareacycle
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionareacycle: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionareacycle: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionareacycle: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionareacycle: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionareacycle/all:
   *   get:
   *     summary: List all Inspection Area Cycle (not recommended)
   *     description: List all Inspection Area Cycle as an JSON array
   *     tags:
   *       - InspectionAreaCycle
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionAreaCycle"
   */
  app.get('/api/e2e/inspectionareacycle/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionareacycle/all');

      inspectionareacycle
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionareacycle/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionareacycle/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionareacycle:
   *   get:
   *     summary: Get a Inspection Area Cycle based on parameters
   *     description: Get a Inspection Area Cycle based on parameters.
   *     tags:
   *       - InspectionAreaCycle
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionareacycleid
   *         in: path
   *         description: "Inspection Area Cycle ID"
   *         required: true
   *         type: integer
   *       - name: inspectionareacyclevalue
   *         in: path
   *         description: "Inspection Area Cycle Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionareacycle', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionareacycle');

     inspectionareacycle
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareacycle: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionareacycle: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionareacycle: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionareacycle/{id}:
   *   delete:
   *     summary: Removes a Inspection Area Cycle
   *     description: Removes a Inspection Area Cycle
   *     tags:
   *       - InspectionAreaCycle
   *     parameters:
   *       - name: inspectionareacycleid
   *         in: path
   *         description: "Inspection Area Cycle id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionareacycle/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionareacycle');

		  return inspectionareacycle
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionareacycle: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionareacycle:
   *   patch:
   *     summary: Update a Inspection Area Cycle
   *     description: Update a Inspection Area Cycle
   *     tags:
   *       - InspectionAreaCycle
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area Cycle object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionAreaCycle"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionareacycle', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionareacycle');

		  return inspectionareacycle
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionareacycle: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
