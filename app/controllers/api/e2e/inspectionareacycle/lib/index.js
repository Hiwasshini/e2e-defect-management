'use strict';

const appRoot = require('app-root-path');

const db      = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/inspectionareacycle/lib/index.js';
const  mergeJSON  = require ("merge-json") ;
var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

/**
 * This is a sub-super API that help to insert the following: 
 * 1. 		Inspection_Area_Cycle
 * 2. 			|_Inspection_Finding
 * 
 * There is a need to retrieve the ID for the following tables and append to the existing data before insertion into the database
 * 1. Inspection_Area_Cycle
 *		a. Equipment_Type
 *		b. Airline
 * 2. Inspection_Finding
 *		a. Finding_Type
 *		b. Part_Criticity
 *		c. Repair_Type
 *		d. Finding_Category
 *		e. Environment_Category
 *		f. EASA_AMC_2020_Level
 */

//This API can only POST/PATCH data to itself, yet to POST/PATCH to subsequent table
//Need to work on GET and Remove functions

/**
 * Class that represents Inspection Area Cycle through database
 */
class InspectionAreaCycle {

    /**
     * Assist the add() to insert/update records into E2E_Inspection_Area_Cycle table
     * @param {JSON} inspectionareacycle 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    addUpdateInspectionAreaCycle(inspectionareacycle,transact, transactionid){
		return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid)');
	
			var objects = inspectionareacycle;
			//====START======================Handle the Inspection Area Cycle======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			  var combine_insert = [];
			  var prom_update;
			  var prom_insert;
			  var map_record_update = [];
			  var map_record_insert = [];
			  
			  var str = JSON.stringify(objects);
												
		  //find all section that requires update only
			  var parentcounter =-1;
			  
				var json_obj = objects;
				var tempval;
				
				//for update & insert
				json_obj.map(a =>{
					parentcounter++;
					if (a.Inspection_Area_Cycle_ID != null){	
						var index_obj = {};
						index_obj.parentindex = parentcounter;
						map_record_update.push(index_obj);
						
						var inspectionareacycle_obj = {};
						if (a.Inspection_Area_Cycle_ID != null){
							inspectionareacycle_obj.ID = a.Inspection_Area_Cycle_ID;
						}
						if (a.Aircraft_Model != null){
							inspectionareacycle_obj.Aircraft_Model = a.Aircraft_Model;
						}
						if (a.MSN != null){
							inspectionareacycle_obj.MSN = a.MSN;
						}
						if (a.Entry_Into_Service != null){
							inspectionareacycle_obj.Entry_Into_Service = a.Entry_Into_Service;
						}
						if (a.Manufacturing_Date != null){
							inspectionareacycle_obj.Manufacturing_Date = a.Manufacturing_Date;
						}
						if (a.Airline_ID != null){
							inspectionareacycle_obj.Airline_ID = a.Airline_ID;
						}
						if (a.Geographical_Area != null){
							inspectionareacycle_obj.Geographical_Area = a.Geographical_Area;
						}
						if (a.Flying_Cycle != null){
							inspectionareacycle_obj.Flying_Cycle = a.Flying_Cycle;
						}
						if (a.Flying_Hours != null){
							inspectionareacycle_obj.Flying_Hours = a.Flying_Hours;
						}
						if (a.Inspection_Campaign != null){
							inspectionareacycle_obj.Inspection_Campaign = a.Inspection_Campaign;
						}
						if (a.Campaign_Date != null){
							inspectionareacycle_obj.Campaign_Date = a.Campaign_Date;
						}
						if (a.Finding_Type_ID != null){
							inspectionareacycle_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						if (a.Name_of_Equipment != null){
							inspectionareacycle_obj.Name_of_Equipment = a.Name_of_Equipment;
						}
						if (a.Equipment_Serial_Number != null){
							inspectionareacycle_obj.Equipment_Serial_Number = a.Equipment_Serial_Number;
						}
						if (a.Calibration_Due_Date != null){
							inspectionareacycle_obj.Calibration_Due_Date = a.Calibration_Due_Date;
						}
						if (a.Calibration_Specimen != null){
							inspectionareacycle_obj.Calibration_Specimen = a.Calibration_Specimen;
						}
						if (a.Calibration_Procedure != null){
							inspectionareacycle_obj.Calibration_Procedure = a.Calibration_Procedure;
						}
						if (a.Inspection_Area_ID != null){
							inspectionareacycle_obj.Inspection_Area_ID = a.Inspection_Area_ID;
						}
						if (a.delete != null){
							inspectionareacycle_obj.deleted_at = commonModule.currentDateTime();
						}
						combine_update.push(inspectionareacycle_obj);
					}else if (a.Inspection_Area_Cycle_ID == null){	          							
						var index_obj = {};
						index_obj.parentindex = parentcounter;
						map_record_insert.push(index_obj);
						
						var inspectionareacycle_obj = {};
						if (a.Inspection_Area_Cycle_ID != null){
							inspectionareacycle_obj.ID = a.Inspection_Area_Cycle_ID;
						}
						if (a.Aircraft_Model != null){
							inspectionareacycle_obj.Aircraft_Model = a.Aircraft_Model;
						}
						if (a.MSN != null){
							inspectionareacycle_obj.MSN = a.MSN;
						}
						if (a.Entry_Into_Service != null){
							inspectionareacycle_obj.Entry_Into_Service = a.Entry_Into_Service;
						}
						if (a.Manufacturing_Date != null){
							inspectionareacycle_obj.Manufacturing_Date = a.Manufacturing_Date;
						}
						if (a.Airline_ID != null){
							inspectionareacycle_obj.Airline_ID = a.Airline_ID;
						}
						if (a.Geographical_Area != null){
							inspectionareacycle_obj.Geographical_Area = a.Geographical_Area;
						}
						if (a.Flying_Cycle != null){
							inspectionareacycle_obj.Flying_Cycle = a.Flying_Cycle;
						}
						if (a.Flying_Hours != null){
							inspectionareacycle_obj.Flying_Hours = a.Flying_Hours;
						}
						if (a.Inspection_Campaign != null){
							inspectionareacycle_obj.Inspection_Campaign = a.Inspection_Campaign;
						}
						if (a.Campaign_Date != null){
							inspectionareacycle_obj.Campaign_Date = a.Campaign_Date;
						}
						if (a.Finding_Type_ID != null){
							inspectionareacycle_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						if (a.Name_of_Equipment != null){
							inspectionareacycle_obj.Name_of_Equipment = a.Name_of_Equipment;
						}
						if (a.Equipment_Serial_Number != null){
							inspectionareacycle_obj.Equipment_Serial_Number = a.Equipment_Serial_Number;
						}
						if (a.Calibration_Due_Date != null){
							inspectionareacycle_obj.Calibration_Due_Date = a.Calibration_Due_Date;
						}
						if (a.Calibration_Specimen != null){
							inspectionareacycle_obj.Calibration_Specimen = a.Calibration_Specimen;
						}
						if (a.Calibration_Procedure != null){
							inspectionareacycle_obj.Calibration_Procedure = a.Calibration_Procedure;
						}
						if (a.Inspection_Area_ID != null){
							inspectionareacycle_obj.Inspection_Area_ID = a.Inspection_Area_ID;
						}
						combine_insert.push(inspectionareacycle_obj);
					}
				});
	
			  if (combine_update.length > 0){
				prom_update = new Promise(function(resolved, rejected) {
					db.E2E_Inspection_Area_Cycle
					.bulkCreate(combine_update, { 
							updateOnDuplicate: ["Name","Aircraft_Model","MSN","Entry_Into_Service","Manufacturing_Date",
							"Airline_ID","Geographical_Area","Flying_Cycle","Flying_Hours","Inspection_Campaign","Campaign_Date",
							"Finding_Type_ID", "Name_of_Equipment","Equipment_Serial_Number","Calibration_Due_Date",
							"Calibration_Specimen","Calibration_Procedure","Inspection_Area_ID","deleted_at"], 
							transaction: transact,
							raw: true,
							useMaster: true
						  })
					.then((result) => {
						var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Area_Cycle_ID\"")
								.replace(/"Aircraft_Model"/g, "\"Aircraft_Model\"")
								.replace(/"MSN"/g, "\"MSN\"")
								.replace(/"Entry_Into_Service"/g, "\"Entry_Into_Service\"")
								.replace(/"Manufacturing_Date"/g, "\"Manufacturing_Date\"")
								.replace(/"Airline_ID"/g, "\"Airline_ID\"")
								.replace(/"Geographical_Area"/g, "\"Geographical_Area\"")
								.replace(/"Flying_Cycle"/g, "\"Flying_Cycle\"")
								.replace(/"Flying_Hours"/g, "\"Flying_Hours\"")
								.replace(/"Inspection_Campaign"/g, "\"Inspection_Campaign\"")
								.replace(/"Campaign_Date"/g, "\"Campaign_Date\"")
								.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\"")
								.replace(/"Name_of_Equipment"/g, "\"Name_of_Equipment\"")
								.replace(/"Equipment_Serial_Number"/g, "\"Equipment_Serial_Number\"")
								.replace(/"Calibration_Due_Date"/g, "\"Calibration_Due_Date\"")
								.replace(/"Calibration_Specimen"/g, "\"Calibration_Specimen\"")
								.replace(/"Calibration_Procedure"/g, "\"Calibration_Procedure\"")
								.replace(/"Inspection_Area_ID"/g, "\"Inspection_Area_ID\"")); 
						for (var i=0;i<json_objs.length;i++){
							json_objs[i].message = 'Inspection Area Cycle updated successfully.';
						}
						resolved(json_objs);
					}).catch((errors) => {
						rejected(errors);
					});
				});
				promises_inner.push(prom_update);
			  }//end if (combine_part_update.length > 0)
				
			  if (combine_insert.length > 0){
				prom_insert = new Promise(function(resolved, rejected) {
				db.E2E_Inspection_Area_Cycle
				.bulkCreate(combine_insert, { 
					individualHooks: true, 
					transaction: transact,
					raw: true,
					useMaster: true })
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Area_Cycle_ID\"")
							.replace(/"Aircraft_Model"/g, "\"Aircraft_Model\"")
							.replace(/"MSN"/g, "\"MSN\"")
							.replace(/"Entry_Into_Service"/g, "\"Entry_Into_Service\"")
							.replace(/"Manufacturing_Date"/g, "\"Manufacturing_Date\"")
							.replace(/"Airline_ID"/g, "\"Airline_ID\"")
							.replace(/"Geographical_Area"/g, "\"Geographical_Area\"")
							.replace(/"Flying_Cycle"/g, "\"Flying_Cycle\"")
							.replace(/"Flying_Hours"/g, "\"Flying_Hours\"")
							.replace(/"Inspection_Campaign"/g, "\"Inspection_Campaign\"")
							.replace(/"Campaign_Date"/g, "\"Campaign_Date\"")
							.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\"")
							.replace(/"Name_of_Equipment"/g, "\"Name_of_Equipment\"")
							.replace(/"Equipment_Serial_Number"/g, "\"Equipment_Serial_Number\"")
							.replace(/"Calibration_Due_Date"/g, "\"Calibration_Due_Date\"")
							.replace(/"Calibration_Specimen"/g, "\"Calibration_Specimen\"")
							.replace(/"Calibration_Procedure"/g, "\"Calibration_Procedure\"")
							.replace(/"Inspection_Area_ID"/g, "\"Inspection_Area_ID\"")); 
							for (var i=0;i<json_objs.length;i++){
								json_objs[i].message = 'Inspection Area Cycle inserted successfully.';
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						  }); 
				  });
				promises_inner.push(prom_insert);
			  }//end if (combine_part_insert.length > 0)
			  
			if (promises_inner.length > 0){
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
				.then(function (ress) {
				ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
					
				  
				  if (combine_update.length > 0){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): updated result returned');
	
					  var resp = ress[0];   
					  for (var j=0;j<resp.length;j++){  
						var index_obj =  map_record_update[j]; 	
						  var obj_new = JSON.parse(JSON.stringify(resp[j]));
				   
						//update the obj_new to the index of the old element
						  objects[index_obj.parentindex] = obj_new;
					  }//end for (var j=0;j<resp.length;j++)
					  ress.splice(0, 1);
				  }
				  if (combine_insert.length > 0){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): inserted result returned');
	
					var resp = ress[0]; 
					for (var j=0;j<resp.length;j++){  
					  var index_obj =  map_record_insert[j]; 	
						var obj_new = JSON.parse(JSON.stringify(resp[j]));
				 
					  //update the obj_new to the index of the old element
						objects[index_obj.parentindex] = obj_new;
					}//end for (var j=0;j<resp.length;j++)
					  ress.splice(0, 1);
				  }
				  
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
				  resolve(objects);
	//====END======================Handle the Inspection Area Cycle====================================================== 
				  }).catch((errors) => {
					  console.log(errors)
					var error_code = errors.errors[0];
					error_code = error_code.type;
	
					var msg
					if (error_code == 'unique violation'){
					  msg = {'message':'Duplicate Inspection Area Cycle value found. Pls ensure that Inspection Area Cycle value is unqiue.'};
					}else{
						msg = errors;
					}
					console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning error');
					reject(msg);
				  }); //end return Promise.all(promises_inner)
			}else{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)
				
		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionAreaCycle
    
    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionareacycle 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    add(inspectionareacycle, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionareacycle/lib/index.js: add(inspectionareacycle, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(inspectionareacycle) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateInspectionAreaCycle(inspectionareacycle,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
					resolve(res);
//console.log(res)
                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: add(inspectionareacycle, transact, transactionid): returning error from addUpdateInspectionAreaCycle: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: add(inspectionareacycle, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionareacycle 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    update(inspectionareacycle, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionareacycle/lib/index.js: update(inspectionareacycle, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(inspectionareacycle) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: update(inspectionareacycle, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateInspectionAreaCycle(inspectionareacycle,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: update(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: update(inspectionareacycle, transact, transactionid): returning error from addUpdateInspectionAreaCycle: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: update(inspectionareacycle, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end update


    /**
	   * Get a specific Inspection Area Cycle
	   *
	   * @param {Integer} Inspection_Area_Cycle_ID - E2E_Inspection_Area_Cycle ID
	   * @param {String} Inspection_Area_Cycle - E2E_Inspection_Area_Cycle Value
   	   * @param {Integer} inspectionareacycleclone - Clone template to become another inspection area cycle (0) or clone to become finding (1)
   	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
    get(param,search,transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid)');
			console.log('('+ transactionid +'): parameter app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			if (param.index == null){
                param.index = 0;
            }else{
                if (param.index > 0){
                    param.index = param.index -1;
                }
            }

            if (param.row == null){
                param.row = 30;
            }
            
            if (param.order == null){
                param.order = 'DESC';
            }

            if (param.ordercolumn == null){
                param.ordercolumn = 'Inspection_Area_Cycle_ID';
            }

            //create the overall sql statements
			var sql_query =
			//' SELECT '+
			' ID as Inspection_Area_Cycle_ID, '+
			' Value as Inspection_Area_Cycle '+
			' FROM '+
			' E2E_Inspection_Area_Cycle '+			
			' WHERE deleted_at is null @inspectionareacycleid @inspectionareacyclevalue ' +
			'   ORDER BY ' + param.ordercolumn + ' ' + param.order  +
			'   LIMIT ' + param.index + ', ' + param.row +
			'	; ';

            var replace_query_inspectionareacycleid = ' AND ID in (@replace) ';
            var replace_query_inspectionareacyclevalue = ' AND lcase(Value) in (@replace) ';

            if (param.Inspection_Area_Cycle_ID != null){   
				sql_query = sql_query.replace('@inspectionareacycleid', replace_query_inspectionareacycleid.replace('@replace',param.Inspection_Area_Cycle_ID));
            }else{
				sql_query = sql_query.replace('@inspectionareacycleid', '');
            }

            if (param.Inspection_Area_Cycle != null){
                var value = param.Inspection_Area_Cycle;
                if (value.split(',').length > 1){
                    value = '\'' + value.replace(/,/g, "\',\'") + '\'';
                }else{
                    value = '\'' + value + '\'';
                }    
                sql_query = sql_query.replace('@inspectionareacyclevalue', replace_query_inspectionareacyclevalue.replace('@replace',value.toLowerCase()));
            }else{
                sql_query = sql_query.replace('@inspectionareacyclevalue', '');
			}
			
			if (search == 0){ //proceed to return the sql string
				sql_query = ' Select ' + sql_query ;
			resolve(sql_query);			 
			}else if (search == 1 || search == 2){ //proceed to search query with database
				if (search == 1){//used for actual search requested by user
					sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + 'SELECT FOUND_ROWS();';
				}else if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					sql_query = ' Select ' + sql_query;
				}
				
			console.log('('+ transactionid +'): query app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.sequelize.query(
                sql_query,
                {
					type: db.sequelize.QueryTypes.SELECT,
					useMaster: false
                })
                .then((res) => {
                    var jsonobj = null;
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					jsonobj = res;
    				//resolve(res);
    			}
    			else if (search == 1){//used for actual search requested by user
					//total number of rows from search result (without limit)
    				var total_rows = res[1][0]["FOUND_ROWS()"];
    				var loop_end_index;
    				
    				if (param.row < total_rows){
    					loop_end_index = param.row;
    				}else if (total_rows < param.row){
    					loop_end_index = total_rows;
    				}else{
    					loop_end_index = total_rows;
    				}
    				
    				var return_result = {};
					var final_result = [];
					
    				for (var i=0;i<loop_end_index;i++){
    					final_result.push(res[0][i]);
    				}
    				return_result.Result = final_result;
    				return_result.Actual_Total_Records_Found = total_rows;

    				jsonobj = return_result.Result;
    				//resolve(return_result);
				}
				
				//put each finding into a finding array
	        	var counter = -1;
	        	jsonobj.map(a =>{
	        		counter++;
	        		var inspectionareacycleplaceholder = {};
	        		
	        		if (param.inspectionareacycleclone != null){
	            		if (param.inspectionareacycleclone == 1){
	            			inspectionareacycleplaceholder.finding = [];
	            		}else{
	            			inspectionareacycleplaceholder = [];
	            		}
	            	}else{
	            		inspectionareacycleplaceholder = [];
	            	}
	        		
	        		    	    	    	        		
	        		 if (a.childinspectionareacycle != null){
	        		 	var inner_counter = -1;
	        		 	a.childinspectionareacycle.map(b =>{
	        		 		inner_counter++;
	        		 		var inspectionareacycleplaceholder_inner = {};
	        		 		inspectionareacycleplaceholder_inner.finding = [];
	        		 		inspectionareacycleplaceholder_inner.finding.push(b);
	        		 		a.childinspectionareacycle[inner_counter] = inspectionareacycleplaceholder_inner;	  
	        		 	});
	        		 }
	        		if (param.inspectionareacycleclone != null){
	            		if ( (param.inspectionareacycleclone == 0 || param.inspectionareacycleclone == 1) && search != 2){
	            			delete a.inspectionareacycleid;
	            		}
	        		}
	        		
					//inspectionareacycleplaceholder.inspectionareacycle.push(a);
					//displays the data (ID and Value)
	        		if (param.inspectionareacycleclone != null){
	            		 if (param.inspectionareacycleclone == 1){
	            			inspectionareacycleplaceholder.finding.push(a);
	            		}else{
	            			inspectionareacycleplaceholder.push(a);
	            		}
	            	}else{
	            		inspectionareacycleplaceholder.push(a);
	            	}
	        		
	        		jsonobj[counter] = inspectionareacycleplaceholder;	  
	    		}).join(",");
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid): returning result');
					resolve(jsonobj);
    			}else if (search == 1){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid): returning result');
					if (jsonobj != 0){
					return_result.Result = jsonobj;
					resolve(return_result);
					}
					else{
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid): returning result');
					return_result.Result = 'Not found';
					resolve(return_result);
					}
					}
    		})
                .catch((error) => {
					console.log(error);
					console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
                    reject(error);
                });
			}//end if (search == 0)
        });
	}//end get

	/**
	 * List all Inspection Area Cycle from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {
        
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionareacycle/lib/index.js: list(transact, transactionid)');
			
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.E2E_Inspection_Area_Cycle
                .findAll({
						attributes: [['ID', 'Inspection_Area_Cycle_ID'], ['Value', 'Inspection_Area_Cycle']],
						useMaster: false
                    }
                )
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: list(transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: list(transact, transactionid): returning error: ' + error);
                    reject(error);
                });
            });
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    remove(id,transact, transactionid) {
        return new Promise((resolve, reject) => {

            console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionareacycle/lib/index.js: remove(id, transact, transactionid)');
            console.log('('+ transactionid +'): parameter app/controllers/api/e2e/inspectionareacycle/lib/index.js: remove(id,transact, transactionid): ' + id);
            //convert id to array
            var array_id = id.split(',');
            db.E2E_Inspection_Area_Cycle
                .destroy({
					transaction: transact,
					useMaster: true,
                    where : {
                        ID : array_id
                    }
                })
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: remove(id, transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionareacycle/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
                    reject(error);
                });
        });
	}
	
	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} inspectionareacycle 
     * @param {String} transactionid 
     */
    checkEmptyFields(inspectionareacycle){
        return new Promise((resolve, reject) => {
            var anyemptyfields = false;

            // inspectionareacycle.map(a =>{
            //     if (a.Inspection_Area_Cycle == null){
            //         reject({'message':'Inspection_Area_Cycle is mandatory field'});
            //     }
           // });

            resolve(0);
        });
    }
}

module.exports = InspectionAreaCycle;
