'use strict';

const FindingType = require('./lib');

const findingtype = new FindingType();
const classfilepath = 'app/controllers/api/e2e/findingtype/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   FindingType:
 *     type: object
 *     required:
 *       - findingtypeid
 *       - findingtypevalue
 *     properties:
 *       findingtypeid:
 *         type: integer
 *       findingtypevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/findingtype:
   *   post:
   *     summary: Add a new Finding Type
   *     description: Add a new Finding Type as a JSON object
   *     tags:
   *       - FindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Finding Type object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/FindingType"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/findingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/findingtype');
 
      return(
        findingtype
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/findingtype: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/findingtype: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/findingtype: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/findingtype: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/findingtype/all:
   *   get:
   *     summary: List all Finding Type (not recommended)
   *     description: List all Finding Type as an JSON array
   *     tags:
   *       - FindingType
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/FindingType"
   */
  app.get('/api/e2e/findingtype/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/findingtype/all');

      findingtype
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/findingtype/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/findingtype/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/findingtype/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/findingtype/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/findingtype:
   *   get:
   *     summary: Get a Finding Type based on parameters
   *     description: Get a Finding Type based on parameters.
   *     tags:
   *       - FindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: findingtypeid
   *         in: path
   *         description: "Finding Type ID"
   *         required: true
   *         type: integer
   *       - name: findingtypevalue
   *         in: path
   *         description: "Finding Type Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/findingtype', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/findingtype');

     findingtype
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/findingtype: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/findingtype: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/findingtype: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/findingtype: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/findingtype: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/findingtype: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/findingtype/{id}:
   *   delete:
   *     summary: Removes a Finding Type
   *     description: Removes a Finding Type
   *     tags:
   *       - FindingType
   *     parameters:
   *       - name: findingtypeid
   *         in: path
   *         description: "Finding Type id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/findingtype/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/findingtype');

		  return findingtype
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/findingtype: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/findingtype: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/findingtype: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/findingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/findingtype: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/findingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/findingtype:
   *   patch:
   *     summary: Update a Finding Type
   *     description: Update a Finding Type
   *     tags:
   *       - FindingType
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Finding Type object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/FindingType"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/findingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/findingtype');

		  return findingtype
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/findingtype: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/findingtype: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/findingtype: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/findingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/findingtype: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/findingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
