'use strict';

const appRoot = require('app-root-path');

const db      = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/inspectionfinding/lib/index.js';
const  mergeJSON  = require ("merge-json") ;
var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

//Need to work on GET function

/**
 * Class that represents Inspection Finding through database
 */
class InspectionFinding {

    /**
     * Assist the add() to insert/update records into E2E_Inspection_Finding table
     * @param {JSON} inspectionfinding 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    addUpdateInspectionFinding(inspectionfinding,transact, transactionid){
		return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid)');
	
			var objects = inspectionfinding;
			//====START======================Handle the Inspection Finding======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			  var combine_insert = [];
			  var prom_update;
			  var prom_insert;
			  var map_record_update = [];
			  var map_record_insert = [];
			  
			  var str = JSON.stringify(objects);
												
		  //find all section that requires update only
			  var parentcounter =-1;
			  
				var json_obj = objects;
				var tempval;
				
				//for update & insert
				json_obj.map(a =>{
					parentcounter++;
					if (a.Inspection_Finding_ID != null){	
						var index_obj = {};
						index_obj.parentindex = parentcounter;
						map_record_update.push(index_obj);
						
						var inspectionfinding_obj = {};
						if (a.Inspection_Finding_ID != null){
							inspectionfinding_obj.ID = a.Inspection_Finding_ID;
						}
						if (a.IP_Name != null){
							inspectionfinding_obj.IP_Name = a.IP_Name;
						} 
						if (a.Finding_Date != null){
							inspectionfinding_obj.Finding_Date = a.Finding_Date;
						}
						if (a.Flying_Cycle_Real != null){
							inspectionfinding_obj.Flying_Cycle_Real = a.Flying_Cycle_Real;
						}
						if (a.Finding_Type_ID != null){
							inspectionfinding_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						if (a.Number_Spot != null){
							inspectionfinding_obj.Number_Spot = a.Number_Spot;
						}
						if (a.Length != null){
							inspectionfinding_obj.Length = a.Length;
						}
						if (a.Width != null){
							inspectionfinding_obj.Width = a.Width;
						}
						if (a.Surface != null){
							inspectionfinding_obj.Surface = a.Surface;
						}
						if (a.Orientation != null){
							inspectionfinding_obj.Orientation = a.Orientation;
						}
						if (a.Angle_Reference != null){
							inspectionfinding_obj.Angle_Reference = a.Angle_Reference;
						}
						if (a.Depth != null){
							inspectionfinding_obj.Depth = a.Depth;
						}
						if (a.Starting_Depth != null){
							inspectionfinding_obj.Starting_Depth = a.Starting_Depth;
						}
						if (a.Ending_Depth != null){
							inspectionfinding_obj.Ending_Depth = a.Ending_Depth;
						}
						if (a.Layer != null){
							inspectionfinding_obj.Layer = a.Layer;
						}
						if (a.Remaining_Thickness != null){
							inspectionfinding_obj.Remaining_Thickness = a.Remaining_Thickness;
						}
						if (a.Rotation_Angle != null){
							inspectionfinding_obj.Rotation_Angle = a.Rotation_Angle;
						}
						if (a.Comments != null){
							inspectionfinding_obj.Comments = a.Comments;
						}
						if (a.Repair_Type_ID != null){
							inspectionfinding_obj.Repair_Type_ID = a.Repair_Type_ID;
						}
						if (a.Repair_Reference != null){
							inspectionfinding_obj.Repair_Reference = a.Repair_Reference;
						}
						if (a.Part_Number != null){
							inspectionfinding_obj.Part_Number = a.Part_Number;
						}
						if (a.Part_Serial_Number != null){
							inspectionfinding_obj.Part_Serial_Number = a.Part_Serial_Number;
						}
						if (a.Environment_Category_ID != null){
							inspectionfinding_obj.Environment_Category_ID = a.Environment_Category_ID;
						}
						if (a.EASA_AMC_2020_Level_ID != null){
							inspectionfinding_obj.EASA_AMC_2020_Level_ID = a.EASA_AMC_2020_Level_ID;
						}
						if (a.SSI_Reference != null){
							inspectionfinding_obj.SSI_Reference = a.SSI_Reference;
						}
						if (a.RMT_File != null){
							inspectionfinding_obj.RMT_File = a.RMT_File;
						}
						if (a.RDAS_Reference != null){
							inspectionfinding_obj.RDAS_Reference = a.RDAS_Reference;
						}
						if (a.Corrosion_Material_1 != null){
							inspectionfinding_obj.Corrosion_Material_1 = a.Corrosion_Material_1;
						}
						if (a.Corrosion_Material_2 != null){
							inspectionfinding_obj.Corrosion_Material_2 = a.Corrosion_Material_2;
						}
						if (a.Corrosion_Fastener_Specification != null){
							inspectionfinding_obj.Corrosion_Fastener_Specification = a.Corrosion_Fastener_Specification;
						}
						if (a.Part_Criticity_ID != null){
							inspectionfinding_obj.Part_Criticity_ID = a.Part_Criticity_ID;
						}
						if (a.Inspection_Area_Cycle_ID != null){
							inspectionfinding_obj.Inspection_Area_Cycle_ID = a.Inspection_Area_Cycle_ID;
						}
						if (a.Finding_Type_ID != null){
							inspectionfinding_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						if (a.delete != null){
							inspectionfinding_obj.deleted_at = commonModule.currentDateTime();
						}
						combine_update.push(inspectionfinding_obj);
					}else if (a.Inspection_Finding_ID == null){	          							
						var index_obj = {};
						index_obj.parentindex = parentcounter;
						map_record_insert.push(index_obj);
						
						var inspectionfinding_obj = {};
						if (a.Inspection_Finding_ID != null){
							inspectionfinding_obj.ID = a.Inspection_Finding_ID;
						}
						if (a.IP_Name != null){
							inspectionfinding_obj.IP_Name = a.IP_Name;
						} 
						if (a.Finding_Date != null){
							inspectionfinding_obj.Finding_Date = a.Finding_Date;
						}
						if (a.Flying_Cycle_Real != null){
							inspectionfinding_obj.Flying_Cycle_Real = a.Flying_Cycle_Real;
						}
						if (a.Finding_Type_ID != null){
							inspectionfinding_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						if (a.Number_Spot != null){
							inspectionfinding_obj.Number_Spot = a.Number_Spot;
						}
						if (a.Length != null){
							inspectionfinding_obj.Length = a.Length;
						}
						if (a.Width != null){
							inspectionfinding_obj.Width = a.Width;
						}
						if (a.Surface != null){
							inspectionfinding_obj.Surface = a.Surface;
						}
						if (a.Orientation != null){
							inspectionfinding_obj.Orientation = a.Orientation;
						}
						if (a.Angle_Reference != null){
							inspectionfinding_obj.Angle_Reference = a.Angle_Reference;
						}
						if (a.Depth != null){
							inspectionfinding_obj.Depth = a.Depth;
						}
						if (a.Starting_Depth != null){
							inspectionfinding_obj.Starting_Depth = a.Starting_Depth;
						}
						if (a.Ending_Depth != null){
							inspectionfinding_obj.Ending_Depth = a.Ending_Depth;
						}
						if (a.Layer != null){
							inspectionfinding_obj.Layer = a.Layer;
						}
						if (a.Remaining_Thickness != null){
							inspectionfinding_obj.Remaining_Thickness = a.Remaining_Thickness;
						}
						if (a.Rotation_Angle != null){
							inspectionfinding_obj.Rotation_Angle = a.Rotation_Angle;
						}
						if (a.Comments != null){
							inspectionfinding_obj.Comments = a.Comments;
						}
						if (a.Repair_Type_ID != null){
							inspectionfinding_obj.Repair_Type_ID = a.Repair_Type_ID;
						}
						if (a.Repair_Reference != null){
							inspectionfinding_obj.Repair_Reference = a.Repair_Reference;
						}
						if (a.Part_Number != null){
							inspectionfinding_obj.Part_Number = a.Part_Number;
						}
						if (a.Part_Serial_Number != null){
							inspectionfinding_obj.Part_Serial_Number = a.Part_Serial_Number;
						}
						if (a.Environment_Category_ID != null){
							inspectionfinding_obj.Environment_Category_ID = a.Environment_Category_ID;
						}
						if (a.EASA_AMC_2020_Level_ID != null){
							inspectionfinding_obj.EASA_AMC_2020_Level_ID = a.EASA_AMC_2020_Level_ID;
						}
						if (a.SSI_Reference != null){
							inspectionfinding_obj.SSI_Reference = a.SSI_Reference;
						}
						if (a.RMT_File != null){
							inspectionfinding_obj.RMT_File = a.RMT_File;
						}
						if (a.RDAS_Reference != null){
							inspectionfinding_obj.RDAS_Reference = a.RDAS_Reference;
						}
						if (a.Corrosion_Material_1 != null){
							inspectionfinding_obj.Corrosion_Material_1 = a.Corrosion_Material_1;
						}
						if (a.Corrosion_Material_2 != null){
							inspectionfinding_obj.Corrosion_Material_2 = a.Corrosion_Material_2;
						}
						if (a.Corrosion_Fastener_Specification != null){
							inspectionfinding_obj.Corrosion_Fastener_Specification = a.Corrosion_Fastener_Specification;
						}
						if (a.Part_Criticity_ID != null){
							inspectionfinding_obj.Part_Criticity_ID = a.Part_Criticity_ID;
						}
						if (a.Inspection_Area_Cycle_ID != null){
							inspectionfinding_obj.Inspection_Area_Cycle_ID = a.Inspection_Area_Cycle_ID;
						}
						if (a.Finding_Type_ID != null){
							inspectionfinding_obj.Finding_Type_ID = a.Finding_Type_ID;
						}
						combine_insert.push(inspectionfinding_obj);
					}
				});
	
			  if (combine_update.length > 0){
				prom_update = new Promise(function(resolved, rejected) {
					db.E2E_Inspection_Finding
					.bulkCreate(combine_update, { 
							updateOnDuplicate: ["Name","IP_Name","Finding_Date","Flying_Cycle_Real","Finding_Type_ID","Number_Spot",
							"Length","Width","Surface","Orientation","Angle_Reference","Depth","Starting_Depth","Ending_Depth","Layer",
							"Remaining_Thickness","Rotation_Angle","Comments","Repair_Type_ID","Repair_Reference","Part_Number","Part_Serial_Number",
							"Environment_Category_ID","EASA_AMC_2020_Level_ID","SSI_Reference","RMT_File","RDAS_Reference","Corrosion_Material_1",
							"Corrosion_Material_2","Corrosion_Fastener_Specification","Part_Criticity_ID","Inspection_Area_Cycle_ID","Finding_Type_ID","deleted_at"], 
							transaction: transact,
							raw: true,
							useMaster: true
						  })
					.then((result) => {
						var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Finding_ID\"")
								.replace(/"IP_Name"/g, "\"IP_Name\"")
								.replace(/"Finding_Date"/g, "\"Finding_Date\"")
								.replace(/"Flying_Cycle_Real"/g, "\"Flying_Cycle_Real\"")
								.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\"")
								.replace(/"Number_Spot"/g, "\"Number_Spot\"")
								.replace(/"Length"/g, "\"Length\"")
								.replace(/"Width"/g, "\"Width\"")
								.replace(/"Surface"/g, "\"Surface\"")
								.replace(/"Orientation"/g, "\"Orientation\"")
								.replace(/"Angle_Reference"/g, "\"Angle_Reference\"")
								.replace(/"Depth"/g, "\"Depth\"")
								.replace(/"Starting_Depth"/g, "\"Starting_Depth\"")
								.replace(/"Ending_Depth"/g, "\"Ending_Depth\"")
								.replace(/"Layer"/g, "\"Layer\"")
								.replace(/"Remaining_Thickness"/g, "\"Remaining_Thickness\"")
								.replace(/"Rotation_Angle"/g, "\"Rotation_Angle\"")
								.replace(/"Comments"/g, "\"Comments\"")
								.replace(/"Repair_Type_ID"/g, "\"Repair_Type_ID\"")
								.replace(/"Repair_Reference"/g, "\"Repair_Reference\"")
								.replace(/"Part_Number"/g, "\"Part_Number\"")
								.replace(/"Part_Serial_Number"/g, "\"Part_Serial_Number\"")
								.replace(/"Environment_Category_ID"/g, "\"Environment_Category_ID\"")
								.replace(/"EASA_AMC_2020_Level_ID"/g, "\"EASA_AMC_2020_Level_ID\"")
								.replace(/"SSI_Reference"/g, "\"SSI_Reference\"")
								.replace(/"RMT_File"/g, "\"RMT_File\"")
								.replace(/"RDAS_Reference"/g, "\"RDAS_Reference\"")
								.replace(/"Corrosion_Material_1"/g, "\"Corrosion_Material_1\"")
								.replace(/"Corrosion_Material_2"/g, "\"Corrosion_Material_2\"")
								.replace(/"Corrosion_Fastener_Specification"/g, "\"Corrosion_Fastener_Specification\"")
								.replace(/"Part_Criticity_ID"/g, "\"Part_Criticity_ID\"")
								.replace(/"Inspection_Area_Cycle_ID"/g, "\"Inspection_Area_Cycle_ID\"")
								.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\"")); 
						for (var i=0;i<json_objs.length;i++){
							json_objs[i].message = 'Inspection Finding updated successfully.';
						}
						resolved(json_objs);
					}).catch((errors) => {
						rejected(errors);
					});
				});
				promises_inner.push(prom_update);
			  }//end if (combine_part_update.length > 0)
				
			  if (combine_insert.length > 0){
				prom_insert = new Promise(function(resolved, rejected) {
				db.E2E_Inspection_Finding
				.bulkCreate(combine_insert, { 
					individualHooks: true, 
					transaction: transact,
					raw: true,
					useMaster: true })
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Finding_ID\"")
							.replace(/"IP_Name"/g, "\"IP_Name\"")
							.replace(/"Finding_Date"/g, "\"Finding_Date\"")
							.replace(/"Flying_Cycle_Real"/g, "\"Flying_Cycle_Real\"")
							.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\"")
							.replace(/"Number_Spot"/g, "\"Number_Spot\"")
							.replace(/"Length"/g, "\"Length\"")
							.replace(/"Width"/g, "\"Width\"")
							.replace(/"Surface"/g, "\"Surface\"")
							.replace(/"Orientation"/g, "\"Orientation\"")
							.replace(/"Angle_Reference"/g, "\"Angle_Reference\"")
							.replace(/"Depth"/g, "\"Depth\"")
							.replace(/"Starting_Depth"/g, "\"Starting_Depth\"")
							.replace(/"Ending_Depth"/g, "\"Ending_Depth\"")
							.replace(/"Layer"/g, "\"Layer\"")
							.replace(/"Remaining_Thickness"/g, "\"Remaining_Thickness\"")
							.replace(/"Rotation_Angle"/g, "\"Rotation_Angle\"")
							.replace(/"Comments"/g, "\"Comments\"")
							.replace(/"Repair_Type_ID"/g, "\"Repair_Type_ID\"")
							.replace(/"Repair_Reference"/g, "\"Repair_Reference\"")
							.replace(/"Part_Number"/g, "\"Part_Number\"")
							.replace(/"Part_Serial_Number"/g, "\"Part_Serial_Number\"")
							.replace(/"Environment_Category_ID"/g, "\"Environment_Category_ID\"")
							.replace(/"EASA_AMC_2020_Level_ID"/g, "\"EASA_AMC_2020_Level_ID\"")
							.replace(/"SSI_Reference"/g, "\"SSI_Reference\"")
							.replace(/"RMT_File"/g, "\"RMT_File\"")
							.replace(/"RDAS_Reference"/g, "\"RDAS_Reference\"")
							.replace(/"Corrosion_Material_1"/g, "\"Corrosion_Material_1\"")
							.replace(/"Corrosion_Material_2"/g, "\"Corrosion_Material_2\"")
							.replace(/"Corrosion_Fastener_Specification"/g, "\"Corrosion_Fastener_Specification\"")
							.replace(/"Part_Criticity_ID"/g, "\"Part_Criticity_ID\"")
							.replace(/"Inspection_Area_Cycle_ID"/g, "\"Inspection_Area_Cycle_ID\"")
							.replace(/"Finding_Type_ID"/g, "\"Finding_Type_ID\""));
							for (var i=0;i<json_objs.length;i++){
								json_objs[i].message = 'Inspection Finding inserted successfully.';
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						  }); 
				  });
				promises_inner.push(prom_insert);
			  }//end if (combine_part_insert.length > 0)
			  
			if (promises_inner.length > 0){
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
				.then(function (ress) {
				ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
					
				  
				  if (combine_update.length > 0){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): updated result returned');
	
					var resp = ress[0]; 
					for (var j=0;j<resp.length;j++){  
					  var index_obj =  map_record_update[j]; 	
						var obj_new = JSON.parse(JSON.stringify(resp[j]));
				 
					  //update the obj_new to the index of the old element
						objects[index_obj.parentindex] = obj_new;
					}//end for (var j=0;j<resp.length;j++)
					  ress.splice(0, 1);
				  }
				  if (combine_insert.length > 0){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): inserted result returned');
	
					  var resp = ress[0];   
					  for (var j=0;j<resp.length;j++){  
						var index_obj =  map_record_insert[j]; 		
						  var obj_new = JSON.parse(JSON.stringify(resp[j]));
						  
						//update the obj_new to the index of the old element
						  objects[index_obj.parentindex] = obj_new;
					  }//end for (var j=0;j<resp.length;j++)
					  ress.splice(0, 1);
				  } //console.log("here") 
				  //console.log(JSON.stringify(objects))
				  
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
				  resolve(objects);
	//====END======================Handle the Inspection Finding====================================================== 
				  }).catch((errors) => {
					  console.log(errors)
					var error_code = errors.errors[0];
					error_code = error_code.type;
	
					var msg
					if (error_code == 'unique violation'){
					  msg = {'message':'Duplicate Inspection Finding value found. Pls ensure that Inspection Finding value is unqiue.'};
					}else{
						msg = errors;
					}
					console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning error');
					reject(msg);
				  }); //end return Promise.all(promises_inner)
			}else{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)
				
		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionFinding
    
    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionfinding 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    add(inspectionfinding, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionfinding/lib/index.js: add(inspectionfinding, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(inspectionfinding) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: add(inspectionfinding, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateInspectionFinding(inspectionfinding,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: add(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: add(inspectionfinding, transact, transactionid): returning error from addUpdateInspectionFinding: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: add(inspectionfinding, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionfinding 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    update(inspectionfinding, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionfinding/lib/index.js: update(inspectionfinding, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(inspectionfinding) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: update(inspectionfinding, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateInspectionFinding(inspectionfinding,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: update(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: update(inspectionfinding, transact, transactionid): returning error from addUpdateInspectionFinding: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: update(inspectionfinding, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end update


    /**
	   * Get a specific Inspection Finding
	   *
	   * @param {Integer} Inspection_Finding_ID - E2E_Inspection_Finding ID
	   * @param {String} Inspection_Finding - E2E_Inspection_Finding Value
   	   * @param {Integer} inspectionfindingclone - Clone template to become another inspection finding (0) or clone to become finding (1)
   	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
    get(param,search,transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid)');
			console.log('('+ transactionid +'): parameter app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			if (param.index == null){
                param.index = 0;
            }else{
                if (param.index > 0){
                    param.index = param.index -1;
                }
            }

            if (param.row == null){
                param.row = 30;
            }
            
            if (param.order == null){
                param.order = 'DESC';
            }

            if (param.ordercolumn == null){
                param.ordercolumn = 'Inspection_Finding_ID';
            }

            //create the overall sql statements
			var sql_query =
			//' SELECT '+
			' ID as Inspection_Finding_ID, '+
			' Value as Inspection_Finding '+
			' FROM '+
			' E2E_Inspection_Finding '+			
			' WHERE deleted_at is null @inspectionfindingid @inspectionfindingvalue ' +
			'   ORDER BY ' + param.ordercolumn + ' ' + param.order  +
			'   LIMIT ' + param.index + ', ' + param.row +
			'	; ';

            var replace_query_inspectionfindingid = ' AND ID in (@replace) ';
            var replace_query_inspectionfindingvalue = ' AND lcase(Value) in (@replace) ';

            if (param.Inspection_Finding_ID != null){   
				sql_query = sql_query.replace('@inspectionfindingid', replace_query_inspectionfindingid.replace('@replace',param.Inspection_Finding_ID));
            }else{
				sql_query = sql_query.replace('@inspectionfindingid', '');
            }

            if (param.Inspection_Finding != null){
                var value = param.Inspection_Finding;
                if (value.split(',').length > 1){
                    value = '\'' + value.replace(/,/g, "\',\'") + '\'';
                }else{
                    value = '\'' + value + '\'';
                }    
                sql_query = sql_query.replace('@inspectionfindingvalue', replace_query_inspectionfindingvalue.replace('@replace',value.toLowerCase()));
            }else{
                sql_query = sql_query.replace('@inspectionfindingvalue', '');
			}
			
			if (search == 0){ //proceed to return the sql string
				sql_query = ' Select ' + sql_query ;
			resolve(sql_query);			 
			}else if (search == 1 || search == 2){ //proceed to search query with database
				if (search == 1){//used for actual search requested by user
					sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + 'SELECT FOUND_ROWS();';
				}else if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					sql_query = ' Select ' + sql_query;
				}
				
			console.log('('+ transactionid +'): query app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.sequelize.query(
                sql_query,
                {
					type: db.sequelize.QueryTypes.SELECT,
					useMaster: false
                })
                .then((res) => {
                    var jsonobj = null;
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					jsonobj = res;
    				//resolve(res);
    			}
    			else if (search == 1){//used for actual search requested by user
					//total number of rows from search result (without limit)
    				var total_rows = res[1][0]["FOUND_ROWS()"];
    				var loop_end_index;
    				
    				if (param.row < total_rows){
    					loop_end_index = param.row;
    				}else if (total_rows < param.row){
    					loop_end_index = total_rows;
    				}else{
    					loop_end_index = total_rows;
    				}
    				
    				var return_result = {};
					var final_result = [];
					
    				for (var i=0;i<loop_end_index;i++){
    					final_result.push(res[0][i]);
    				}
    				return_result.Result = final_result;
    				return_result.Actual_Total_Records_Found = total_rows;

    				jsonobj = return_result.Result;
    				//resolve(return_result);
				}
				
				//put each finding into a finding array
	        	var counter = -1;
	        	jsonobj.map(a =>{
	        		counter++;
	        		var inspectionfindingplaceholder = {};
	        		
	        		if (param.inspectionfindingclone != null){
	            		if (param.inspectionfindingclone == 1){
	            			inspectionfindingplaceholder.finding = [];
	            		}else{
	            			inspectionfindingplaceholder = [];
	            		}
	            	}else{
	            		inspectionfindingplaceholder = [];
	            	}
	        		
	        		    	    	    	        		
	        		 if (a.childinspectionfinding != null){
	        		 	var inner_counter = -1;
	        		 	a.childinspectionfinding.map(b =>{
	        		 		inner_counter++;
	        		 		var inspectionfindingplaceholder_inner = {};
	        		 		inspectionfindingplaceholder_inner.finding = [];
	        		 		inspectionfindingplaceholder_inner.finding.push(b);
	        		 		a.childinspectionfinding[inner_counter] = inspectionfindingplaceholder_inner;	  
	        		 	});
	        		 }
	        		if (param.inspectionfindingclone != null){
	            		if ( (param.inspectionfindingclone == 0 || param.inspectionfindingclone == 1) && search != 2){
	            			delete a.inspectionfindingid;
	            		}
	        		}
	        		
					//inspectionfindingplaceholder.inspectionfinding.push(a);
					//displays the data (ID and Value)
	        		if (param.inspectionfindingclone != null){
	            		 if (param.inspectionfindingclone == 1){
	            			inspectionfindingplaceholder.finding.push(a);
	            		}else{
	            			inspectionfindingplaceholder.push(a);
	            		}
	            	}else{
	            		inspectionfindingplaceholder.push(a);
	            	}
	        		
	        		jsonobj[counter] = inspectionfindingplaceholder;	  
	    		}).join(",");
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid): returning result');
					resolve(jsonobj);
    			}else if (search == 1){
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid): returning result');
					if (jsonobj != 0){
					return_result.Result = jsonobj;
					resolve(return_result);
					}
					else{
					console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid): returning result');
					return_result.Result = 'Not found';
					resolve(return_result);
					}
					}
    		})
                .catch((error) => {
					console.log(error);
					console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
                    reject(error);
                });
			}//end if (search == 0)
        });
	}//end get

	/**
	 * List all Inspection Finding from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {
        
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionfinding/lib/index.js: list(transact, transactionid)');
			
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.E2E_Inspection_Finding
                .findAll({
						attributes: [['ID', 'Inspection_Finding_ID'], ['Value', 'Inspection_Finding']],
						useMaster: false
                    }
                )
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: list(transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: list(transact, transactionid): returning error: ' + error);
                    reject(error);
                });
            });
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    remove(id,transact, transactionid) {
        return new Promise((resolve, reject) => {

            console.log('('+ transactionid +'): entered app/controllers/api/e2e/inspectionfinding/lib/index.js: remove(id, transact, transactionid)');
            console.log('('+ transactionid +'): parameter app/controllers/api/e2e/inspectionfinding/lib/index.js: remove(id,transact, transactionid): ' + id);
            //convert id to array
            var array_id = id.split(',');
            db.E2E_Inspection_Finding
                .destroy({
					transaction: transact,
					useMaster: true,
                    where : {
                        ID : array_id
                    }
                })
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: remove(id, transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionfinding/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
                    reject(error);
                });
        });
	}
	
	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} inspectionfinding 
     * @param {String} transactionid 
     */
    checkEmptyFields(inspectionfinding){
        return new Promise((resolve, reject) => {
            var anyemptyfields = false;

            // inspectionfinding.map(a =>{
            //     if (a.Inspection_Finding == null){
            //         reject({'message':'Inspection_Finding is mandatory field'});
            //     }
            // });

            resolve(0);
        });
    }
}

module.exports = InspectionFinding;
