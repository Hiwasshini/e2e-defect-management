'use strict';

const InspectionFinding = require('./lib');

const inspectionfinding = new InspectionFinding();
const classfilepath = 'app/controllers/api/e2e/inspectionfinding/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionFinding:
 *     type: object
 *     required:
 *       - inspectionfindingid
 *       - inspectionfindingvalue
 *     properties:
 *       inspectionfindingid:
 *         type: integer
 *       inspectionfindingvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionfinding:
   *   post:
   *     summary: Add a new Inspection Finding
   *     description: Add a new Inspection Finding as a JSON object
   *     tags:
   *       - InspectionFinding
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFinding"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionfinding', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionfinding');
 
      return(
        inspectionfinding
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfinding: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfinding: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfinding: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfinding: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionfinding/all:
   *   get:
   *     summary: List all Inspection Finding (not recommended)
   *     description: List all Inspection Finding as an JSON array
   *     tags:
   *       - InspectionFinding
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionFinding"
   */
  app.get('/api/e2e/inspectionfinding/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfinding/all');

      inspectionfinding
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfinding/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfinding/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionfinding:
   *   get:
   *     summary: Get a Inspection Finding based on parameters
   *     description: Get a Inspection Finding based on parameters.
   *     tags:
   *       - InspectionFinding
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionfindingid
   *         in: path
   *         description: "Inspection Finding ID"
   *         required: true
   *         type: integer
   *       - name: inspectionfindingvalue
   *         in: path
   *         description: "Inspection Finding Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionfinding', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfinding');

     inspectionfinding
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfinding: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfinding: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfinding: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionfinding/{id}:
   *   delete:
   *     summary: Removes a Inspection Finding
   *     description: Removes a Inspection Finding
   *     tags:
   *       - InspectionFinding
   *     parameters:
   *       - name: inspectionfindingid
   *         in: path
   *         description: "Inspection Finding id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionfinding/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionfinding');

		  return inspectionfinding
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfinding: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionfinding:
   *   patch:
   *     summary: Update a Inspection Finding
   *     description: Update a Inspection Finding
   *     tags:
   *       - InspectionFinding
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFinding"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionfinding', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionfinding');

		  return inspectionfinding
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfinding: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
