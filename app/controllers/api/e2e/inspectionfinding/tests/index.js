
'use strict';

const Doctor = require('../lib');

const doctor = new Doctor();


const expect = require('chai').expect;

module.exports = () => {
  const dat = {
		  acregid: 33,
		  inspectionfindingid: 1,
		  defecttypeid: 1,
		  parentdefectid: 1,
		  regautoseattrackid:1,
		  title: "TESTING PURPOSE ONLY",
		  description: "TESTING PURPOSE ONLY",
		  defectdate: "2018-02-02 00:50:00",
		  reportedbyid: 2,
		  result: "Testing purposes, Testing purposes, Testing purposes",
		  child: [
		  		{inspectionfindingid: 1,defecttypeid: 1,title: "TESTING PURPOSE ONLY-CHILD 1",description: "TESTING PURPOSE ONLY-CHILD 1",defectdate: "2018-02-02 01:50:00",reportedbyid: 3,result: "Testing purposes child 1, Testing purposes child 1, Testing purposes child 1"},
		  		{inspectionfindingid: 1,defecttypeid: 1,title: "TESTING PURPOSE ONLY-CHILD 2",description: "TESTING PURPOSE ONLY-CHILD 2",defectdate: "2018-02-02 02:50:00",reportedbyid: 3,result: "Testing purposes child 2, Testing purposes child 2, Testing purposes child 2"}
		  		]		  
  };

  const modifiedDat = {
		  acregid: 33,
		  inspectionfindingid: 1,
		  defecttypeid: 1,
		  parentdefectid: 1,
		  regautoseattrackid:1,
		  title: "TESTING PURPOSE ONLY-Updated",
		  description: "TESTING PURPOSE ONLY-Updated",
		  defectdate: "2018-02-02 00:50:00",
		  reportedbyid: 2,
		  result: "Testing purposes-Updated, Testing purposes-Updated, Testing purposes-Updated"
  };

  let Id;
  
  var search = {
		  id: '109,110,111',
		  acregid: '32,33',
		  inspectionfindingid: '1,2',
		  defecttypeid: '1,2',
		  regautoseattrackid: '1',
		  reportedbyid: '1',
		  parentdefectid: '1',
		  defectdatestart: '2018-01-30 00:00:00',
		  defectdateend: '2018-02-04 12:00:00'
		};

  describe('MRO_Defect', () => {
    it('add an defect', () => mro_defect
      .add(dat)
      .then((data) => {
    	  Id = data.ID;

        expect(data)
        .to.be.an('object');
      })
    );

    it('list all defect', () => mro_defect
      .list()
      .then((data) => {
        expect(data)
          .to.be.an('array')
          .to.have.length.of.at.least(1);
      })
    );
    
//    it('get defect', () => mro_defect
//      .get(Id)
//      .then((data) => {
//        expect(data.dataValues)
//          .to.be.an('object');
//      })
//    );
    
    it('get defect', () => mro_defect
      .get(search)
      .then((data) => {
        expect(data)
          .to.be.an('array');
      })
    );

    it('update defect', () => mro_defect
      .update(Id, modifiedDat)
      .then((data) => {
        expect(data[0])
          .to.equal(1);
      })
    );

    it('remove defect', () => mro_defect
      .remove(Id)
      .then((data) => {
        expect(data)
          .to.equal(1);
      })
    );
  });
};