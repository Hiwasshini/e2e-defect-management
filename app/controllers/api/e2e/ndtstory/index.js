'use strict';

const NDTStory = require('./lib');

const ndtstory = new NDTStory();
const classfilepath = 'app/controllers/api/e2e/ndtstory/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   NDTStory:
 *     type: object
 *     required:
 *       - ndtstoryid
 *       - ndtstoryvalue
 *     properties:
 *       ndtstoryid:
 *         type: integer
 *       ndtstoryvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/ndtstory:
   *   post:
   *     summary: Add a new NDT Story
   *     description: Add a new NDT Story as a JSON object
   *     tags:
   *       - NDTStory
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "NDT Story object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/NDTStory"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/ndtstory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/ndtstory');
 
      return(
        ndtstory
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/ndtstory: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/ndtstory: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/ndtstory: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/ndtstory: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/ndtstory/all:
   *   get:
   *     summary: List all NDT Story (not recommended)
   *     description: List all NDT Story as an JSON array
   *     tags:
   *       - NDTStory
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/NDTStory"
   */
  app.get('/api/e2e/ndtstory/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/ndtstory/all');

      ndtstory
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/ndtstory/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/ndtstory/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/ndtstory:
   *   get:
   *     summary: Get a NDT Story based on parameters
   *     description: Get a NDT Story based on parameters.
   *     tags:
   *       - NDTStory
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: ndtstoryid
   *         in: path
   *         description: "NDT Story ID"
   *         required: true
   *         type: integer
   *       - name: ndtstoryvalue
   *         in: path
   *         description: "NDT Story Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/ndtstory', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/ndtstory');

     ndtstory
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/ndtstory: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/ndtstory: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/ndtstory: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/ndtstory/{id}:
   *   delete:
   *     summary: Removes a NDT Story
   *     description: Removes a NDT Story
   *     tags:
   *       - NDTStory
   *     parameters:
   *       - name: ndtstoryid
   *         in: path
   *         description: "NDT Story id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/ndtstory/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/ndtstory');

		  return ndtstory
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/ndtstory: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/ndtstory: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/ndtstory: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/ndtstory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/ndtstory: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/ndtstory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/ndtstory:
   *   patch:
   *     summary: Update a NDT Story
   *     description: Update a NDT Story
   *     tags:
   *       - NDTStory
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "NDT Story object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/NDTStory"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/ndtstory', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/ndtstory');

		  return ndtstory
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/ndtstory: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/ndtstory: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/ndtstory: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/ndtstory: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/ndtstory: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/ndtstory: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
