'use strict';

const appRoot = require('app-root-path');

const db      = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/ndtstory/lib/index.js';
const  mergeJSON  = require ("merge-json") ;
var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

/**
 * Class that represents NDT Story through database
 */
class NDTStory {

    /**
     * Assist the add() to insert/update records into E2E_NDT_Story table
     * @param {JSON} ndtstory 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    addUpdateNDTStory(ndtstory,transact, transactionid){
		return new Promise((resolve, reject) => {
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid)');

			var objects = ndtstory;
			//====START======================Handle the NDT Story======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
	  		var combine_insert = [];
	  		var prom_update;
	  		var prom_insert;
	  		var map_record_update = [];
	  		var map_record_insert = [];
	  		
	  		var str = JSON.stringify(objects);
	  		                          		
		  //find all section that requires update only
			  var parentcounter =-1;
			  
	    		var json_obj = objects;
				var tempval;
				
				//for update & insert
				json_obj.map(a =>{
	    			parentcounter++;
	    			if (a.NDT_Story_ID != null){	
	    				var index_obj = {};
	    				index_obj.parentindex = parentcounter;
						map_record_update.push(index_obj);
						
						var ndtstory_obj = {};
						if (a.NDT_Story_ID != null){
							ndtstory_obj.ID = a.NDT_Story_ID;
						}
						if (a.NDT_Story != null){
							ndtstory_obj.Value = a.NDT_Story;
						}
						if (a.delete != null){
							ndtstory_obj.deleted_at = commonModule.currentDateTime();
						}
						combine_update.push(ndtstory_obj);
					}else if (a.NDT_Story_ID == null){	          							
	    				var index_obj = {};
	    				index_obj.parentindex = parentcounter;
						map_record_insert.push(index_obj);
						
						var ndtstory_obj = {};
						if (a.NDT_Story_ID != null){
							ndtstory_obj.ID = a.NDT_Story_ID;
						}
						if (a.NDT_Story != null){
							ndtstory_obj.Value = a.NDT_Story;
						}
						combine_insert.push(ndtstory_obj);
					}
				});

			  if (combine_update.length > 0){
				prom_update = new Promise(function(resolved, rejected) {
					db.E2E_NDT_Story
	                .bulkCreate(combine_update, { 
	                		updateOnDuplicate: ["Name","Value","deleted_at"], 
			  	        	transaction: transact,
							raw: true,
							useMaster: true
			  	        })
	                .then((result) => {
	                	var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"NDT_Story_ID\"")
                    			.replace(/"Value"/g, "\"NDT_Story\"")); 
	                	for (var i=0;i<json_objs.length;i++){
		            		json_objs[i].message = 'NDT Story updated successfully.';
		            	}
		            	resolved(json_objs);
	                }).catch((errors) => {
	                	rejected(errors);
    			    });
				});
				promises_inner.push(prom_update);
			  }//end if (combine_part_update.length > 0)
				
			  if (combine_insert.length > 0){
				prom_insert = new Promise(function(resolved, rejected) {
				db.E2E_NDT_Story
	            .bulkCreate(combine_insert, { 
	            	individualHooks: true, 
	            	transaction: transact,
					raw: true,
					useMaster: true })
		                .then((result) => {
		                	var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"NDT_Story_ID\"")
							.replace(/"Value"/g, "\"NDT_Story\""));    
		                	for (var i=0;i<json_objs.length;i++){
			            		json_objs[i].message = 'NDT Story inserted successfully.';
			            	}
			            	resolved(json_objs);
		                }).catch((errors) => {
		                	rejected(errors);
		  			    }); 
				  });
				promises_inner.push(prom_insert);
			  }//end if (combine_part_insert.length > 0)
			  
			if (promises_inner.length > 0){
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
				.then(function (ress) {
	  			
	  			if (combine_update.length > 0){
                    console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid): updated result returned');

	  				var resp = ress[0];   
	  				for (var j=0;j<resp.length;j++){  
                        var index_obj =  map_record_update[j]; 	
	  					var obj_new = JSON.parse(JSON.stringify(resp[j]));
                   
                        //update the obj_new to the index of the old element
	  					objects[index_obj.parentindex] = obj_new;
	  				}//end for (var j=0;j<resp.length;j++)
	  				ress.splice(0, 1);
	  			}
	  			if (combine_insert.length > 0){
                    console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid): inserted result returned');

	  				var resp = ress[0];   
	  				for (var j=0;j<resp.length;j++){  
                        var index_obj =  map_record_insert[j]; 		
	  					var obj_new = JSON.parse(JSON.stringify(resp[j]));
                          
                        //update the obj_new to the index of the old element
	  					objects[index_obj.parentindex] = obj_new;
	  				}//end for (var j=0;j<resp.length;j++)
	  				ress.splice(0, 1);
	  			}
                  
                console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid): returning result');
	  			resolve(objects);
	//====END======================Handle the NDT Story====================================================== 
	      		}).catch((errors) => {
					var error_code = errors.errors[0];
					error_code = error_code.type;

					var msg
					if (error_code == 'unique violation'){
					  msg = {'message':'Duplicate NDT Story value found. Pls ensure that NDT Story value is unqiue.'};
					}else{
						msg = errors;
					}
					console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid): returning error');
					reject(msg);
	  			}); //end return Promise.all(promises_inner)
			}else{
				console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: addUpdateNDTStory(ndtstory, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)
				
		});//end return new Promise((resolve, reject) =>
	};//end addUpdateNDTStory
    
    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} ndtstory 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    add(ndtstory, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: add(ndtstory, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(ndtstory) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: add(ndtstory, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateNDTStory(ndtstory,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: add(ndtstory, transact, transactionid): returning result from addUpdateNDTStory');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: add(ndtstory, transact, transactionid): returning error from addUpdateNDTStory: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: add(ndtstory, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} ndtstory 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    update(ndtstory, transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: update(ndtstory, transact, transactionid)');
            var self = this;
			
			self.checkEmptyFields(ndtstory) 
            .then(res=>{
				console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: update(ndtstory, transact, transactionid): returning result from checkEmptyFields');

				self.addUpdateNDTStory(ndtstory,transact, transactionid)
                .then((res) => {
					console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: update(ndtstory, transact, transactionid): returning result from addUpdateNDTStory');
					resolve(res);

                }).catch((error_0) => {
				    console.log(error_0);
				    console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: update(ndtstory, transact, transactionid): returning error from addUpdateNDTStory: ' + error_0);
                    reject(error_0);
            	});
			})
			.catch(error_1=>{
				console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: update(ndtstory, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
				reject(error_1);
			});	

        });
    }//end update


    /**
	   * Get a specific NDT Story
	   *
	   * @param {Integer} NDT_Story_ID - E2E_NDT_Story ID
	   * @param {String} NDT_Story - E2E_NDT_Story Value
   	   * @param {Integer} ndtstoryclone - Clone template to become another ndt story (0) or clone to become finding (1)
   	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
    get(param,search,transact, transactionid) {
        return new Promise((resolve, reject) => {
			console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid)');
			console.log('('+ transactionid +'): parameter app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			if (param.index == null){
                param.index = 0;
            }else{
                if (param.index > 0){
                    param.index = param.index -1;
                }
            }

            if (param.row == null){
                param.row = 30;
            }
            
            if (param.order == null){
                param.order = 'DESC';
            }

            if (param.ordercolumn == null){
                param.ordercolumn = 'NDT_Story_ID';
            }

            //create the overall sql statements
			var sql_query =
			//' SELECT '+
			' ID as NDT_Story_ID, '+
			' Value as NDT_Story '+
			' FROM '+
			' E2E_NDT_Story '+			
			' WHERE deleted_at is null @ndtstoryid @ndtstoryvalue ' +
			'   ORDER BY ' + param.ordercolumn + ' ' + param.order  +
			'   LIMIT ' + param.index + ', ' + param.row +
			'	; ';

            var replace_query_ndtstoryid = ' AND ID in (@replace) ';
			var replace_query_ndtstoryvalue = ' AND lcase(Value) in (@replace) ';

				if (param.NDT_Story_ID != null){   
				 sql_query = sql_query.replace('@ndtstoryid', replace_query_ndtstoryid.replace('@replace',param.NDT_Story_ID));
            }else{
				sql_query = sql_query.replace('@ndtstoryid', '');
			}

			 	if (param.NDT_Story != null){
                	var value = param.NDT_Story;
                if (value.split(',').length > 1){
                    value = '\'' + value.replace(/,/g, "\',\'") + '\'';
            }else{
                value = '\'' + value + '\'';
            }    
                sql_query = sql_query.replace('@ndtstoryvalue', replace_query_ndtstoryvalue.replace('@replace',value.toLowerCase()));
            }else{
                sql_query = sql_query.replace('@ndtstoryvalue', '');
			}
		
			if (search == 0){ //proceed to return the sql string
				sql_query = ' Select ' + sql_query ;
			resolve(sql_query);			 
			}else if (search == 1 || search == 2){ //proceed to search query with database
				if (search == 1){//used for actual search requested by user
					sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + 'SELECT FOUND_ROWS();';
				}else if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					sql_query = ' Select ' + sql_query;
				}
				
			console.log('('+ transactionid +'): query app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.sequelize.query(
                sql_query,
                {
					type: db.sequelize.QueryTypes.SELECT,
					useMaster: false
                })
                .then((res) => {
                    var jsonobj = null;
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					jsonobj = res;
    				//resolve(res);
    			}
    			else if (search == 1){//used for actual search requested by user
					//total number of rows from search result (without limit)
    				var total_rows = res[1][0]["FOUND_ROWS()"];
    				var loop_end_index;
    				
    				if (param.row < total_rows){
    					loop_end_index = param.row;
    				}else if (total_rows < param.row){
    					loop_end_index = total_rows;
    				}else{
    					loop_end_index = total_rows;
    				}
    				
    				var return_result = {};
					var final_result = [];
					
    				for (var i=0;i<loop_end_index;i++){
    					final_result.push(res[0][i]);
    				}
    				return_result.Result = final_result;
    				return_result.Actual_Total_Records_Found = total_rows;

    				jsonobj = return_result.Result;
    				//resolve(return_result);
				}
				
				//put each finding into a finding array
	        	var counter = -1;
	        	jsonobj.map(a =>{
	        		counter++;
	        		var ndtstoryplaceholder = {};
	        		
	        		if (param.ndtstoryclone != null){
	            		if (param.ndtstoryclone == 1){
	            			ndtstoryplaceholder.finding = [];
	            		}else{
	            			ndtstoryplaceholder = [];
	            		}
	            	}else{
	            		ndtstoryplaceholder = [];
	            	}
	        		
	        		    	    	    	        		
	        		 if (a.childndtstory != null){
	        		 	var inner_counter = -1;
	        		 	a.childndtstory.map(b =>{
	        		 		inner_counter++;
	        		 		var ndtstoryplaceholder_inner = {};
	        		 		ndtstoryplaceholder_inner.finding = [];
	        		 		ndtstoryplaceholder_inner.finding.push(b);
	        		 		a.childndtstory[inner_counter] = ndtstoryplaceholder_inner;	  
	        		 	});
	        		 }
	        		if (param.ndtstoryclone != null){
	            		if ( (param.ndtstoryclone == 0 || param.ndtstoryclone == 1) && search != 2){
	            			delete a.ndtstoryid;
	            		}
	        		}
	        		
					//ndtstoryplaceholder.ndtstory.push(a);
					//displays the data (ID and Value)
	        		if (param.ndtstoryclone != null){
	            		 if (param.ndtstoryclone == 1){
	            			ndtstoryplaceholder.finding.push(a);
	            		}else{
	            			ndtstoryplaceholder.push(a);
	            		}
	            	}else{
	            		ndtstoryplaceholder.push(a);
	            	}
	        		
	        		jsonobj[counter] = ndtstoryplaceholder;	  
	    		}).join(",");
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
					console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid): returning result');
					resolve(jsonobj);
    			}else if (search == 1){
					console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid): returning result');
					if (jsonobj != 0){
					return_result.Result = jsonobj;
					resolve(return_result);
					}
					else{
					console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid): returning result');
					return_result.Result = 'Not found';
					resolve(return_result);
					}
					}
    		})
                .catch((error) => {
					console.log(error);
					console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
                    reject(error);
                });
			}//end if (search == 0)
        });
	}//end get

	/**
	 * List all NDT Story from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {
        
            console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: list(transact, transactionid)');
			
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
            db.E2E_NDT_Story
                .findAll({
						attributes: [['ID', 'NDT_Story_ID'], ['Value', 'NDT_Story']],
						useMaster: false
                    }
                )
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: list(transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: list(transact, transactionid): returning error: ' + error);
                    reject(error);
                });
            });
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
    remove(id,transact, transactionid) {
        return new Promise((resolve, reject) => {

            console.log('('+ transactionid +'): entered app/controllers/api/e2e/ndtstory/lib/index.js: remove(id, transact, transactionid)');
            console.log('('+ transactionid +'): parameter app/controllers/api/e2e/ndtstory/lib/index.js: remove(id,transact, transactionid): ' + id);
            //convert id to array
            var array_id = id.split(',');
            db.E2E_NDT_Story
                .destroy({
					transaction: transact,
					useMaster: true,
                    where : {
                        ID : array_id
                    }
                })
                .then((res) => {
                    console.log('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: remove(id, transact, transactionid): returning result');
                    resolve(res);
                })
                .catch((error) => {
                    console.error('('+ transactionid +'): app/controllers/api/e2e/ndtstory/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
                    reject(error);
                });
        });
	}
	
	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} ndtstory 
     * @param {String} transactionid 
     */
    checkEmptyFields(ndtstory){
        return new Promise((resolve, reject) => {
            var anyemptyfields = false;

            ndtstory.map(a =>{
                if (a.NDT_Story == null){
                    reject({'message':'NDT_Story is mandatory field'});
                }
            });

            resolve(0);
        });
    }
}

module.exports = NDTStory;
