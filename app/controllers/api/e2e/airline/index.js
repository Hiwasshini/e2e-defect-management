'use strict';

const Airline = require('./lib');

const airline = new Airline();
const classfilepath = 'app/controllers/api/e2e/airline/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   Airline:
 *     type: object
 *     required:
 *       - airlineid
 *       - airlinevalue
 *     properties:
 *       airlineid:
 *         type: integer
 *       airlinevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/airline:
   *   post:
   *     summary: Add a new Airline
   *     description: Add a new Airline as a JSON object
   *     tags:
   *       - Airline
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Airline object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/Airline"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/airline', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/airline');
 
      return(
        airline
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/airline: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/airline: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/airline: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/airline: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/airline/all:
   *   get:
   *     summary: List all Airline (not recommended)
   *     description: List all Airline as an JSON array
   *     tags:
   *       - Airline
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/Airline"
   */
  app.get('/api/e2e/airline/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/airline/all');

      airline
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/airline/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/airline/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/airline/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/airline/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/airline:
   *   get:
   *     summary: Get a Airline based on parameters
   *     description: Get a Airline based on parameters.
   *     tags:
   *       - Airline
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: airlineid
   *         in: path
   *         description: "Airline ID"
   *         required: true
   *         type: integer
   *       - name: airlinevalue
   *         in: path
   *         description: "Airline Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/airline', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/airline');

     airline
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/airline: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/airline: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/airline: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/airline: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/airline: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/airline: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/airline/{id}:
   *   delete:
   *     summary: Removes a Airline
   *     description: Removes a Airline
   *     tags:
   *       - Airline
   *     parameters:
   *       - name: airlineid
   *         in: path
   *         description: "Airline id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/airline/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/airline');

		  return airline
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/airline: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/airline: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/airline: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/airline: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/airline: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/airline: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/airline:
   *   patch:
   *     summary: Update a Airline
   *     description: Update a Airline
   *     tags:
   *       - Airline
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Airline object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/Airline"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/airline', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/airline');

		  return airline
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/airline: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/airline: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/airline: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/airline: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/airline: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/airline: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
