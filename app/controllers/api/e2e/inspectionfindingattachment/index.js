'use strict';

const InspectionFindingAttachment = require('./lib');

const inspectionfindingattachment = new InspectionFindingAttachment();
const classfilepath = 'app/controllers/api/e2e/inspectionfindingattachment/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionFindingAttachment:
 *     type: object
 *     required:
 *       - inspectionfindingattachmentid
 *       - inspectionfindingattachmentvalue
 *     properties:
 *       inspectionfindingattachmentid:
 *         type: integer
 *       inspectionfindingattachmentvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionfindingattachment:
   *   post:
   *     summary: Add a new Inspection Finding Attachment
   *     description: Add a new Inspection Finding Attachment as a JSON object
   *     tags:
   *       - InspectionFindingAttachment
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding Attachment object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFindingAttachment"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionfindingattachment', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionfindingattachment');
 
      return(
        inspectionfindingattachment
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfindingattachment: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionfindingattachment: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfindingattachment: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionfindingattachment: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionfindingattachment/all:
   *   get:
   *     summary: List all Inspection Finding Attachment (not recommended)
   *     description: List all Inspection Finding Attachment as an JSON array
   *     tags:
   *       - InspectionFindingAttachment
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionFindingAttachment"
   */
  app.get('/api/e2e/inspectionfindingattachment/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfindingattachment/all');

      inspectionfindingattachment
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionfindingattachment:
   *   get:
   *     summary: Get a Inspection Finding Attachment based on parameters
   *     description: Get a Inspection Finding Attachment based on parameters.
   *     tags:
   *       - InspectionFindingAttachment
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionfindingattachmentid
   *         in: path
   *         description: "Inspection Finding Attachment ID"
   *         required: true
   *         type: integer
   *       - name: inspectionfindingattachmentvalue
   *         in: path
   *         description: "Inspection Finding Attachment FileKey"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionfindingattachment', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionfindingattachment');

     inspectionfindingattachment
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionfindingattachment: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionfindingattachment/{id}:
   *   delete:
   *     summary: Removes a Inspection Finding Attachment
   *     description: Removes a Inspection Finding Attachment
   *     tags:
   *       - InspectionFindingAttachment
   *     parameters:
   *       - name: inspectionfindingattachmentid
   *         in: path
   *         description: "Inspection Finding Attachment id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionfindingattachment/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionfindingattachment');

		  return inspectionfindingattachment
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionfindingattachment: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionfindingattachment:
   *   patch:
   *     summary: Update a Inspection Finding Attachment
   *     description: Update a Inspection Finding Attachment
   *     tags:
   *       - InspectionFindingAttachment
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Finding Attachment object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionFindingAttachment"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionfindingattachment', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionfindingattachment');

		  return inspectionfindingattachment
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionfindingattachment: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
