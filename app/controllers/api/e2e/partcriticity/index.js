'use strict';

const PartCriticity = require('./lib');

const partcriticity = new PartCriticity();
const classfilepath = 'app/controllers/api/e2e/partcriticity/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   PartCriticity:
 *     type: object
 *     required:
 *       - partcriticityid
 *       - partcriticityvalue
 *     properties:
 *       partcriticityid:
 *         type: integer
 *       partcriticityvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/partcriticity:
   *   post:
   *     summary: Add a new Part Criticity
   *     description: Add a new Part Criticity as a JSON object
   *     tags:
   *       - PartCriticity
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Part Criticity object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/PartCriticity"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/partcriticity', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/partcriticity');
 
      return(
        partcriticity
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/partcriticity: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/partcriticity: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/partcriticity: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/partcriticity: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/partcriticity/all:
   *   get:
   *     summary: List all Part Criticity (not recommended)
   *     description: List all Part Criticity as an JSON array
   *     tags:
   *       - PartCriticity
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/PartCriticity"
   */
  app.get('/api/e2e/partcriticity/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/partcriticity/all');

      partcriticity
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/partcriticity/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/partcriticity/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/partcriticity:
   *   get:
   *     summary: Get a Part Criticity based on parameters
   *     description: Get a Part Criticity based on parameters.
   *     tags:
   *       - PartCriticity
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: partcriticityid
   *         in: path
   *         description: "Part Criticity ID"
   *         required: true
   *         type: integer
   *       - name: partcriticityvalue
   *         in: path
   *         description: "Part Criticity Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/partcriticity', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/partcriticity');

     partcriticity
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/partcriticity: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/partcriticity: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/partcriticity: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/partcriticity/{id}:
   *   delete:
   *     summary: Removes a Part Criticity
   *     description: Removes a Part Criticity
   *     tags:
   *       - PartCriticity
   *     parameters:
   *       - name: partcriticityid
   *         in: path
   *         description: "Part Criticity id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/partcriticity/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/partcriticity');

		  return partcriticity
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/partcriticity: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/partcriticity: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/partcriticity: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/partcriticity: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/partcriticity: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/partcriticity: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/partcriticity:
   *   patch:
   *     summary: Update a Part Criticity
   *     description: Update a Part Criticity
   *     tags:
   *       - PartCriticity
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Part Criticity object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/PartCriticity"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/partcriticity', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/partcriticity');

		  return partcriticity
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/partcriticity: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/partcriticity: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/partcriticity: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/partcriticity: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/partcriticity: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/partcriticity: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
