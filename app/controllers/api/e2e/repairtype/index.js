'use strict';

const RepairType = require('./lib');

const repairtype = new RepairType();
const classfilepath = 'app/controllers/api/e2e/repairtype/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   RepairType:
 *     type: object
 *     required:
 *       - repairtypeid
 *       - repairtypevalue
 *     properties:
 *       repairtypeid:
 *         type: integer
 *       repairtypevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/repairtype:
   *   post:
   *     summary: Add a new Repair Type
   *     description: Add a new Repair Type as a JSON object
   *     tags:
   *       - RepairType
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Repair Type object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/RepairType"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/repairtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/repairtype');
 
      return(
        repairtype
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/repairtype: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/repairtype: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/repairtype: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/repairtype: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/repairtype/all:
   *   get:
   *     summary: List all Repair Type (not recommended)
   *     description: List all Repair Type as an JSON array
   *     tags:
   *       - RepairType
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/RepairType"
   */
  app.get('/api/e2e/repairtype/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/repairtype/all');

      repairtype
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/repairtype/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/repairtype/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/repairtype/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/repairtype/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/repairtype:
   *   get:
   *     summary: Get a Repair Type based on parameters
   *     description: Get a Repair Type based on parameters.
   *     tags:
   *       - RepairType
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: repairtypeid
   *         in: path
   *         description: "Repair Type ID"
   *         required: true
   *         type: integer
   *       - name: repairtypevalue
   *         in: path
   *         description: "Repair Type Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/repairtype', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/repairtype');

     repairtype
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/repairtype: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/repairtype: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/repairtype: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/repairtype: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/repairtype: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/repairtype: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/repairtype/{id}:
   *   delete:
   *     summary: Removes a Repair Type
   *     description: Removes a Repair Type
   *     tags:
   *       - RepairType
   *     parameters:
   *       - name: repairtypeid
   *         in: path
   *         description: "Repair Type id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/repairtype/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/repairtype');

		  return repairtype
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/repairtype: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/repairtype: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/repairtype: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/repairtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/repairtype: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/repairtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/repairtype:
   *   patch:
   *     summary: Update a Repair Type
   *     description: Update a Repair Type
   *     tags:
   *       - RepairType
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Repair Type object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/RepairType"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/repairtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/repairtype');

		  return repairtype
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/repairtype: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/repairtype: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/repairtype: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/repairtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/repairtype: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/repairtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
