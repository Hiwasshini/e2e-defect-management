'use strict';

const InspectionAreaFindingType = require('./lib');

const inspectionareafindingtype = new InspectionAreaFindingType();
const classfilepath = 'app/controllers/api/e2e/inspectionareafindingtype/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionAreaFindingType:
 *     type: object
 *     required:
 *       - inspectionareafindingtypeid
 *       - inspectionareafindingtypevalue
 *     properties:
 *       inspectionareafindingtypeid:
 *         type: integer
 *       inspectionareafindingtypevalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionareafindingtype:
   *   post:
   *     summary: Add a new Inspection Area Finding Type
   *     description: Add a new Inspection Area Finding Type as a JSON object
   *     tags:
   *       - InspectionAreaFindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area Finding Type object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionAreaFindingType"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionareafindingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionareafindingtype');
 
      return(
        inspectionareafindingtype
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionareafindingtype: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionareafindingtype: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionareafindingtype: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionareafindingtype: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionareafindingtype/all:
   *   get:
   *     summary: List all Inspection Area Finding Type (not recommended)
   *     description: List all Inspection Area Finding Type as an JSON array
   *     tags:
   *       - InspectionAreaFindingType
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionAreaFindingType"
   */
  app.get('/api/e2e/inspectionareafindingtype/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionareafindingtype/all');

      inspectionareafindingtype
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionareafindingtype:
   *   get:
   *     summary: Get a Inspection Area Finding Type based on parameters
   *     description: Get a Inspection Area Finding Type based on parameters.
   *     tags:
   *       - InspectionAreaFindingType
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionareafindingtypeid
   *         in: path
   *         description: "Inspection Area Finding Type ID"
   *         required: true
   *         type: integer
   *       - name: inspectionareafindingtypevalue
   *         in: path
   *         description: "Inspection Area Finding Type Value"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionareafindingtype', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionareafindingtype');

     inspectionareafindingtype
    .get(req.query,1,t,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionareafindingtype: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionareafindingtype/{id}:
   *   delete:
   *     summary: Removes a Inspection Area Finding Type
   *     description: Removes a Inspection Area Finding Type
   *     tags:
   *       - InspectionAreaFindingType
   *     parameters:
   *       - name: inspectionareafindingtypeid
   *         in: path
   *         description: "Inspection Area Finding Type id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionareafindingtype/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionareafindingtype');

		  return inspectionareafindingtype
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionareafindingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionareafindingtype:
   *   patch:
   *     summary: Update a Inspection Area Finding Type
   *     description: Update a Inspection Area Finding Type
   *     tags:
   *       - InspectionAreaFindingType
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Area Finding Type object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionAreaFindingType"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionareafindingtype', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionareafindingtype');

		  return inspectionareafindingtype
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionareafindingtype: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
