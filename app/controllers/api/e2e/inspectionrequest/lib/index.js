'use strict';

const appRoot = require('app-root-path');

const db = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
const classfilepath = 'app/controllers/api/e2e/inspectionrequest/lib/index.js';
const mergeJSON = require("merge-json");
const InspectionArea = require('../../inspectionarea/lib');
const InspectionAreaCycle = require('../../inspectionareacycle/lib');
const InspectionAreaFindingType = require('../../inspectionareafindingtype/lib');
const InspectionFinding = require('../../inspectionfinding/lib');
//const InspectionFindingFindingType = require('../../inspectionfindingfindingtype/lib');

var commonModule = require(`${appRoot}/common/common.js`);//common module that contains common functions

const NDT_Inspection_Area = new InspectionArea();
const NDT_Inspection_Area_Cycle = new InspectionAreaCycle();
const Expected_Finding_Type = new InspectionAreaFindingType();
const NDT_Inspection_Finding = new InspectionFinding();
//const NDT_Inspection_Finding__Finding_Type = new InspectionFindingFindingType();

/**
 * This is a super API that help to insert the following: 
 * 1. Inspection_Request
 * 2. 	|_Inspection_Area
 * 3. 		|_Inspection_Area_Cycle
 * 4. 			|_Inspection_Finding
 * 
 * There is a need to retrieve the ID for the following tables and append to the existing data before insertion into the database
 * 1. Inspection_Request
 * 		a. NDT_Story
 * 2. Inspection_Area
 *		a. Area_Inspectability
 *		b. Side
 *		c. Inspection_Type
 *		d. Assembly_Conditions
 *		e. Inspection_Method
 *		f. Finding_Type (multi-valued)
 * 3. Inspection_Area_Cycle
 *		a. Equipment_Type
 *		b. Airline
 * 4. Inspection_Finding
 *		a. Finding_Type
 *		b. Part_Criticity
 *		c. Repair_Type
 *		d. Finding_Category
 *		e. Environment_Category
 *		f. EASA_AMC_2020_Level
 */

/**
 * Class that represents Inspection Request through database
 */
class InspectionRequest {

    /**
     * Assist the add() to insert/update records into E2E_Inspection_Request table
     * @param {JSON} inspectionrequest 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	addUpdateInspectionRequest(inspectionrequest, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionRequest(inspectionrequest, transact, transactionid)');

			var objects = inspectionrequest;
			//====START======================Handle the Inspection Request======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;

			var json_obj = objects;
			var tempval;

			//for update & insert
			json_obj.map(a => {
				parentcounter++;
				if (a.Inspection_Request_ID != null) {
					var index_obj = {};
					index_obj.parentindex = parentcounter;
					map_record_update.push(index_obj);

					var inspectionrequest_obj = {};
					if (a.Inspection_Request_ID != null) {
						inspectionrequest_obj.ID = a.Inspection_Request_ID;
					}
					if (a.Program != null) {
						inspectionrequest_obj.Program = a.Program;
					}
					if (a.Aircraft_Type != null) {
						inspectionrequest_obj.Aircraft_Type = a.Aircraft_Type;
					}
					if (a.Applicable_MSN_Range != null) {
						inspectionrequest_obj.Applicable_MSN_Range = a.Applicable_MSN_Range;
					}
					if (a.Priority != null) {
						inspectionrequest_obj.Priority = a.Priority;
					}
					if (a.Stress_Leader != null) {
						inspectionrequest_obj.Stress_Leader = a.Stress_Leader;
					}
					if (a.NDI_compiler != null) {
						inspectionrequest_obj.NDI_compiler = a.NDI_compiler;
					}
					if (a.Test_Leader != null) {
						inspectionrequest_obj.Test_Leader = a.Test_Leader;
					}
					if (a.Requestor != null) {
						inspectionrequest_obj.Requestor = a.Requestor;
					}
					if (a.NDT_Story_ID != null) {
						inspectionrequest_obj.NDT_Story_ID = a.NDT_Story_ID;
					}
					if (a.delete != null) {
						inspectionrequest_obj.deleted_at = commonModule.currentDateTime();
					}
					combine_update.push(inspectionrequest_obj);
				} else if (a.Inspection_Request_ID == null) {
					var index_obj = {};
					index_obj.parentindex = parentcounter;
					map_record_insert.push(index_obj);

					var inspectionrequest_obj = {};
					if (a.Inspection_Request_ID != null) {
						inspectionrequest_obj.ID = a.Inspection_Request_ID;
					}
					if (a.Program != null) {
						inspectionrequest_obj.Program = a.Program;
					}
					if (a.Aircraft_Type != null) {
						inspectionrequest_obj.Aircraft_Type = a.Aircraft_Type;
					}
					if (a.Applicable_MSN_Range != null) {
						inspectionrequest_obj.Applicable_MSN_Range = a.Applicable_MSN_Range;
					}
					if (a.Priority != null) {
						inspectionrequest_obj.Priority = a.Priority;
					}
					if (a.Stress_Leader != null) {
						inspectionrequest_obj.Stress_Leader = a.Stress_Leader;
					}
					if (a.NDI_compiler != null) {
						inspectionrequest_obj.NDI_compiler = a.NDI_compiler;
					}
					if (a.Test_Leader != null) {
						inspectionrequest_obj.Test_Leader = a.Test_Leader;
					}
					if (a.Requestor != null) {
						inspectionrequest_obj.Requestor = a.Requestor;
					}
					if (a.NDT_Story_ID != null) {
						inspectionrequest_obj.NDT_Story_ID = a.NDT_Story_ID;
					}
					if (a.delete != null) {
						inspectionrequest_obj.deleted_at = commonModule.currentDateTime();
					}
					combine_insert.push(inspectionrequest_obj);
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					db.E2E_Inspection_Request
						.bulkCreate(combine_update, {
							updateOnDuplicate: ["Name", "Program", "Aircraft_Type", "Applicable_MSN_Range", "Priority",
								"Stress_Leader", "NDI_compiler", "Test_Leader", "Requestor", "NDT_Story_ID", "deleted_at"],
							transaction: transact,
							raw: true,
							useMaster: true
						})
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Request_ID\"")
								.replace(/"Program"/g, "\"Program\"")
								.replace(/"Aircraft Type"/g, "\"Aircraft Type\"")
								.replace(/"Applicable_MSN_Range"/g, "\"Applicable_MSN_Range\"")
								.replace(/"Priority"/g, "\"Priority\"")
								.replace(/"Stress_Leader"/g, "\"Stress_Leader\"")
								.replace(/"NDI_compiler"/g, "\"NDI_compiler\"")
								.replace(/"Test_Leader"/g, "\"Test_Leader\"")
								.replace(/"Requestor"/g, "\"Requestor\"")
								.replace(/"NDT_Story_ID"/g, "\"NDT_Story_ID\""));
							for (var i = 0; i < json_objs.length; i++) {
								json_objs[i].message = 'Inspection Request updated successfully.';
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					db.E2E_Inspection_Request
						.bulkCreate(combine_insert, {
							individualHooks: true,
							transaction: transact,
							raw: true,
							useMaster: true
						})
						.then((result) => {
							var json_objs = JSON.parse(JSON.stringify(result).replace(/"ID"/g, "\"Inspection_Request_ID\"")
								.replace(/"Program"/g, "\"Program\"")
								.replace(/"Aircraft Type"/g, "\"Aircraft Type\"")
								.replace(/"Applicable_MSN_Range"/g, "\"Applicable_MSN_Range\"")
								.replace(/"Priority"/g, "\"Priority\"")
								.replace(/"Stress_Leader"/g, "\"Stress_Leader\"")
								.replace(/"NDI_compiler"/g, "\"NDI_compiler\"")
								.replace(/"Test_Leader"/g, "\"Test_Leader\"")
								.replace(/"Requestor"/g, "\"Requestor\"")
								.replace(/"NDT_Story_ID"/g, "\"NDT_Story_ID\""));
							for (var i = 0; i < json_objs.length; i++) {
								json_objs[i].message = 'Inspection Request inserted successfully.';
							}
							resolved(json_objs);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						//console.log(ress)
						if (combine_update.length > 0) {
							var resp = ress[0];
							//childcounter = -1;
							parentcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								parentcounter++;
								//console.log(map_record_update[parentcounter])
								var index_obj = map_record_update[parentcounter];
								var ir_obj = objects[index_obj.parentindex].NDT_Inspection_Area; //inspection request object which contains an array of inspection area
								for (var k = 0; k < ir_obj.length; k++) {
									ir_obj[k].Inspection_Request_ID = resp[j].Inspection_Request_ID;//set the inspection request id to each and every inspection area
								}
								objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								objects[index_obj.parentindex].NDT_Inspection_Area = ir_obj; //put back the array of inspection area(including inspection request id) into inspection request object
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}
						if (combine_insert.length > 0) {
							//console.log(ress)
							var resp = ress[0];
							//childcounter = -1;
							parentcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								parentcounter++;
								//console.log(map_record_insert[parentcounter])
								var index_obj = map_record_insert[parentcounter];
								var ir_obj = objects[index_obj.parentindex].NDT_Inspection_Area; //inspection request object which contains an array of inspection area
								for (var k = 0; k < ir_obj.length; k++) {
									ir_obj[k].Inspection_Request_ID = resp[j].Inspection_Request_ID;//set the inspection request id to each and every inspection area
								}
								objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								objects[index_obj.parentindex].NDT_Inspection_Area = ir_obj; //put back the array of inspection area(including inspection request id) into inspection request object
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);

						}//console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionRequest(inspectionrequest, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Request====================================================== 
					}).catch((errors) => {
						console.log(errors)
						var error_code = errors.errors[0];
						error_code = error_code.type;

						 var msg
						if (error_code == 'unique violation') {
							msg = { 'message': 'Duplicate Inspection Request value found. Pls ensure that Inspection Request value is unqiue.' };
						} else {
							msg = errors;
						}
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionRequest(inspectionrequest, transact, transactionid): returning error');
						reject(msg);
					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionRequest(inspectionrequest, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionRequest

	addUpdateInspectionArea(inspectionarea, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid)');

			var objects = inspectionarea;
			//====START======================Handle the Inspection Area======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;

			var json_obj = objects;
			var tempval;

			//for update & insert
			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area != null) {
					a.NDT_Inspection_Area.map(b => {
						childcounter++;
						if (b.Inspection_Area_ID != null) {	//for update
							var index_obj = {};
							index_obj.parentindex = parentcounter;
							index_obj.childindex = childcounter;
							map_record_update.push(index_obj);
							combine_update.push(b);

						} else if (b.Inspection_Area_ID == null) {	//for insert
							var index_obj = {};
							index_obj.parentindex = parentcounter;
							index_obj.childindex = childcounter;
							map_record_insert.push(index_obj);
							combine_insert.push(b);
						}
					});
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area.update(combine_update, transact)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area.add(combine_insert, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database

						// for (var i=0;i<objects.length;i++){
						// 	if (objects[i].NDT_Inspection_Area != null){
						// 		objects[i].NDT_Inspection_Area = [];
						// 	}            	      				
						// }

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): updated result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								ia_obj.Inspection_Area_ID = resp[j].Inspection_Area_ID;
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								for (var k = 0; k < iac_array.length; k++) {
									iac_array[k].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area cycle
								}
								var Expected_Finding_Type_array = ia_obj.Expected_Finding_Type;
								for (var m = 0; m < Expected_Finding_Type_array.length; m++) {
									Expected_Finding_Type_array[m].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area finding type
								}
								ia_obj.Expected_Finding_Type = Expected_Finding_Type_array; //put back the array of inspection area finding type(including inspection area id) to inspection area object 
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): inserted result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								// console.log('index_obj')
								// console.log(index_obj)
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								// console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								ia_obj.Inspection_Area_ID = resp[j].Inspection_Area_ID;
								//console.log('ia_obj')
								//append all the ids from the resp[j] to ia_obj
								ia_obj.Side_ID = resp[j].Side_ID;
								ia_obj.Area_Inspectability_ID = resp[j].Area_Inspectability_ID;
								ia_obj.Assembly_Condition_ID = resp[j].Assembly_Condition_ID;
								ia_obj.Inspection_Type_ID = resp[j].Inspection_Type_ID;
								//console.log(JSON.stringify(ia_obj))
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								for (var k = 0; k < iac_array.length; k++) {
									iac_array[k].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area cycle
								}
								var Expected_Finding_Type_array = ia_obj.Expected_Finding_Type;
								for (var m = 0; m < Expected_Finding_Type_array.length; m++) {
									Expected_Finding_Type_array[m].Inspection_Area_ID = resp[j].Inspection_Area_ID;//set the inspection area id to each and every inspection area finding type
								}
								// console.log('resp[j]')
								// console.log(JSON.stringify(resp[j]))
								ia_obj.Expected_Finding_Type = Expected_Finding_Type_array; //put back the array of inspection area finding type(including inspection area id) to inspection area object 
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								//console.log(JSON.stringify(ia_obj))
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								//objects[index_obj.parentindex] = JSON.parse(JSON.stringify(resp[j]));
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						}  //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Area====================================================== 
					}).catch((errors) => {
						console.log(errors)
					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionArea(inspectionarea, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionArea


	addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid)');
			//   console.log('Objects')
			//   console.log(JSON.stringify(inspectionareacycle))
			var objects = inspectionareacycle;
			//====START======================Handle the Inspection Area Cycle======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;
			var grandchildcounter = -1;

			var json_obj = objects;
			var tempval;

			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area != null) {
					a.NDT_Inspection_Area.map(b => {
						childcounter++; //increment the child(parent)
						grandchildcounter = -1; //reset back the grandchild to its child(parent)
						if (b.NDT_Inspection_Area_Cycle != null) {
							b.NDT_Inspection_Area_Cycle.map(c => {
								grandchildcounter++;
								if (c.Inspection_Area_Cycle_ID != null) {	//for update
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									map_record_update.push(index_obj);
									combine_update.push(c);
								} else if (c.Inspection_Area_Cycle_ID == null) {	//for insert
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									//console.log("hello")
									map_record_insert.push(index_obj);
									combine_insert.push(c);
									// console.log(JSON.stringify(index_obj))
								}
							});
						}
					});
				}
			});

			// 	//for update & insert
			// 	json_obj.map(a =>{
			// 	  parentcounter++;
			// 	  childcounter++; //increment the child(parent)
			// 	  grandchildcounter= -1; //reset back the grandchild to its child(parent)
			// 	  if (a.NDT_Inspection_Area_Cycle != null){	
			// 		a.NDT_Inspection_Area_Cycle.map(b => {
			// 			grandchildcounter++;
			// 		  if (b.Inspection_Area_Cycle_ID != null){	//for update
			// 			var index_obj = {};
			// 			index_obj.parentindex = parentcounter;
			// 			index_obj.childindex = childcounter;
			// 			index_obj.grandchildindex = grandchildcounter;
			// 				map_record_update.push(index_obj);
			// 		  		combine_update.push(b);
			// 	  }else if (b.Inspection_Area_Cycle_ID == null){	//for insert
			// 		var index_obj = {};
			// 		index_obj.parentindex = parentcounter;
			// 		index_obj.childindex = childcounter;
			// 		index_obj.grandchildindex = grandchildcounter;
			// 		console.log("hello")
			// 			map_record_insert.push(index_obj);
			// 			 combine_insert.push(b);
			// 			 console.log(JSON.stringify(index_obj))
			// 	  }
			// 	});
			//   }
			// });
			//console.log("helloo here")

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area_Cycle.update(combine_update, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					NDT_Inspection_Area_Cycle.add(combine_insert, transact, transactionid)
						.then((res) => {
							//console.log(res)
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						//console.log(ress)

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): updated result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								for (var k = 0; k < if_array.length; k++) {
									if_array[k].Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;//set the inspection area cycle id to each and every inspection finding
								}
								iac_obj.Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.grandchildindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): inserted result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								//append all the ids from the resp[j] to iac_obj
								iac_obj.Equipment_Type_ID = resp[j].Equipment_Type_ID;
								iac_obj.Airline_ID = resp[j].Airline_ID;
								for (var k = 0; k < if_array.length; k++) {
									if_array[k].Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;//set the inspection area cycle id to each and every inspection finding
								}
								//   console.log('iac_obj')
								//   console.log(JSON.stringify(iac_obj))
								iac_obj.Inspection_Area_Cycle_ID = resp[j].Inspection_Area_Cycle_ID;
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.grandchildindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						} //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Area Cycle====================================================== 
					}).catch((errors) => {
						console.log(errors)
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning error');

					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaCycle(inspectionareacycle, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionAreaCycle

	addUpdateInspectionFinding(inspectionfinding, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid)');
			//console.log(JSON.stringify(inspectionfinding))
			var objects = inspectionfinding;
			//====START======================Handle the Inspection Finding======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;
			var grandchildcounter = -1;
			var greatgrandchildcounter = -1;
			var json_obj = objects;
			var tempval;

			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area != null) {
					a.NDT_Inspection_Area.map(b => {
						childcounter++; //increment the child(parent)
						grandchildcounter = -1; //reset back the grandchild to its child(parent)
						if (b.NDT_Inspection_Area_Cycle != null) {
							b.NDT_Inspection_Area_Cycle.map(c => {
								grandchildcounter++;//increment the grandchild(parent)
								greatgrandchildcounter = -1; //reset back the greatgrandchild to its grandchild(parent)
								if (c.NDT_Inspection_Finding != null) {
									c.NDT_Inspection_Finding.map(d => {
										greatgrandchildcounter++;
										if (d.Inspection_Finding_ID != null) {	//for update
											var index_obj = {};
											index_obj.parentindex = parentcounter;
											index_obj.childindex = childcounter;
											index_obj.grandchildindex = grandchildcounter;
											index_obj.greatgrandchildindex = greatgrandchildcounter;
											map_record_update.push(index_obj);
											combine_update.push(d);

										} else if (d.Inspection_Finding_ID == null) {	//for insert
											var index_obj = {};
											index_obj.parentindex = parentcounter;
											index_obj.childindex = childcounter;
											index_obj.grandchildindex = grandchildcounter;
											index_obj.greatgrandchildindex = greatgrandchildcounter;
											map_record_insert.push(index_obj);
											combine_insert.push(d);
										}
									});
								}
							});
						}
					});
				}
			});

			// 	//for update & insert
			// 	json_obj.map(a =>{
			// 	  parentcounter++;
			// 	  childcounter++;
			// 	  grandchildcounter++;//increment the grandchild(parent)
			// 	  greatgrandchildcounter= -1; //reset back the greatgrandchild to its grandchild(parent)
			// 	  if (a.NDT_Inspection_Finding != null){	
			// 		a.NDT_Inspection_Finding.map(b => {
			// 			greatgrandchildcounter++;
			// 		  if (b.Inspection_Finding_ID != null){	//for update
			// 			var index_obj = {};
			// 			index_obj.parentindex = parentcounter;
			// 			index_obj.childindex = childcounter;
			// 			index_obj.grandchildindex = grandchildcounter;
			// 			index_obj.greatgrandchildindex = greatgrandchildcounter;
			// 				map_record_update.push(index_obj);
			// 		  		combine_update.push(b);

			// 	  }else if (b.Inspection_Finding_ID == null){	//for insert
			// 		var index_obj = {};
			// 		index_obj.parentindex = parentcounter;
			// 		index_obj.childindex = childcounter;
			// 		index_obj.grandchildindex = grandchildcounter;
			// 		index_obj.greatgrandchildindex = greatgrandchildcounter;
			// 			map_record_update.push(index_obj);
			// 			  combine_update.push(b);
			// 	  }
			// 	});
			//   }
			// });

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					NDT_Inspection_Finding.update(combine_update, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					NDT_Inspection_Finding.add(combine_insert, transact, transactionid)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			console.log("Objects")
			console.log(JSON.stringify(objects))

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						// console.log("ress")
						// console.log(ress)

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): updated result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								//  var if_obj = if_array[index_obj.greatgrandchildindex]; //each inspection finding object (greatgrandchild) from the array of inspection finding type
								//  var ifft_array = if_obj.NDT_Finding__Finding_Type; //inspection findng object which contains an array of inspection finding finding type
								// append all the ids from the resp[j] to if_obj
								if_obj.Finding_Category_ID = resp[j].Finding_Category_ID;
								if_obj.Repair_Type_ID = resp[j].Repair_Type_ID;
								if_obj.Environment_Category_ID = resp[j].Environment_Category_ID;
								if_obj.EASA_AMC_2020_Level_ID = resp[j].EASA_AMC_2020_Level_ID;
								if_obj.Part_Criticity_ID = resp[j].Part_Criticity_ID;
								if_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								for (var k = 0; k < if_array.length; k++) {
									if_array[k].Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								}
								//console.log(JSON.stringify(ifft_array))
								//   if_obj.NDT_Finding__Finding_Type = ifft_array; //put back the array of inspection finding finding type(including inspection finding id) to inspection finding object
								//   if_array[index_obj.greatgrandchildindex] = if_obj; //put back the inspection finding object to the array of inspection finding
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.grandchildindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): inserted result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iac_array = ia_obj.NDT_Inspection_Area_Cycle; //inspection area object which contains an array of inspection area cycle
								var iac_obj = iac_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								var if_array = iac_obj.NDT_Inspection_Finding; //inspection area cycle object which contains an array of inspection finding
								var if_obj = if_array[index_obj.greatgrandchildindex]; //each inspection finding object (greatgrandchild) from the array of inspection finding type
								//  var ifft_array = if_obj.NDT_Finding__Finding_Type; //inspection findng object which contains an array of inspection finding finding type
								//append all the ids from the resp[j] to if_obj
								if_obj.Finding_Category_ID = resp[j].Finding_Category_ID;
								if_obj.Repair_Type_ID = resp[j].Repair_Type_ID;
								if_obj.Environment_Category_ID = resp[j].Environment_Category_ID;
								if_obj.EASA_AMC_2020_Level_ID = resp[j].EASA_AMC_2020_Level_ID;
								if_obj.Part_Criticity_ID = resp[j].Part_Criticity_ID;
								if_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								if_obj.Inspection_Finding_ID = resp[j].Inspection_Finding_ID;//set the inspection area cycle id to each and every inspection finding
								// console.log(JSON)
								// //   if_obj.NDT_Finding__Finding_Type = ifft_array; //put back the array of inspection finding finding type(including inspection finding id) to inspection finding object
								// console.log("objects")
								// console.log(JSON.stringify())
								if_array[index_obj.greatgrandchildindex] = if_obj; //put back the inspection finding object to the array of inspection finding
								iac_obj.NDT_Inspection_Finding = if_array; //put back the array of inspection finding(including inspection area  cycle id) to inspection area cycle object 
								iac_array[index_obj.grandchildindex] = iac_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.NDT_Inspection_Area_Cycle = iac_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						} //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Finding====================================================== 
					}).catch((errors) => {
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning error');
						reject(errors)
					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFinding(inspectionfinding, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionFinding

	addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid)');
			//   console.log('Objects')
			//   console.log(JSON.stringify(inspectionareafindingtype))
			var objects = inspectionareafindingtype;
			//====START======================Handle the Inspection Area Finding Type======================================================            	      			
			const promises_inner = [];
			var combine_update = [];
			var combine_insert = [];
			var prom_update;
			var prom_insert;
			var map_record_update = [];
			var map_record_insert = [];

			var str = JSON.stringify(objects);

			//find all section that requires update only
			var parentcounter = -1;
			var childcounter = -1;
			var grandchildcounter = -1;

			var json_obj = objects;
			var tempval;

			json_obj.map(a => {
				parentcounter++;
				childcounter = -1; //reset back the child to its own parent
				if (a.NDT_Inspection_Area != null) {
					a.NDT_Inspection_Area.map(b => {
						childcounter++; //increment the child(parent)
						grandchildcounter = -1; //reset back the grandchild to its child(parent)
						if (b.Expected_Finding_Type != null) {
							b.Expected_Finding_Type.map(c => {
								grandchildcounter++;
								if (c.Inspection_Area__Finding_Type_ID != null) {	//for update
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									map_record_update.push(index_obj);
									combine_update.push(c);
								} else if (c.Inspection_Area__Finding_Type_ID == null) {	//for insert
									var index_obj = {};
									index_obj.parentindex = parentcounter;
									index_obj.childindex = childcounter;
									index_obj.grandchildindex = grandchildcounter;
									//console.log("hello")
									map_record_insert.push(index_obj);
									combine_insert.push(c);
									// console.log(JSON.stringify(index_obj))
								}
							});
						}
					});
				}
			});

			if (combine_update.length > 0) {
				prom_update = new Promise(function (resolved, rejected) {
					Expected_Finding_Type.update(combine_update, transact)
						.then((res) => {
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_update);
			}//end if (combine_part_update.length > 0)

			if (combine_insert.length > 0) {
				prom_insert = new Promise(function (resolved, rejected) {
					Expected_Finding_Type.add(combine_insert, transact, transactionid)
						.then((res) => {
							//console.log(res)
							resolved(res);
						}).catch((errors) => {
							rejected(errors);
						});
				});
				promises_inner.push(prom_insert);
			}//end if (combine_part_insert.length > 0)

			if (promises_inner.length > 0) {
				//console.log('promises_inner.length: '+promises_inner.length);
				return Promise.all(promises_inner)
					.then(function (ress) {
						ress = JSON.parse(JSON.stringify(ress)); //the return result may provide a lot of redundant values, hence by converting it to string and back to JSON, we get only the actual result returned from the database
						//console.log(ress)

						if (combine_update.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid): updated result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_update[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iaft_array = ia_obj.Expected_Finding_Type; //inspection area object which contains an array of inspection area cycle
								var iaft_obj = iaft_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								//append all the ids from the resp[j] to iaft_obj
								iaft_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								iaft_obj.Inspection_Area_ID = resp[j].Inspection_Area_ID;
								iaft_obj.Inspection_Area__Finding_Type_ID = resp[j].Inspection_Area__Finding_Type_ID;
								//   console.log('iaft_obj')
								//   console.log(JSON.stringify(iaft_obj))
								iaft_array[index_obj.grandchildindex] = iaft_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.Expected_Finding_Type = iaft_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)
							ress.splice(0, 1);
						}

						if (combine_insert.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid): inserted result returned');

							var resp = ress[0];
							var indexcounter = -1;
							for (var j = 0; j < resp.length; j++) {
								indexcounter++;
								var index_obj = map_record_insert[indexcounter];
								var ir_obj = objects[index_obj.parentindex]; //inspection request object (parent)
								//console.log(JSON.stringify(ir_obj))
								var ia_array = ir_obj.NDT_Inspection_Area; //inspection request object which contains an array of inspection area 
								var ia_obj = ia_array[index_obj.childindex]; //each inspection area object (child) from the array of inspection area
								var iaft_array = ia_obj.Expected_Finding_Type; //inspection area object which contains an array of inspection area cycle
								var iaft_obj = iaft_array[index_obj.grandchildindex]; //each inspection area cycle object (grandchild) from the array of inspection areac cycle
								//append all the ids from the resp[j] to iaft_obj
								iaft_obj.Finding_Type_ID = resp[j].Finding_Type_ID;
								iaft_obj.Inspection_Area_ID = resp[j].Inspection_Area_ID;
								iaft_obj.Inspection_Area__Finding_Type_ID = resp[j].Inspection_Area__Finding_Type_ID;
								//   console.log('iaft_obj')
								//   console.log(JSON.stringify(iaft_obj))
								iaft_array[index_obj.grandchildindex] = iaft_obj;//put back the inspection area cycle object to the array of inspection area cycle
								ia_obj.Expected_Finding_Type = iaft_array; //put back the array of inspection area cycle(including inspection area id) to inspection area object 
								ia_array[index_obj.childindex] = ia_obj; //put back the inspection area object to the array of inspection area 
								ir_obj.NDT_Inspection_Area = ia_array; //put back the array of inspection area (including inspection request id) to inspection request object 
								objects[index_obj.parentindex] = ir_obj; //put back the inspection request object	  
							}//end for (var j=0;j<resp.length;j++)

							ress.splice(0, 1);
						} //console.log(JSON.stringify(objects))

						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid): returning result');
						resolve(objects);
						//====END======================Handle the Inspection Area Finding Type====================================================== 
					}).catch((errors) => {
						console.log(errors)
						console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid): returning error');

					}); //end return Promise.all(promises_inner)
			} else {
				console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionAreaFindingType(inspectionareafindingtype, transact, transactionid): returning result');
				resolve(objects);
			}//end if (promises_inner.length > 0)

		});//end return new Promise((resolve, reject) =>
	};//end addUpdateInspectionAreaFindingType

	// addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid) {
	// 	return new Promise((resolve, reject) => {
	// 		console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid)');

	// 		var objects = inspectionfindingfindingtype;
	// 		//====START======================Handle the Inspection Finding Finding Type======================================================            	      			
	// 		const promises_inner = [];
	// 		var combine_update = [];
	// 		var combine_insert = [];
	// 		var prom_update;
	// 		var prom_insert;
	// 		var map_record_update = [];
	// 		var map_record_insert = [];

	// 		var str = JSON.stringify(objects);

	// 		//find all section that requires update only
	// 		var parentcounter = -1;
	// 		var childcounter = -1;

	// 		var json_obj = objects;
	// 		var tempval;

	// 		//for update & insert
	// 		json_obj.map(a => {
	// 			parentcounter++;
	// 			if (a.Inspection_Finding__Finding_Type != null) {
	// 				a.Inspection_Finding__Finding_Type.map(b => {
	// 					if (b.Inspection_Finding_ID != null) {	//for update
	// 						childcounter++;
	// 						map_record_update.push(parentcounter);
	// 						combine_update.push(b);

	// 					} else if (b.Inspection_Finding_ID == null) {	//for insert
	// 						childcounter++;
	// 						map_record_insert.push(parentcounter);
	// 						combine_insert.push(b);
	// 					}
	// 				});
	// 			}
	// 		});

	// 		if (combine_update.length > 0) {
	// 			prom_update = new Promise(function (resolved, rejected) {
	// 				Inspection_Finding__Finding_Type.update(combine_update, transact)
	// 					.then((result) => {
	// 						resolved(json_objs);
	// 					}).catch((errors) => {
	// 						rejected(errors);
	// 					});
	// 			});
	// 			promises_inner.push(prom_update);
	// 		}//end if (combine_part_update.length > 0)

	// 		if (combine_insert.length > 0) {
	// 			prom_insert = new Promise(function (resolved, rejected) {
	// 				Inspection_Finding__Finding_Type.add(combine_insert, transact)
	// 					.then((result) => {
	// 						resolved(json_objs);
	// 					}).catch((errors) => {
	// 						rejected(errors);
	// 					});
	// 			});
	// 			promises_inner.push(prom_insert);
	// 		}//end if (combine_part_insert.length > 0)

	// 		if (promises_inner.length > 0) {
	// 			//console.log('promises_inner.length: '+promises_inner.length);
	// 			return Promise.all(promises_inner)
	// 				.then(function (ress) {
	// 					//reset the counter
	// 					childcounter = -1;
	// 					//reset the combine_part section to empty array
	// 					for (var i = 0; i < objects.length; i++) {
	// 						if (objects[i].Inspection_Finding__Finding_Type != null) {
	// 							objects[i].Inspection_Finding__Finding_Type = [];
	// 						}
	// 					}
	// 					if (combine_update.length > 0) {
	// 						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid): updated result returned');

	// 						var resp = ress[0];
	// 						childcounter = -1;

	// 						for (var j = 0; j < resp.length; j++) {
	// 							childcounter++;
	// 							parentcounter = map_record_update[childcounter];
	// 							var inspectionfindingfindingtypez = objects[parentcounter].Inspection_Finding__Finding_Type;
	// 							inspectionfindingfindingtypez.push(JSON.parse(JSON.stringify(resp[j])));
	// 							objects[parentcounter].Inspection_Finding__Finding_Type = inspectionfindingfindingtypez;

	// 						}//end for (var j=0;j<resp.length;j++)
	// 						ress.splice(0, 1);
	// 					}
	// 					if (combine_insert.length > 0) {
	// 						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid): inserted result returned');

	// 						var resp = ress[0];
	// 						childcounter = -1;

	// 						for (var j = 0; j < resp.length; j++) {
	// 							childcounter++;
	// 							parentcounter = map_record_update[childcounter];
	// 							var inspectionfindingfindingtypez = objects[parentcounter].Inspection_Finding__Finding_Type;
	// 							inspectionfindingfindingtypez.push(JSON.parse(JSON.stringify(resp[j])));
	// 							objects[parentcounter].Inspection_Finding__Finding_Type = inspectionfindingfindingtypez;

	// 						}//end for (var j=0;j<resp.length;j++)
	// 						ress.splice(0, 1);
	// 					}

	// 					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid): returning result');
	// 					resolve(objects);
	// 					//====END======================Handle the Inspection Finding Finding Type====================================================== 
	// 				}).catch((errors) => {
	// 					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid): returning error');
	// 					reject(msg);
	// 				}); //end return Promise.all(promises_inner)
	// 		} else {
	// 			console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: addUpdateInspectionFindingFindingType(inspectionfindingfindingtype, transact, transactionid): returning result');
	// 			resolve(objects);
	// 		}//end if (promises_inner.length > 0)

	// 	});//end return new Promise((resolve, reject) =>
	// };//end addUpdateInspectionFindingFindingType

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionrequest 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	add(inspectionrequest, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid)');
			var self = this;

			self.retrieveReferenceValues(inspectionrequest)
				.then(re => {

					// self.checkEmptyFields(inspectionrequest)//this function is not use at al 
					// .then(res=>{
					// 	console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning result from checkEmptyFields');

					self.addUpdateInspectionRequest(re, transact, transactionid)
						.then((res) => {

							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning result from addUpdateInspectionRequest');
							//console.log(JSON.stringify(res))
							self.addUpdateInspectionArea(res, transact, transactionid)
								//NDT_Inspection_Area.add(res, transact, transactionid)
								.then((ress) => {

									console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionarea, transact, transactionid): returning result from addUpdateInspectionArea');
									//console.log(JSON.stringify(ress))

									self.addUpdateInspectionAreaFindingType(ress, transact, transactionid)
										.then((resss) => {

											//console.log(JSON.stringify(resss));
											console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaFindingType');

											self.addUpdateInspectionAreaCycle(resss, transact, transactionid)
												//self.addUpdateInspectionAreaFindingType(ress, transact, transactionid)
												.then((resp) => {

													console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
													//console.log(JSON.stringify(resp))
													self.addUpdateInspectionFinding(resp, transact, transactionid)
														.then((ressp) => {
															console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
															resolve(ressp);

															// console.log('Final result from Inspection_Finding')
															// console.log(JSON.stringify(ressp))
															// 			self.addUpdateInspectionFindingFindingType(ressp, transact, transactionid)
															// 			.then((respp) => {
															// 				console.log('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionfindingfindingtype, transact, transactionid): returning result from addUpdateInspectionFindingFindingType');
															// resolve(respp);

															// 		}).catch((error_0) => {
															// 			console.log(error_0);
															// 			console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from addUpdateInspectionRequest: ' + error_0);
															// 			reject(error_0);
															// 		});
														})
														.catch(error_1 => {
															console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
															reject(error_1);
														});
												})
												.catch(error_2 => {
													console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_2);
													reject(error_2);
												});
										})
										.catch(error_2 => {
											console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_2);
											reject(error_2);
										});

								})
								.catch(error_3 => {
									console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_3);
									reject(error_3);
								});
						})
						.catch(error_4 => {
							console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_4);
							reject(error_4);
						});
					// })
					// .catch(error_5=>{
					// 	console.error('('+ transactionid +'): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_5);
					// 	reject(error_5);
					// });

				})
				.catch(error_5 => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: add(inspectionrequest, transact, transactionid): returning error from retrieveReferenceValues: ' + error_5);
					reject(error_5);
				});
		});
	}//end add

    /**
     * Handle the insert/update request the outer index.js
     * @param {JSON} inspectionrequest 
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	update(inspectionrequest, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid)');
			var self = this;

			self.checkEmptyFields(inspectionrequest)
				.then(res => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning result from checkEmptyFields');

					self.addUpdateInspectionRequest(inspectionrequest, transact, transactionid)
						.then((res) => {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning result from addUpdateInspectionRequest');
							//resolve(res);
							console.log(JSON.stringify(res))
							self.addUpdateInspectionArea(res, transact, transactionid)
								.then((ress) => {
									console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionarea, transact, transactionid): returning result from addUpdateInspectionArea');

									self.addUpdateInspectionAreaCycle(ress, transact, transactionid)
										//self.addUpdateInspectionAreaFindingType(ress, transact, transactionid)
										.then((resp) => {
											console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionareacycle, transact, transactionid): returning result from addUpdateInspectionAreaCycle');
											self.addUpdateInspectionFinding(resp, transact, transactionid)
												.then((ressp) => {
													console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionfinding, transact, transactionid): returning result from addUpdateInspectionFinding');
													self.addUpdateInspectionFindingFindingType(ressp, transact, transactionid)
														.then((respp) => {
															console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionfindingfindingtype, transact, transactionid): returning result from addUpdateInspectionFindingFindingType');
															resolve(respp);

														}).catch((error_0) => {
															console.log(error_0);
															console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from addUpdateInspectionRequest: ' + error_0);
															reject(error_0);
														});
												})
												.catch(error_1 => {
													console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_1);
													reject(error_1);
												});
										})
										.catch(error_2 => {
											console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_2);
											reject(error_2);
										});
								})
								.catch(error_3 => {
									console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_3);
									reject(error_3);
								});
						})
						.catch(error_4 => {
							console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_4);
							reject(error_4);
						});
				})
				.catch(error_5 => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: update(inspectionrequest, transact, transactionid): returning error from checkEmptyFields: ' + error_5);
					reject(error_5);
				});

		});
	}//end update


    /**
	   * Get a specific Inspection Request
	   *
	   * @param {Integer} Inspection_Request_ID - E2E_Inspection_Request ID
	   * @param {String} Program - E2E_Inspection_Request Program
	   * @param {String} Aircraft_Type - E2E_Inspection_Request Aircraft_Type
	   * @param {String} Applicable_MSN_Range - E2E_Inspection_Request Applicable_MSN_Range
	   * @param {String} Priority - E2E_Inspection_Request Priority
	   * @param {String} Stress_Leader - E2E_Inspection_Request Stress_Leader
	   * @param {String} NDI_compiler - E2E_Inspection_Request NDI_compiler
	   * @param {String} Test_Leader - E2E_Inspection_Request Test_Leader
	   * @param {String} Requestor - E2E_Inspection_Request Requestor
	   * @param {integer} NDT_Story_ID - E2E_Inspection_Request NDT_Story_ID
	   * @param {integer} index - Starting index to retrieve the records (default:0)
	   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	   * @param {String} ordercolumn - Order the records by which column
	   * @param {Integer} search - 0:return SQL string, 1:Search database
	   * @returns {Object}
	   */
	//@param {integer} index - Starting index to retrieve the records (default:0)
	// * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
	// * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
	// * @param {String} ordercolumn - Order the records by which column
	get(param, search, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid)');
			console.log('(' + transactionid + '): parameter app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): ' + JSON.stringify(param));

			// if (param.index == null){
			//     param.index = 0;
			// }else{
			//     if (param.index > 0){
			//         param.index = param.index -1;
			//     }
			// }

			// if (param.row == null){
			//     param.row = 30;
			// }

			// if (param.order == null){
			//     param.order = 'DESC';
			// }

			// if (param.ordercolumn == null){
			//     param.ordercolumn = 'NDT_Story_ID';
			// }

			var isinspectionareaparamprovided = false;//check if inspectionarea param is provided
			var isinspectionareacycleparamprovided = false;//check if inspectionareacycle param is provided
			var isinspectionfindingparamprovided = false;//check if inspectionfinding param is provided
			var isinspectionrequestparamprovided = false;//check if inspectionrequest param is provided

			//create the overall sql statements
			var sql_query =
				' SELECT  ' +
				' DISTINCT ' +
				' Inspection_Request.ID as Inspection_Request_ID ' +
				' FROM E2E_Inspection_Request Inspection_Request ' +
				' LEFT JOIN ' +
				' ( ' +
				' SELECT  ' +
				' Inspection_Area.Inspection_Request_ID, ' +
				' Inspection_Area.ID as Inspection_Area_ID, ' +
				' Inspection_Area_Cycle.Inspection_Area_Cycle_ID, ' +
				' Inspection_Area_Cycle.Inspection_Finding_ID    ' +
				' FROM E2E_Inspection_Area Inspection_Area  ' +
				' LEFT JOIN ' +
				' ( ' +
				' SELECT  ' +
				' Inspection_Area_Cycle.Inspection_Area_ID, ' +
				' Inspection_Area_Cycle.ID as Inspection_Area_Cycle_ID, ' +
				' Inspection_Finding_ID ' +
				' FROM E2E_Inspection_Area_Cycle Inspection_Area_Cycle ' +
				' LEFT JOIN ' +
				' ( ' +
				' SELECT  ' +
				' Inspection_Finding.Inspection_Area_Cycle_ID, ' +
				' Inspection_Finding.ID as Inspection_Finding_ID ' +
				' FROM E2E_Inspection_Finding Inspection_Finding ' +
				' LEFT JOIN E2E_Finding_Category Finding_Category ' +
				' ON Inspection_Finding.Finding_Category_ID = Finding_Category.ID ' +
				' LEFT JOIN E2E_Repair_Type Repair_Type ' +
				' ON Inspection_Finding.Repair_Type_ID = Repair_Type.ID ' +
				' LEFT JOIN E2E_Environment_Category Environment_Category ' +
				' ON Inspection_Finding.Environment_Category_ID = Environment_Category.ID ' +
				' LEFT JOIN E2E_EASA_AMC_2020_Level EASA_AMC_2020_Level ' +
				' ON Inspection_Finding.EASA_AMC_2020_Level_ID = EASA_AMC_2020_Level.ID ' +
				' LEFT JOIN E2E_Part_Criticity Part_Criticity ' +
				' ON Inspection_Finding.Part_Criticity_ID = Part_Criticity.ID ' +
				' LEFT JOIN E2E_Finding_Type Finding_Type ' +
				' ON Inspection_Finding.Finding_Type_ID = Finding_Type.ID ' +
				' WHERE Inspection_Finding.deleted_at is null ' +
				' @inspectionfinding_id ' +
				' @inspectionfinding_ipname ' +
				' @inspectionfinding_findingdate ' +
				' @inspectionfinding_flyingcyclereal ' +
				' @inspectionfinding_findingcategoryid ' +
				' @inspectionfinding_findingcategory ' +
				' @inspectionfinding_numberspot ' +
				' @inspectionfinding_length ' +
				' @inspectionfinding_width ' +
				' @inspectionfinding_surface ' +
				' @inspectionfinding_orientation ' +
				' @inspectionfinding_anglereference ' +
				' @inspectionfinding_depthrange ' +
				' @inspectionfinding_depth ' +
				' @inspectionfinding_layer ' +
				' @inspectionfinding_remainingthickness ' +
				' @inspectionfinding_rotationangle ' +
				' @inspectionfinding_repairtypeid ' +
				' @inspectionfinding_repairtype ' +
				' @inspectionfinding_repairreference ' +
				' @inspectionfinding_partnumber ' +
				' @inspectionfinding_partserialnumber ' +
				' @inspectionfinding_environmentcategoryid ' +
				' @inspectionfinding_environmentcategory ' +
				' @inspectionfinding_easaamc2020levelid ' +
				' @inspectionfinding_easaamc2020level ' +
				' @inspectionfinding_ssireference ' +
				' @inspectionfinding_rmtfile ' +
				' @inspectionfinding_rdasreference ' +
				' @inspectionfinding_corrosionmaterial1 ' +
				' @inspectionfinding_corrosionmaterial2 ' +
				' @inspectionfinding_corrosionfastenerspecification ' +
				' @inspectionfinding_partcriticityid ' +
				' @inspectionfinding_partcriticity ' +
				' @inspectionfinding_inspectionareacycleid ' +
				' @inspectionfinding_findingtypeid ' +
				' @inspectionfinding_findingtype ' +
				' ) Inspection_Finding ' +
				' ON Inspection_Area_Cycle.ID = Inspection_Finding.Inspection_Area_Cycle_ID ' +
				' LEFT JOIN E2E_Airline Airline ' +
				' ON Inspection_Area_Cycle.Airline_ID = Airline.ID ' +
				' LEFT JOIN E2E_Equipment_Type Equipment_Type ' +
				' ON Inspection_Area_Cycle.Equipment_Type_ID = Equipment_Type.ID ' +
				' WHERE Inspection_Area_Cycle.deleted_at is null ' +
				' @inspectionareacycle_id ' +
				' @inspectionareacycle_inspectioncampaign ' +
				' @inspectionareacycle_campaigndate ' +
				' @inspectionareacycle_equipmenttypeid ' +
				' @inspectionareacycle_equipmenttype ' +
				' @inspectionareacycle_inspectionareaid ' +
				' @inspectionareacycle_aircraftmodel ' +
				' @inspectionareacycle_msn ' +
				' @inspectionareacycle_entryintoservice ' +
				' @inspectionareacycle_manufacturingdate ' +
				' @inspectionareacycle_airlineid ' +
				' @inspectionareacycle_airline ' +
				' @inspectionareacycle_geographicalarea ' +
				' @inspectionareacycle_flyingcycle ' +
				' @inspectionareacycle_flyinghours ' +
				' ) Inspection_Area_Cycle ' +
				' ON Inspection_Area.ID = Inspection_Area_Cycle.Inspection_Area_ID ' +
				' LEFT JOIN E2E_Area_Inspectability Area_Inspectability ' +
				' ON Inspection_Area.Area_Inspectability_ID = Area_Inspectability.ID ' +
				' LEFT JOIN E2E_Side Side ' +
				' ON Inspection_Area.Side_ID = Side.ID ' +
				' LEFT JOIN E2E_Assembly_Condition Assembly_Condition ' +
				' ON Inspection_Area.Assembly_Condition_ID = Assembly_Condition.ID ' +
				' LEFT JOIN E2E_Inspection_Type Inspection_Type ' +
				' ON Inspection_Area.Inspection_Type_ID = Inspection_Type.ID ' +
				' LEFT JOIN E2E_Inspection_Method Inspection_Method ' +
				' ON Inspection_Area.Inspection_Method_ID = Inspection_Method.ID ' +
				' LEFT JOIN  ' +
				' ( ' +
				' SELECT Inspection_Area__Finding_Type.ID as Inspection_Area__Finding_Type_ID,Inspection_Area__Finding_Type.Inspection_Area_ID, Inspection_Area__Finding_Type.Finding_Type_ID, Finding_Type.Value as Finding_Type FROM E2E_Inspection_Area__Finding_Type Inspection_Area__Finding_Type LEFT JOIN E2E_Finding_Type Finding_Type  ' +
				' on Inspection_Area__Finding_Type.Finding_Type_ID=Finding_Type.ID WHERE Inspection_Area__Finding_Type.deleted_at is null and Finding_Type.deleted_at is null ' +
				' ) Inspection_Area__Finding_Type ' +
				' ON Inspection_Area.ID = Inspection_Area__Finding_Type.Inspection_Area_ID ' +
				' WHERE Inspection_Area.deleted_at is null  ' +
				' @inspectionarea_id ' +
				' @inspectionarea_areainspectabilityid ' +
				' @inspectionarea_areainspectability ' +
				' @inspectionarea_sideid ' +
				' @inspectionarea_side ' +
				' @inspectionarea_assemblyconditionid ' +
				' @inspectionarea_assemblycondition ' +
				' @inspectionarea_inspectioneffort ' +
				' @inspectionarea_material ' +
				' @inspectionarea_inspectiontypeid ' +
				' @inspectionarea_inspectiontype ' +
				' @inspectionarea_inspectionmethodid ' +
				' @inspectionarea_inspectionmethod ' +
				' @inspectionarea_inspectionrequestid ' +
				' @inspectionarea_inspectionareafindingtypeid ' +
				' @inspectionarea_findingtypeid ' +
				' @inspectionarea_findingtype ' +
				' GROUP BY Inspection_Area.ID,Inspection_Area_Cycle.Inspection_Finding_ID ' +
				' ) Inspection_Area ' +
				' ON Inspection_Request.ID = Inspection_Area.Inspection_Request_ID ' +
				' LEFT JOIN E2E_NDT_Story NDT_Story  ' +
				' ON Inspection_Request.NDT_Story_ID = NDT_Story.ID ' +
				' WHERE Inspection_Request.deleted_at is null  ' +
				' @inspectionrequest_id ' +
				' @inspectionrequest_program ' +
				' @inspectionrequest_applicablemsnrange ' +
				' @inspectionrequest_priority ' +
				' @inspectionrequest_stressleader ' +
				' @inspectionrequest_ndicompiler ' +
				' @inspectionrequest_testleader ' +
				' @inspectionrequest_requestor ' +
				' @inspectionrequest_ndtstoryid ' +
				' @inspectionrequest_ndtstory ' +
				' @inspectionrequest_aircrafttype ' +
				' @isinspectionareaparamprovided ' +
				' @isinspectionareacycleparamprovided ' +
				' @isinspectionfindingparamprovided ' +
				' @isinspectionrequestparamprovided ' +
				' ; ';

			//inspection_finding section
			var replace_query_inspectionfinding_id = ' AND Inspection_Finding.ID in (@replace) ';
			var replace_query_inspectionfinding_ipname = ' AND lcase(Inspection_Finding.IP_Name) in (@replace) ';
			var replace_query_inspectionfinding_findingdate = ' AND Inspection_Finding.Finding_Date >= (@replace) and Inspection_Finding.Finding_Date <= (@replace) ';
			var replace_query_inspectionfinding_flyingcyclereal = ' AND Inspection_Finding.Flying_Cycle_Real >= @replace and Inspection_Finding.Flying_Cycle_Real <= @replace ';
			var replace_query_inspectionfinding_findingcategoryid = ' AND Inspection_Finding.Finding_Category_ID in (@replace) ';
			var replace_query_inspectionfinding_findingcategory = ' AND lcase(Finding_Category.Value) in (@replace) ';
			var replace_query_inspectionfinding_numberspot = ' AND Inspection_Finding.Number_Spot >= @replace and Inspection_Finding.Number_Spot <= @replace ';
			var replace_query_inspectionfinding_length = ' AND Inspection_Finding.Length >= @replace and Inspection_Finding.Length <= @replace ';
			var replace_query_inspectionfinding_width = ' AND Inspection_Finding.Width >= @replace and Inspection_Finding.Width <= @replace ';
			var replace_query_inspectionfinding_surface = ' AND Inspection_Finding.Surface >= @replace and Inspection_Finding.Surface <= @replace ';
			var replace_query_inspectionfinding_orientation = ' AND Inspection_Finding.Orientation >= @replace and Inspection_Finding.Orientation <= @replace ';
			var replace_query_inspectionfinding_anglereference = ' AND lcase(Inspection_Finding.Angle_Reference) in (@replace) ';
			var replace_query_inspectionfinding_depthrange = ' AND Inspection_Finding.Starting_Depth >= @replace and Inspection_Finding.Ending_Depth <= @replace ';
			var replace_query_inspectionfinding_depth = ' AND Inspection_Finding.Depth >= @replace and Inspection_Finding.Depth <= @replace ';
			var replace_query_inspectionfinding_layer = ' AND lcase(Inspection_Finding.Layer) in (@replace) ';
			var replace_query_inspectionfinding_remainingthickness = ' AND Inspection_Finding.Remaining_Thickness >= @replace and Inspection_Finding.Remaining_Thickness <= @replace ';
			var replace_query_inspectionfinding_rotationangle = ' AND Inspection_Finding.Rotation_Angle >= @replace and Inspection_Finding.Rotation_Angle <= @replace ';
			var replace_query_inspectionfinding_repairtypeid = ' AND Inspection_Finding.Repair_Type_ID in (@replace) ';
			var replace_query_inspectionfinding_repairtype = ' AND lcase(Repair_Type.Value) in (@replace) ';
			var replace_query_inspectionfinding_repairreference = ' AND lcase(Inspection_Finding.Repair_Reference) in (@replace) ';
			var replace_query_inspectionfinding_partnumber = ' AND lcase(Inspection_Finding.Part_Number) in (@replace) ';
			var replace_query_inspectionfinding_partserialnumber = ' AND lcase(Inspection_Finding.Part_Serial_Number) in (@replace) ';
			var replace_query_inspectionfinding_environmentcategoryid = ' AND Inspection_Finding.Environment_Category_ID in (@replace) ';
			var replace_query_inspectionfinding_environmentcategory = ' AND lcase(Environment_Category.Value) in (@replace) ';
			var replace_query_inspectionfinding_easaamc2020levelid = ' AND Inspection_Finding.EASA_AMC_2020_Level_ID in (@replace) ';
			var replace_query_inspectionfinding_easaamc2020level = ' AND lcase(EASA_AMC_2020_Level.Value) in (@replace) ';
			var replace_query_inspectionfinding_ssireference = ' AND lcase(Inspection_Finding.SSI_Reference) in (@replace) ';
			var replace_query_inspectionfinding_rmtfile = ' AND lcase(Inspection_Finding.RMT_File) in (@replace) ';
			var replace_query_inspectionfinding_rdasreference = ' AND lcase(Inspection_Finding.RDAS_Reference) in (@replace) ';
			var replace_query_inspectionfinding_corrosionmaterial1 = ' AND lcase(Inspection_Finding.Corrosion_Material_1) in (@replace) ';
			var replace_query_inspectionfinding_corrosionmaterial2 = ' AND lcase(Inspection_Finding.Corrosion_Material_2) in (@replace) ';
			var replace_query_inspectionfinding_corrosionfastenerspecification = ' AND lcase(Inspection_Finding.Corrosion_Fastener_Specification) in (@replace) ';
			var replace_query_inspectionfinding_partcriticityid = ' AND Inspection_Finding.Part_Criticity_ID in (@replace) ';
			var replace_query_inspectionfinding_partcriticity = ' AND lcase(Part_Criticity.Value) in (@replace) ';
			var replace_query_inspectionfinding_inspectionareacycleid = ' AND Inspection_Finding.Inspection_Area_Cycle_ID in (@replace) ';
			var replace_query_inspectionfinding_findingtypeid = ' AND Inspection_Finding.Finding_Type_ID in (@replace) ';
			var replace_query_inspectionfinding_findingtype = ' AND lcase(Finding_Type.Value) in (@replace) ';

			//inspection_area_cycle section
			var replace_query_inspectionareacycle_id = ' AND Inspection_Area_Cycle.ID in (@replace) ';
			var replace_query_inspectionareacycle_inspectioncampaign = ' AND lcase(Inspection_Area_Cycle.Inspection_Campaign) in (@replace) ';
			var replace_query_inspectionareacycle_campaigndate = ' AND Inspection_Area_Cycle.Campaign_Date >= (@replace) and Inspection_Area_Cycle.Campaign_Date <= (@replace) ';
			var replace_query_inspectionareacycle_equipmenttypeid = ' AND Inspection_Area_Cycle.Equipment_Type_ID in (@replace) ';
			var replace_query_inspectionareacycle_equipmenttype = ' AND lcase(Equipment_Type.Value) in (@replace) ';
			var replace_query_inspectionareacycle_inspectionareaid = ' AND Inspection_Area_Cycle.Inspection_Area_ID in (@replace) ';
			var replace_query_inspectionareacycle_aircraftmodel = ' AND lcase(Inspection_Area_Cycle.Aircraft_Model) in (@replace) ';
			var replace_query_inspectionareacycle_msn = ' AND lcase(Inspection_Area_Cycle.MSN) in (@replace) ';
			var replace_query_inspectionareacycle_entryintoservice = ' AND Inspection_Area_Cycle.Entry_Into_Service >= (@replace) and Inspection_Area_Cycle.Entry_Into_Service <= (@replace) ';
			var replace_query_inspectionareacycle_manufacturingdate = ' AND Inspection_Area_Cycle.Manufacturing_Date >= (@replace) and Inspection_Area_Cycle.Manufacturing_Date <= (@replace) ';
			var replace_query_inspectionareacycle_airlineid = ' AND Inspection_Area_Cycle.Airline_ID in (@replace) ';
			var replace_query_inspectionareacycle_airline = ' AND lcase(Airline.Value) in (@replace) ';
			var replace_query_inspectionareacycle_geographicalarea = ' AND lcase(Inspection_Area_Cycle.Geographical_Area) in (@replace) ';
			var replace_query_inspectionareacycle_flyingcycle = ' AND Inspection_Area_Cycle.Flying_Cycle >= @replace and Inspection_Area_Cycle.Flying_Cycle <= @replace ';
			var replace_query_inspectionareacycle_flyinghours = ' AND Inspection_Area_Cycle.Flying_Hours >= @replace and Inspection_Area_Cycle.Flying_Hours <= @replace ';

			//inspection_area section
			var replace_query_inspectionarea_id = ' AND Inspection_Area.ID in (@replace) ';
			var replace_query_inspectionarea_areainspectabilityid = ' AND Inspection_Area.Area_Inspectability_ID in (@replace) ';
			var replace_query_inspectionarea_areainspectability = ' AND lcase(Area_Inspectability.Value) in (@replace) ';
			var replace_query_inspectionarea_sideid = ' AND Inspection_Area.Side_ID in (@replace) ';
			var replace_query_inspectionarea_side = ' AND lcase(Side.Value) in (@replace) ';
			var replace_query_inspectionarea_assemblyconditionid = ' AND Inspection_Area.Assembly_Condition_ID in (@replace) ';
			var replace_query_inspectionarea_assemblycondition = ' AND lcase(Assembly_Condition.Value) in (@replace) ';
			var replace_query_inspectionarea_inspectioneffort = ' AND lcase(Inspection_Area.Inspection_Effort) in (@replace) ';
			var replace_query_inspectionarea_material = ' AND lcase(Inspection_Area.Material) in (@replace) ';
			var replace_query_inspectionarea_inspectiontypeid = ' AND Inspection_Area.Inspection_Type_ID in (@replace) ';
			var replace_query_inspectionarea_inspectiontype = ' AND lcase(Inspection_Type.Value) in (@replace) ';
			var replace_query_inspectionarea_inspectionmethodid = ' AND Inspection_Area.Inspection_Method_ID in (@replace) ';
			var replace_query_inspectionarea_inspectionmethod = ' AND lcase(Inspection_Method.Value) in (@replace) ';
			var replace_query_inspectionarea_inspectionrequestid = ' AND Inspection_Area.Inspection_Request_ID in (@replace) ';
			var replace_query_inspectionarea_inspectionareafindingtypeid = ' AND Inspection_Area__Finding_Type.Inspection_Area__Finding_Type_ID in (@replace) ';
			var replace_query_inspectionarea_findingtypeid = ' AND Inspection_Area__Finding_Type.Finding_Type_ID in (@replace) ';
			var replace_query_inspectionarea_findingtype = ' AND lcase(Inspection_Area__Finding_Type.Finding_Type) in (@replace) ';

			//inspection_request section
			var replace_query_inspectionrequest_id = ' AND Inspection_Request.ID in (@replace) ';
			var replace_query_inspectionrequest_program = ' AND lcase(Inspection_Request.Program) in (@replace) ';
			var replace_query_inspectionrequest_applicablemsnrange = ' AND lcase(Inspection_Request.Applicable_MSN_Range) in (@replace) ';
			var replace_query_inspectionrequest_priority = ' AND lcase(Inspection_Request.Priority) in (@replace) ';
			var replace_query_inspectionrequest_stressleader = ' AND lcase(Inspection_Request.Stress_Leader) in (@replace) ';
			var replace_query_inspectionrequest_ndicompiler = ' AND lcase(Inspection_Request.NDI_Compiler) in (@replace) ';
			var replace_query_inspectionrequest_testleader = ' AND lcase(Inspection_Request.Test_Leader) in (@replace) ';
			var replace_query_inspectionrequest_requestor = ' AND lcase(Inspection_Request.Requestor) in (@replace) ';
			var replace_query_inspectionrequest_ndtstoryid = ' AND Inspection_Request.NDT_Story_ID in (@replace) ';
			var replace_query_inspectionrequest_ndtstory = ' AND lcase(NDT_Story.Value) in (@replace) ';
			var replace_query_inspectionrequest_aircrafttype = ' AND lcase(Inspection_Request.Aircraft_Type) in (@replace) ';

			//param boolean
			var replace_query_isinspectionareaparamprovided = ' AND Inspection_Area.Inspection_Area_ID is not null ';
			var replace_query_isinspectionareacycleparamprovided = ' AND Inspection_Area.Inspection_Area_Cycle_ID is not null ';
			var replace_query_isinspectionfindingparamprovided = ' AND Inspection_Area.Inspection_Finding_ID is not null ';
			var replace_query_isinspectionrequestparamprovided = ' AND Inspection_Request.ID is not null ';

			//id replacement
			if (param.Inspection_Finding_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_id', replace_query_inspectionfinding_id.replace('@replace', param.Inspection_Finding_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_id', ''); }
			if (param.Finding_Category_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_findingcategoryid', replace_query_inspectionfinding_findingcategoryid.replace('@replace', param.Finding_Category_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_findingcategoryid', ''); }
			if (param.Repair_Type_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_repairtypeid', replace_query_inspectionfinding_repairtypeid.replace('@replace', param.Repair_Type_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_repairtypeid', ''); }
			if (param.Environment_Category_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_environmentcategoryid', replace_query_inspectionfinding_environmentcategoryid.replace('@replace', param.Environment_Category_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_environmentcategoryid', ''); }
			if (param.EASA_AMC_2020_Level_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_easaamc2020levelid', replace_query_inspectionfinding_easaamc2020levelid.replace('@replace', param.EASA_AMC_2020_Level_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_easaamc2020levelid', ''); }
			if (param.Part_Criticity_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_partcriticityid', replace_query_inspectionfinding_partcriticityid.replace('@replace', param.Part_Criticity_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_partcriticityid', ''); }
			if (param.Inspection_Area_Cycle_ID != null) {
				isinspectionareacycleparamprovided = true;
				sql_query = sql_query.replace('@inspectionareacycle_id', replace_query_inspectionareacycle_id.replace('@replace', param.Inspection_Area_Cycle_ID));
				sql_query = sql_query.replace('@inspectionfinding_inspectionareacycleid', replace_query_inspectionfinding_inspectionareacycleid.replace('@replace', param.Inspection_Area_Cycle_ID));
			} else {
				sql_query = sql_query.replace('@inspectionareacycle_id', '');
				sql_query = sql_query.replace('@inspectionfinding_inspectionareacycleid', '');
			}
			if (param.Finding_Type_ID != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_findingtypeid', replace_query_inspectionfinding_findingtypeid.replace('@replace', param.Finding_Type_ID)); } else { sql_query = sql_query.replace('@inspectionfinding_findingtypeid', ''); }
			if (param.Equipment_Type_ID != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_equipmenttypeid', replace_query_inspectionareacycle_equipmenttypeid.replace('@replace', param.Equipment_Type_ID)); } else { sql_query = sql_query.replace('@inspectionareacycle_equipmenttypeid', ''); }
			if (param.Inspection_Area_ID != null) {
				isinspectionareaparamprovided = true;
				sql_query = sql_query.replace('@inspectionarea_id', replace_query_inspectionarea_id.replace('@replace', param.Inspection_Area_ID));
				sql_query = sql_query.replace('@inspectionareacycle_inspectionareaid', replace_query_inspectionareacycle_inspectionareaid.replace('@replace', param.Inspection_Area_ID));
			} else {
				sql_query = sql_query.replace('@inspectionarea_id', '');
				sql_query = sql_query.replace('@inspectionareacycle_inspectionareaid', '');
			}
			if (param.Airline_ID != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_airlineid', replace_query_inspectionareacycle_airlineid.replace('@replace', param.Airline_ID)); } else { sql_query = sql_query.replace('@inspectionareacycle_airlineid', ''); }
			if (param.Area_Inspectability_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_areainspectabilityid', replace_query_inspectionarea_areainspectabilityid.replace('@replace', param.Area_Inspectability_ID)); } else { sql_query = sql_query.replace('@inspectionarea_areainspectabilityid', ''); }
			if (param.Side_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_sideid', replace_query_inspectionarea_sideid.replace('@replace', param.Side_ID)); } else { sql_query = sql_query.replace('@inspectionarea_sideid', ''); }
			if (param.Assembly_Condition_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_assemblyconditionid', replace_query_inspectionarea_assemblyconditionid.replace('@replace', param.Assembly_Condition_ID)); } else { sql_query = sql_query.replace('@inspectionarea_assemblyconditionid', ''); }
			if (param.Inspection_Type_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_inspectiontypeid', replace_query_inspectionarea_inspectiontypeid.replace('@replace', param.Inspection_Type_ID)); } else { sql_query = sql_query.replace('@inspectionarea_inspectiontypeid', ''); }
			if (param.Inspection_Method_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_inspectionmethodid', replace_query_inspectionarea_inspectionmethodid.replace('@replace', param.Inspection_Method_ID)); } else { sql_query = sql_query.replace('@inspectionarea_inspectionmethodid', ''); }
			if (param.Inspection_Request_ID != null) {
				isinspectionrequestparamprovided = true;
				sql_query = sql_query.replace('@inspectionrequest_id', replace_query_inspectionrequest_id.replace('@replace', param.Inspection_Request_ID));
				sql_query = sql_query.replace('@inspectionarea_inspectionrequestid', replace_query_inspectionarea_inspectionrequestid.replace('@replace', param.Inspection_Request_ID));
			} else {
				sql_query = sql_query.replace('@inspectionrequest_id', '');
				sql_query = sql_query.replace('@inspectionarea_inspectionrequestid', '');
			}
			if (param.Inspection_Area__Finding_Type_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_inspectionareafindingtypeid', replace_query_inspectionarea_inspectionareafindingtypeid.replace('@replace', param.Inspection_Area__Finding_Type_ID)); } else { sql_query = sql_query.replace('@inspectionarea_inspectionareafindingtypeid', ''); }
			if (param.Expected_Finding_Type_ID != null) { isinspectionareaparamprovided = true; sql_query = sql_query.replace('@inspectionarea_findingtypeid', replace_query_inspectionarea_findingtypeid.replace('@replace', param.Expected_Finding_Type_ID)); } else { sql_query = sql_query.replace('@inspectionarea_findingtypeid', ''); }
			if (param.NDT_Story_ID != null) { isinspectionrequestparamprovided = true; sql_query = sql_query.replace('@inspectionrequest_ndtstoryid', replace_query_inspectionrequest_ndtstoryid.replace('@replace', param.NDT_Story_ID)); } else { sql_query = sql_query.replace('@inspectionrequest_ndtstoryid', ''); }

			//string replacement
			if (param.IP_Name != null) { isinspectionfindingparamprovided = true; var value = param.IP_Name; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_ipname', replace_query_inspectionfinding_ipname.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_ipname', ''); }
			if (param.Finding_Category != null) { isinspectionfindingparamprovided = true; var value = param.Finding_Category; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_findingcategory', replace_query_inspectionfinding_findingcategory.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_findingcategory', ''); }
			if (param.Angle_Reference != null) { isinspectionfindingparamprovided = true; var value = param.Angle_Reference; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_anglereference', replace_query_inspectionfinding_anglereference.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_anglereference', ''); }
			if (param.Layer != null) { isinspectionfindingparamprovided = true; var value = param.Layer; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_layer', replace_query_inspectionfinding_layer.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_layer', ''); }
			if (param.Repair_Type != null) { isinspectionfindingparamprovided = true; var value = param.Repair_Type; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_repairtype', replace_query_inspectionfinding_repairtype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_repairtype', ''); }
			if (param.Repair_Reference != null) { isinspectionfindingparamprovided = true; var value = param.Repair_Reference; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_repairreference', replace_query_inspectionfinding_repairreference.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_repairreference', ''); }
			if (param.Part_Number != null) { isinspectionfindingparamprovided = true; var value = param.Part_Number; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_partnumber', replace_query_inspectionfinding_partnumber.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_partnumber', ''); }
			if (param.Part_Serial_Number != null) { isinspectionfindingparamprovided = true; var value = param.Part_Serial_Number; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_partserialnumber', replace_query_inspectionfinding_partserialnumber.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_partserialnumber', ''); }
			if (param.Environment_Category != null) { isinspectionfindingparamprovided = true; var value = param.Environment_Category; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_environmentcategory', replace_query_inspectionfinding_environmentcategory.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_environmentcategory', ''); }
			if (param.EASA_AMC_2020_Level != null) { isinspectionfindingparamprovided = true; var value = param.EASA_AMC_2020_Level; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_easaamc2020level', replace_query_inspectionfinding_easaamc2020level.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_easaamc2020level', ''); }
			if (param.SSI_Reference != null) { isinspectionfindingparamprovided = true; var value = param.SSI_Reference; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_ssireference', replace_query_inspectionfinding_ssireference.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_ssireference', ''); }
			if (param.RMT_File != null) { isinspectionfindingparamprovided = true; var value = param.RMT_File; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_rmtfile', replace_query_inspectionfinding_rmtfile.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_rmtfile', ''); }
			if (param.RDAS_Reference != null) { isinspectionfindingparamprovided = true; var value = param.RDAS_Reference; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_rdasreference', replace_query_inspectionfinding_rdasreference.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_rdasreference', ''); }
			if (param.Corrosion_Material_1 != null) { isinspectionfindingparamprovided = true; var value = param.Corrosion_Material_1; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_corrosionmaterial1', replace_query_inspectionfinding_corrosionmaterial1.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_corrosionmaterial1', ''); }
			if (param.Corrosion_Material_2 != null) { isinspectionfindingparamprovided = true; var value = param.Corrosion_Material_2; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_corrosionmaterial2', replace_query_inspectionfinding_corrosionmaterial2.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_corrosionmaterial2', ''); }
			if (param.Corrosion_Fastener_Specification != null) { isinspectionfindingparamprovided = true; var value = param.Corrosion_Fastener_Specification; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_corrosionfastenerspecification', replace_query_inspectionfinding_corrosionfastenerspecification.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_corrosionfastenerspecification', ''); }
			if (param.Part_Criticity != null) { isinspectionfindingparamprovided = true; var value = param.Part_Criticity; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_partcriticity', replace_query_inspectionfinding_partcriticity.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_partcriticity', ''); }
			if (param.Finding_Type != null) { isinspectionfindingparamprovided = true; var value = param.Finding_Type; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionfinding_findingtype', replace_query_inspectionfinding_findingtype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionfinding_findingtype', ''); }
			if (param.Inspection_Campaign != null) { isinspectionareacycleparamprovided = true; var value = param.Inspection_Campaign; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_inspectioncampaign', replace_query_inspectionareacycle_inspectioncampaign.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_inspectioncampaign', ''); }
			if (param.Type_of_Equipment != null) { isinspectionareacycleparamprovided = true; var value = param.Type_of_Equipment; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_equipmenttype', replace_query_inspectionareacycle_equipmenttype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_equipmenttype', ''); }
			if (param.Aircraft_Model != null) { isinspectionareacycleparamprovided = true; var value = param.Aircraft_Model; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_aircraftmodel', replace_query_inspectionareacycle_aircraftmodel.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_aircraftmodel', ''); }
			if (param.MSN != null) { isinspectionareacycleparamprovided = true; var value = param.MSN; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_msn', replace_query_inspectionareacycle_msn.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_msn', ''); }
			if (param.Airlines != null) { isinspectionareacycleparamprovided = true; var value = param.Airlines; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_airline', replace_query_inspectionareacycle_airline.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_airline', ''); }
			if (param.Geographical_Area != null) { isinspectionareacycleparamprovided = true; var value = param.Geographical_Area; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionareacycle_geographicalarea', replace_query_inspectionareacycle_geographicalarea.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionareacycle_geographicalarea', ''); }
			if (param.Area_Inspectability != null) { isinspectionareaparamprovided = true; var value = param.Area_Inspectability; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_areainspectability', replace_query_inspectionarea_areainspectability.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_areainspectability', ''); }
			if (param.Side != null) { isinspectionareaparamprovided = true; var value = param.Side; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_side', replace_query_inspectionarea_side.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_side', ''); }
			if (param.Assembly_Condition != null) { isinspectionareaparamprovided = true; var value = param.Assembly_Condition; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_assemblycondition', replace_query_inspectionarea_assemblycondition.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_assemblycondition', ''); }
			if (param.Inspection_Effort != null) { isinspectionareaparamprovided = true; var value = param.Inspection_Effort; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_inspectioneffort', replace_query_inspectionarea_inspectioneffort.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_inspectioneffort', ''); }
			if (param.Material != null) { isinspectionareaparamprovided = true; var value = param.Material; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_material', replace_query_inspectionarea_material.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_material', ''); }
			if (param.Inspection_Type != null) { isinspectionareaparamprovided = true; var value = param.Inspection_Type; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_inspectiontype', replace_query_inspectionarea_inspectiontype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_inspectiontype', ''); }
			if (param.Inspection_Method != null) { isinspectionareaparamprovided = true; var value = param.Inspection_Method; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_inspectionmethod', replace_query_inspectionarea_inspectionmethod.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_inspectionmethod', ''); }
			if (param.Expected_Finding_Type != null) { isinspectionareaparamprovided = true; var value = param.Expected_Finding_Type; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionarea_findingtype', replace_query_inspectionarea_findingtype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionarea_findingtype', ''); }
			if (param.Program != null) { isinspectionrequestparamprovided = true; var value = param.Program; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_program', replace_query_inspectionrequest_program.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_program', ''); }
			if (param.Applicable_MSN_Range != null) { isinspectionrequestparamprovided = true; var value = param.Applicable_MSN_Range; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_applicablemsnrange', replace_query_inspectionrequest_applicablemsnrange.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_applicablemsnrange', ''); }
			if (param.Priority != null) { isinspectionrequestparamprovided = true; var value = param.Priority; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_priority', replace_query_inspectionrequest_priority.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_priority', ''); }
			if (param.Stress_Leader != null) { isinspectionrequestparamprovided = true; var value = param.Stress_Leader; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_stressleader', replace_query_inspectionrequest_stressleader.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_stressleader', ''); }
			if (param.NDI_Compiler != null) { isinspectionrequestparamprovided = true; var value = param.NDI_Compiler; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_ndicompiler', replace_query_inspectionrequest_ndicompiler.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_ndicompiler', ''); }
			if (param.Test_Leader != null) { isinspectionrequestparamprovided = true; var value = param.Test_Leader; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_testleader', replace_query_inspectionrequest_testleader.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_testleader', ''); }
			if (param.Requestor != null) { isinspectionrequestparamprovided = true; var value = param.Requestor; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_requestor', replace_query_inspectionrequest_requestor.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_requestor', ''); }
			if (param.NDT_Story != null) { isinspectionrequestparamprovided = true; var value = param.NDT_Story; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_ndtstory', replace_query_inspectionrequest_ndtstory.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_ndtstory', ''); }
			if (param.Aircraft_Type != null) { isinspectionrequestparamprovided = true; var value = param.Aircraft_Type; if (value.split(',').length > 1) { value = '\'' + value.replace(/,/g, "\',\'") + '\''; } else { value = '\'' + value + '\''; } sql_query = sql_query.replace('@inspectionrequest_aircrafttype', replace_query_inspectionrequest_aircrafttype.replace('@replace', value.toLowerCase())); } else { sql_query = sql_query.replace('@inspectionrequest_aircrafttype', ''); }

			//numerial range replacement
			if (param.Flying_Cycle_Real != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_flyingcyclereal', replace_query_inspectionfinding_flyingcyclereal.replace(/@replace/g, param.Flying_Cycle_Real)); } else { sql_query = sql_query.replace('@inspectionfinding_flyingcyclereal', ''); }
			if (param.Number_Spot != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_numberspot', replace_query_inspectionfinding_numberspot.replace(/@replace/g, param.Number_Spot)); } else { sql_query = sql_query.replace('@inspectionfinding_numberspot', ''); }
			if (param.Length != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_length', replace_query_inspectionfinding_length.replace(/@replace/g, param.Length)); } else { sql_query = sql_query.replace('@inspectionfinding_length', ''); }
			if (param.Width != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_width', replace_query_inspectionfinding_width.replace(/@replace/g, param.Width)); } else { sql_query = sql_query.replace('@inspectionfinding_width', ''); }
			if (param.Surface != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_surface', replace_query_inspectionfinding_surface.replace(/@replace/g, param.Surface)); } else { sql_query = sql_query.replace('@inspectionfinding_surface', ''); }
			if (param.Orientation != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_orientation', replace_query_inspectionfinding_orientation.replace(/@replace/g, param.Orientation)); } else { sql_query = sql_query.replace('@inspectionfinding_orientation', ''); }
			if (param.Starting_Depth != null || param.Ending_Depth != null) {
				isinspectionfindingparamprovided = true;
				if (param.Starting_Depth != null) {
					replace_query_inspectionfinding_depthrange = replace_query_inspectionfinding_depthrange.replace('@replace', param.Starting_Depth);
				} else {
					replace_query_inspectionfinding_depthrange = replace_query_inspectionfinding_depthrange.replace('@replace', '');
				}
				if (param.Ending_Depth != null) {
					replace_query_inspectionfinding_depthrange = replace_query_inspectionfinding_depthrange.replace('@replace', param.Ending_Depth);
				} else {
					replace_query_inspectionfinding_depthrange = replace_query_inspectionfinding_depthrange.replace('@replace', '');
				}
				sql_query = sql_query.replace('@inspectionfinding_depthrange', replace_query_inspectionfinding_depthrange);
			} else {
				sql_query = sql_query.replace('@inspectionfinding_depthrange', '');
			}
			if (param.Depth != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_depth', replace_query_inspectionfinding_depth.replace(/@replace/g, param.Depth)); } else { sql_query = sql_query.replace('@inspectionfinding_depth', ''); }
			if (param.Remaining_Thickness != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_remainingthickness', replace_query_inspectionfinding_remainingthickness.replace(/@replace/g, param.Remaining_Thickness)); } else { sql_query = sql_query.replace('@inspectionfinding_remainingthickness', ''); }
			if (param.Rotation_Angle != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_rotationangle', replace_query_inspectionfinding_rotationangle.replace(/@replace/g, param.Rotation_Angle)); } else { sql_query = sql_query.replace('@inspectionfinding_rotationangle', ''); }
			if (param.Flying_Cycle != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_flyingcycle', replace_query_inspectionareacycle_flyingcycle.replace(/@replace/g, param.Flying_Cycle)); } else { sql_query = sql_query.replace('@inspectionareacycle_flyingcycle', ''); }
			if (param.Flying_Hours != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_flyinghours', replace_query_inspectionareacycle_flyinghours.replace(/@replace/g, param.Flying_Hours)); } else { sql_query = sql_query.replace('@inspectionareacycle_flyinghours', ''); }

			//string range replacement
			if (param.Finding_Date != null) { isinspectionfindingparamprovided = true; sql_query = sql_query.replace('@inspectionfinding_findingdate', replace_query_inspectionfinding_findingdate.replace(/@replace/g, '\'' + param.Finding_Date + '\'')); } else { sql_query = sql_query.replace('@inspectionfinding_findingdate', ''); }
			if (param.Campaign_Date != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_campaigndate', replace_query_inspectionareacycle_campaigndate.replace(/@replace/g, '\'' + param.Campaign_Date + '\'')); } else { sql_query = sql_query.replace('@inspectionareacycle_campaigndate', ''); }
			if (param.Entry_Into_Service != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_entryintoservice', replace_query_inspectionareacycle_entryintoservice.replace(/@replace/g, '\'' + param.Entry_Into_Service + '\'')); } else { sql_query = sql_query.replace('@inspectionareacycle_entryintoservice', ''); }
			if (param.Manufacturing_Date != null) { isinspectionareacycleparamprovided = true; sql_query = sql_query.replace('@inspectionareacycle_manufacturingdate', replace_query_inspectionareacycle_manufacturingdate.replace(/@replace/g, '\'' + param.Manufacturing_Date + '\'')); } else { sql_query = sql_query.replace('@inspectionareacycle_manufacturingdate', ''); }

			//boolean param replacement
			if (isinspectionareacycleparamprovided == true) {
				sql_query = sql_query.replace('@isinspectionareacycleparamprovided', replace_query_isinspectionareacycleparamprovided);
			} else {
				sql_query = sql_query.replace('@isinspectionareacycleparamprovided', '');
			}
			if (isinspectionareaparamprovided == true) {
				sql_query = sql_query.replace('@isinspectionareaparamprovided', replace_query_isinspectionareaparamprovided);
			} else {
				sql_query = sql_query.replace('@isinspectionareaparamprovided', '');
			}
			if (isinspectionfindingparamprovided == true) {
				sql_query = sql_query.replace('@isinspectionfindingparamprovided', replace_query_isinspectionfindingparamprovided);
			} else {
				sql_query = sql_query.replace('@isinspectionfindingparamprovided', '');
			}
			if (isinspectionrequestparamprovided == true) {
				sql_query = sql_query.replace('@isinspectionrequestparamprovided', replace_query_isinspectionrequestparamprovided);
			} else {
				sql_query = sql_query.replace('@isinspectionrequestparamprovided', '');
			}

			console.log('(' + transactionid + '): query app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid):sql:' + sql_query);
			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
			db.sequelize.query(
				sql_query,
				{
					type: db.sequelize.QueryTypes.SELECT,
					useMaster: false
				})
				.then((res) => {
					if (search == 0) { //proceed to return the sql string
						if (res.length > 0) {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): returning result');
							resolve(sql_query);
						} else {
							console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): returning result');
							resolve('Not Found');
						}
					} else if (search == 1) {
						console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): returning result');

						if (res.length > 0) {//ensure there is inspection request record that meet the filter criteria
							//read all the inspection request id and concat as a string
							var ids_array = [];
							res.map(a => {
								if (a.Inspection_Request_ID != null) {
									ids_array.push(a.Inspection_Request_ID);
								}
							});
							//convert array to string
							ids_array = ids_array.toString();

							//get those inspection request id that meet the filter conditions and use it to retrieve the full inspection request
							var sql_query_retrieve_full_inspection_request =
								' SELECT ' +
								' Inspection_Request.ID as Inspection_Request_ID, ' +
								' Inspection_Request.Program, ' +
								' Inspection_Request.Aircraft_Type, ' +
								' Inspection_Request.Applicable_MSN_Range, ' +
								' Inspection_Request.Priority, ' +
								' Inspection_Request.Stress_Leader, ' +
								' Inspection_Request.NDI_Compiler, ' +
								' Inspection_Request.Test_Leader, ' +
								' Inspection_Request.Requestor, ' +
								' Inspection_Request.NDT_Story_ID, ' +
								' NDT_Story.Value as NDT_Story, ' +
								' Inspection_Area.Inspection_Area_ID, ' +
								' Inspection_Area.json_inspection_area, ' +
								' Inspection_Area.Inspection_Area_Cycle_ID, ' +
								' Inspection_Area.json_inspection_area_cycle, ' +
								' Inspection_Area.Inspection_Finding_ID, ' +
								' Inspection_Area.json_inspection_finding ' +
								' FROM E2E_Inspection_Request Inspection_Request ' +
								' LEFT JOIN ' +
								' ( ' +
								' SELECT ' +
								' Inspection_Area.Inspection_Request_ID, ' +
								' Inspection_Area.ID as Inspection_Area_ID, ' +
								' JSON_OBJECT( ' +
								' \'Inspection_Area_ID\',Inspection_Area.ID, ' +
								' \'Area_Description\',Inspection_Area.Area_Description, ' +
								' \'Area_Inspectability_ID\',Inspection_Area.Area_Inspectability_ID, ' +
								' \'Area_Inspectability\',Area_Inspectability.Value, ' +
								' \'Side_ID\',Inspection_Area.Side_ID, ' +
								' \'Side\',Side.Value, ' +
								' \'Assembly_Condition_ID\',Inspection_Area.Assembly_Condition_ID, ' +
								' \'Assembly_Condition\',Assembly_Condition.Value, ' +
								' \'Inspection_Effort\',Inspection_Area.Inspection_Effort, ' +
								' \'Material\',Inspection_Area.Material, ' +
								' \'Inspection_Type_ID\',Inspection_Area.Inspection_Type_ID, ' +
								' \'Inspection_Type\',Inspection_Type.Value, ' +
								' \'Inspection_Method_ID\',Inspection_Area.Inspection_Method_ID, ' +
								' \'Inspection_Method\',Inspection_Method.Value, ' +
								' \'Inspection_Procedure\',Inspection_Area.Inspection_Procedure, ' +
								' \'Inspection_Request_ID\',Inspection_Area.Inspection_Request_ID, ' +
								' \'Expected_Finding_Type\',CONCAT(\'[\',GROUP_CONCAT(CONCAT(\'{\"Finding_Type_ID\":\', Inspection_Area__Finding_Type.Inspection_Area__Finding_Type_ID, \', \"Finding_Type\":\"\',Inspection_Area__Finding_Type.Finding_Type, \'\", \"Inspection_Area__Finding_Type_ID\":\',Inspection_Area__Finding_Type.Inspection_Area__Finding_Type_ID, \', \"Inspection_Area_ID\":\',Inspection_Area__Finding_Type.Inspection_Area_ID,\'}\')),\']\') ' +
								' ) as json_inspection_area, ' +
								' Inspection_Area_Cycle.Inspection_Area_Cycle_ID, ' +
								' Inspection_Area_Cycle.json_inspection_area_cycle, ' +
								' Inspection_Area_Cycle.Inspection_Finding_ID, ' +
								' Inspection_Area_Cycle.json_inspection_finding ' +
								' FROM E2E_Inspection_Area Inspection_Area ' +
								' LEFT JOIN ' +
								' ( ' +
								' SELECT  ' +
								' Inspection_Area_Cycle.Inspection_Area_ID, ' +
								' Inspection_Area_Cycle.ID as Inspection_Area_Cycle_ID,  ' +
								' JSON_OBJECT( ' +
								' \'Inspection_Area_Cycle_ID\',Inspection_Area_Cycle.ID, ' +
								' \'Aircraft_Model\',Inspection_Area_Cycle.Aircraft_Model, ' +
								' \'MSN\',Inspection_Area_Cycle.MSN, ' +
								' \'Entry_Into_Service\',DATE_FORMAT(Inspection_Area_Cycle.Entry_Into_Service, \"%Y-%m-%d %T\") , ' +
								' \'Manufacturing_Date\',DATE_FORMAT(Inspection_Area_Cycle.Manufacturing_Date, \"%Y-%m-%d %T\") , ' +
								' \'Airline_ID\',Inspection_Area_Cycle.Airline_ID, ' +
								' \'Airlines\',Airline.Value, ' +
								' \'Geographical_Area\',Inspection_Area_Cycle.Geographical_Area, ' +
								' \'Flying_Cycle\',Inspection_Area_Cycle.Flying_Cycle, ' +
								' \'Flying_Hours\',Inspection_Area_Cycle.Flying_Hours, ' +
								' \'Inspection_Campaign\',Inspection_Area_Cycle.Inspection_Campaign, ' +
								' \'Campaign_Date\',DATE_FORMAT(Inspection_Area_Cycle.Campaign_Date, \"%Y-%m-%d %T\"), ' +
								' \'Equipment_Type_ID\',Inspection_Area_Cycle.Equipment_Type_ID, ' +
								' \'Type_of_Equipment\',Equipment_Type.Value, ' +
								' \'Name_of_Equipment\',Inspection_Area_Cycle.Name_of_Equipment, ' +
								' \'Equipment_Serial_Number\',Inspection_Area_Cycle.Equipment_Serial_Number, ' +
								' \'Calibration_Due_Date\',DATE_FORMAT(Inspection_Area_Cycle.Calibration_Due_Date, \"%Y-%m-%d %T\"), ' +
								' \'Calibration_Specimen\',Inspection_Area_Cycle.Calibration_Specimen, ' +
								' \'Calibration_Procedure\',Inspection_Area_Cycle.Calibration_Procedure, ' +
								' \'Inspection_Area_ID\',Inspection_Area_Cycle.Inspection_Area_ID ' +
								' \) as json_inspection_area_cycle, ' +
								' Inspection_Finding_ID, ' +
								' json_inspection_finding ' +
								' FROM E2E_Inspection_Area_Cycle Inspection_Area_Cycle ' +
								' LEFT JOIN ' +
								' ( ' +
								' SELECT  ' +
								' Inspection_Finding.Inspection_Area_Cycle_ID, ' +
								' Inspection_Finding.ID as Inspection_Finding_ID, ' +
								' JSON_OBJECT( ' +
								' \'Inspection_Finding_ID\',Inspection_Finding.ID, ' +
								' \'IP_Name\',Inspection_Finding.IP_Name, ' +
								' \'Finding_Date\',DATE_FORMAT(Inspection_Finding.Finding_Date, \"%Y-%m-%d %T\"), ' +
								' \'Flying_Cycle_Real\',Inspection_Finding.Flying_Cycle_Real, ' +
								' \'Finding_Category_ID\',Inspection_Finding.Finding_Category_ID, ' +
								' \'Finding_Category\',Finding_Category.Value, ' +
								' \'Number_Spot\',Inspection_Finding.Number_Spot, ' +
								' \'Length\',Inspection_Finding.Length, ' +
								' \'Width\',Inspection_Finding.Width, ' +
								' \'Surface\',Inspection_Finding.Surface, ' +
								' \'Orientation\',Inspection_Finding.Orientation, ' +
								' \'Angle_Reference\',Inspection_Finding.Angle_Reference, ' +
								' \'Depth\',Inspection_Finding.Depth, ' +
								' \'Starting_Depth\',Inspection_Finding.Starting_Depth, ' +
								' \'Ending_Depth\',Inspection_Finding.Ending_Depth, ' +
								' \'Layer\',Inspection_Finding.Layer, ' +
								' \'Remaining_Thickness\',Inspection_Finding.Remaining_Thickness, ' +
								' \'Rotation_Angle\',Inspection_Finding.Rotation_Angle, ' +
								' \'Comments\',Inspection_Finding.Comments, ' +
								' \'Repair_Type_ID\',Inspection_Finding.Repair_Type_ID, ' +
								' \'Repair_Type\',Repair_Type.Value, ' +
								' \'Repair_Reference\',Inspection_Finding.Repair_Reference, ' +
								' \'Part_Number\',Inspection_Finding.Part_Number, ' +
								' \'Part_Serial_Number\',Inspection_Finding.Part_Serial_Number, ' +
								' \'Environment_Category_ID\',Inspection_Finding.Environment_Category_ID, ' +
								' \'Environment_Category\',Environment_Category.Value, ' +
								' \'EASA_AMC_2020_Level_ID\',Inspection_Finding.EASA_AMC_2020_Level_ID, ' +
								' \'EASA_AMC_2020_Level\',EASA_AMC_2020_Level.Value, ' +
								' \'SSI_Reference\',Inspection_Finding.SSI_Reference, ' +
								' \'RMT_File\',Inspection_Finding.RMT_File, ' +
								' \'RDAS_Reference\',Inspection_Finding.RDAS_Reference, ' +
								' \'Corrosion_Material_1\',Inspection_Finding.Corrosion_Material_1, ' +
								' \'Corrosion_Material_2\',Inspection_Finding.Corrosion_Material_2, ' +
								' \'Corrosion_Fastener_Specification\',Inspection_Finding.Corrosion_Fastener_Specification, ' +
								' \'Part_Criticity_ID\',Inspection_Finding.Part_Criticity_ID, ' +
								' \'Part_Criticity\',Part_Criticity.Value, ' +
								' \'Finding_Type_ID\',Inspection_Finding.Finding_Type_ID, ' +
								' \'Finding_Type\',Finding_Type.Value, ' +
								' \'Inspection_Area_Cycle_ID\',Inspection_Finding.Inspection_Area_Cycle_ID ' +
								' \) as json_inspection_finding ' +
								' FROM E2E_Inspection_Finding Inspection_Finding ' +
								' LEFT JOIN E2E_Finding_Category Finding_Category ' +
								' ON Inspection_Finding.Finding_Category_ID = Finding_Category.ID ' +
								' LEFT JOIN E2E_Repair_Type Repair_Type ' +
								' ON Inspection_Finding.Repair_Type_ID = Repair_Type.ID ' +
								' LEFT JOIN E2E_Environment_Category Environment_Category ' +
								' ON Inspection_Finding.Environment_Category_ID = Environment_Category.ID ' +
								' LEFT JOIN E2E_EASA_AMC_2020_Level EASA_AMC_2020_Level ' +
								' ON Inspection_Finding.EASA_AMC_2020_Level_ID = EASA_AMC_2020_Level.ID ' +
								' LEFT JOIN E2E_Part_Criticity Part_Criticity ' +
								' ON Inspection_Finding.Part_Criticity_ID = Part_Criticity.ID ' +
								' LEFT JOIN E2E_Finding_Type Finding_Type ' +
								' ON Inspection_Finding.Finding_Type_ID = Finding_Type.ID ' +
								' WHERE Inspection_Finding.deleted_at is null ' +
								' ) Inspection_Finding ' +
								' ON Inspection_Area_Cycle.ID = Inspection_Finding.Inspection_Area_Cycle_ID ' +
								' LEFT JOIN E2E_Airline Airline ' +
								' ON Inspection_Area_Cycle.Airline_ID = Airline.ID ' +
								' LEFT JOIN E2E_Equipment_Type Equipment_Type ' +
								' ON Inspection_Area_Cycle.Equipment_Type_ID = Equipment_Type.ID ' +
								' WHERE Inspection_Area_Cycle.deleted_at is null  ' +
								' ) Inspection_Area_Cycle ' +
								' ON Inspection_Area.ID = Inspection_Area_Cycle.Inspection_Area_ID ' +
								' LEFT JOIN E2E_Area_Inspectability Area_Inspectability ' +
								' ON Inspection_Area.Area_Inspectability_ID = Area_Inspectability.ID ' +
								' LEFT JOIN E2E_Side Side ' +
								' ON Inspection_Area.Side_ID = Side.ID ' +
								' LEFT JOIN E2E_Assembly_Condition Assembly_Condition ' +
								' ON Inspection_Area.Assembly_Condition_ID = Assembly_Condition.ID ' +
								' LEFT JOIN E2E_Inspection_Type Inspection_Type ' +
								' ON Inspection_Area.Inspection_Type_ID = Inspection_Type.ID ' +
								' LEFT JOIN E2E_Inspection_Method Inspection_Method ' +
								' ON Inspection_Area.Inspection_Method_ID = Inspection_Method.ID ' +
								' LEFT JOIN ' +
								' ( ' +
								' SELECT Inspection_Area__Finding_Type.ID as Inspection_Area__Finding_Type_ID,Inspection_Area__Finding_Type.Inspection_Area_ID, Inspection_Area__Finding_Type.Finding_Type_ID, Finding_Type.Value as Finding_Type FROM E2E_Inspection_Area__Finding_Type Inspection_Area__Finding_Type LEFT JOIN E2E_Finding_Type Finding_Type ' +
								' on Inspection_Area__Finding_Type.Finding_Type_ID=Finding_Type.ID WHERE Inspection_Area__Finding_Type.deleted_at is null and Finding_Type.deleted_at is null ' +
								' ) Inspection_Area__Finding_Type ' +
								' ON Inspection_Area.ID = Inspection_Area__Finding_Type.Inspection_Area_ID ' +
								' WHERE Inspection_Area.deleted_at is null ' +
								' GROUP BY Inspection_Area.ID,Inspection_Area_Cycle.Inspection_Finding_ID ' +
								' ) Inspection_Area ' +
								' ON Inspection_Request.ID = Inspection_Area.Inspection_Request_ID ' +
								' LEFT JOIN E2E_NDT_Story NDT_Story ' +
								' ON Inspection_Request.NDT_Story_ID = NDT_Story.ID ' +
								' WHERE Inspection_Request.deleted_at is null ' +
								' AND Inspection_Request.ID in (@replace) ' +
								' ORDER BY Inspection_Request_ID DESC, Inspection_Area_ID ASC, Inspection_Area_Cycle_ID ASC, Inspection_Finding_ID ASC ' +
								' ;	';
							//update the sql statement with the inspection request id	
							sql_query_retrieve_full_inspection_request = sql_query_retrieve_full_inspection_request.replace('@replace', ids_array);

							db.sequelize.query(
								sql_query_retrieve_full_inspection_request,
								{
									type: db.sequelize.QueryTypes.SELECT,
									useMaster: false
								})
								.then((ress) => {
									//console.log(ress);
									var final_result = [];

									var current_inspection_request_id = 0;
									var current_inspection_area_id = 0;
									var current_inspection_area_cycle_id = 0;
									var current_inspectionrequest_obj = null;
									var current_inspectionarea_obj = null;
									var current_inspectionareacycle_obj = null;

									try {
										ress.map(inspectionrequestobj => {
											// console.log('next element');	
											// 		console.log(inspectionrequestobj);					
											if (inspectionrequestobj.Inspection_Request_ID != null) {
												if (inspectionrequestobj.Inspection_Request_ID != current_inspection_request_id) {
													if (current_inspectionrequest_obj != null && current_inspection_request_id > 0) {
														//add it into the final_result array

														if (current_inspectionarea_obj != null) {
															var inspectionarea_array = [];
															var inspectionareacycle_array = [];

															if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null) {
																inspectionareacycle_array = current_inspectionarea_obj.current_inspectionarea_obj;
															}//end if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null)

															inspectionareacycle_array.push(current_inspectionareacycle_obj);
															current_inspectionarea_obj.NDT_Inspection_Area_Cycle = inspectionareacycle_array;

															if (current_inspectionrequest_obj.NDT_Inspection_Area != null) {
																inspectionarea_array = current_inspectionrequest_obj.NDT_Inspection_Area;
															}//end if (current_inspectionrequest_obj.NDT_Inspection_Area != null)

															inspectionarea_array.push(current_inspectionarea_obj);
															current_inspectionrequest_obj.NDT_Inspection_Area = inspectionarea_array;

														}

														//delete the Inspection_Area_ID,json_inspection_area,Inspection_Area_Cycle_ID,json_inspection_area_cycle,Inspection_Finding_ID,json_inspection_finding
														delete current_inspectionrequest_obj.Inspection_Area_ID;
														delete current_inspectionrequest_obj.json_inspection_area;
														delete current_inspectionrequest_obj.Inspection_Area_Cycle_ID;
														delete current_inspectionrequest_obj.json_inspection_area_cycle;
														delete current_inspectionrequest_obj.Inspection_Finding_ID;
														delete current_inspectionrequest_obj.json_inspection_finding;

														final_result.push(current_inspectionrequest_obj);
													}
													//update the following variables to the current inspectionrequestobj
													current_inspectionrequest_obj = inspectionrequestobj;
													current_inspection_request_id = inspectionrequestobj.Inspection_Request_ID;

													//reset current inspectionarea variables
													current_inspectionarea_obj = null;
													current_inspection_area_id = 0;

													//reset current inspectionareacycle variables
													current_inspectionareacycle_obj = null;
													current_inspection_area_cycle_id = 0;
												}
											}

											if (inspectionrequestobj.Inspection_Area_ID != null) {
												//console.log(current_inspectionarea_obj);	
												if (inspectionrequestobj.Inspection_Area_ID != current_inspection_area_id) {
													//condition to determine if we are processing the next inspectionareaobj
													if (current_inspectionarea_obj != null && current_inspection_area_id > 0) {

														if (current_inspectionareacycle_obj != null) {
															//check whether the current_inspectionrequest_obj got NDT_Inspection_Area
															var inspectionarea_array = [];
															var inspectionareacycle_array = [];

															if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null) {
																inspectionareacycle_array = current_inspectionarea_obj.current_inspectionarea_obj;
															}//end if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null)

															inspectionareacycle_array.push(current_inspectionareacycle_obj);
															current_inspectionarea_obj.NDT_Inspection_Area_Cycle = inspectionareacycle_array;

															if (current_inspectionrequest_obj.NDT_Inspection_Area != null) {
																inspectionarea_array = current_inspectionrequest_obj.NDT_Inspection_Area;
															}//end if (current_inspectionrequest_obj.NDT_Inspection_Area != null)	

															inspectionarea_array.push(current_inspectionarea_obj);
															current_inspectionrequest_obj.NDT_Inspection_Area = inspectionarea_array;
														}

													}//end if (current_inspectionarea_obj != null && current_inspection_area_id > 0)

													//update the following variables to the current inspectionrequestobj
													//convert the Expected_Finding_Type to a JSON format
													var json_inspection_area_obj = inspectionrequestobj.json_inspection_area;
													json_inspection_area_obj.Expected_Finding_Type = JSON.parse(json_inspection_area_obj.Expected_Finding_Type);

													current_inspectionarea_obj = json_inspection_area_obj;
													current_inspection_area_id = inspectionrequestobj.Inspection_Area_ID;

													//reset current inspectionareacycle variables
													current_inspectionareacycle_obj = null;
													current_inspection_area_cycle_id = 0;
												}//end if (inspectionrequestobj.Inspection_Area_ID != current_inspection_area_id)
											}

											if (inspectionrequestobj.Inspection_Area_Cycle_ID != null) {
												if (inspectionrequestobj.Inspection_Area_Cycle_ID != current_inspection_area_cycle_id) {
													//condition to determine if we are processing the next inspectionareacycleobj
													if (current_inspectionareacycle_obj != null && current_inspection_area_cycle_id > 0) {
														//check whether the current_inspectionarea_obj got NDT_Inspection_Area_Cycle
														var inspectionareacycle_array = [];

														if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null) {
															inspectionareacycle_array = current_inspectionarea_obj.NDT_Inspection_Area_Cycle;
														}//end if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null)

														inspectionareacycle_array.push(current_inspectionareacycle_obj);
														current_inspectionarea_obj.NDT_Inspection_Area_Cycle = inspectionareacycle_array;
													}//end if (current_inspectionareacycle_obj != null && current_inspection_area_cycle_id > 0)

													//update the following variables to the current inspectionareacycleobj
													current_inspectionareacycle_obj = inspectionrequestobj.json_inspection_area_cycle;
													current_inspection_area_cycle_id = inspectionrequestobj.Inspection_Area_Cycle_ID;
												}//end if (inspectionrequestobj.Inspection_Area_Cycle_ID != current_inspection_area_cycle_id)

											}//end if (inspectionrequestobj.Inspection_Area_Cycle_ID != null)

											if (inspectionrequestobj.Inspection_Finding_ID != null) {
												var inspectionfinding_array = [];

												if (current_inspectionareacycle_obj.NDT_Inspection_Finding != null) {
													inspectionfinding_array = current_inspectionareacycle_obj.NDT_Inspection_Finding;
												}
												//update the following variables to the current inspectionareacycleobj
												inspectionfinding_array.push(inspectionrequestobj.json_inspection_finding);
												current_inspectionareacycle_obj.NDT_Inspection_Finding = inspectionfinding_array;
											}//end if (inspectionrequestobj.Inspection_Finding_ID != null)

										});//end ress.map(inspectionrequestobj =>

										if (current_inspectionrequest_obj != null && current_inspection_request_id > 0) {
											//add it into the final_result array

											if (current_inspectionarea_obj != null) {
												var inspectionarea_array = [];
												var inspectionareacycle_array = [];

												if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null) {
													inspectionareacycle_array = current_inspectionarea_obj.current_inspectionarea_obj;
												}//end if (current_inspectionarea_obj.NDT_Inspection_Area_Cycle != null)

												inspectionareacycle_array.push(current_inspectionareacycle_obj);
												current_inspectionarea_obj.NDT_Inspection_Area_Cycle = inspectionareacycle_array;

												if (current_inspectionrequest_obj.NDT_Inspection_Area != null) {
													inspectionarea_array = current_inspectionrequest_obj.NDT_Inspection_Area;
												}//end if (current_inspectionrequest_obj.NDT_Inspection_Area != null)

												inspectionarea_array.push(current_inspectionarea_obj);
												current_inspectionrequest_obj.NDT_Inspection_Area = inspectionarea_array;

											}

											//delete the Inspection_Area_ID,json_inspection_area,Inspection_Area_Cycle_ID,json_inspection_area_cycle,Inspection_Finding_ID,json_inspection_finding
											delete current_inspectionrequest_obj.Inspection_Area_ID;
											delete current_inspectionrequest_obj.json_inspection_area;
											delete current_inspectionrequest_obj.Inspection_Area_Cycle_ID;
											delete current_inspectionrequest_obj.json_inspection_area_cycle;
											delete current_inspectionrequest_obj.Inspection_Finding_ID;
											delete current_inspectionrequest_obj.json_inspection_finding;

											final_result.push(current_inspectionrequest_obj);
										}
										resolve(final_result);

									} catch (error_ress) {
										console.log(error_ress);
									}

									resolve(final_result);
								}).catch((errors) => {
									console.log(errors);
									console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): returning error: ' + errors);
									reject(errors);
								});
						} else {
							resolve({ 'message': 'No record is found.' });
						}

					}
				})
				.catch((error) => {
					console.log(error);
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: get(param,search,transact, transactionid): returning error: ' + error);
					reject(error);
				});

		});
	}//end get

	/**
	 * List all Inspection Request from database
	 *
	 * @returns {Array}
	 */
	list(transact, transactionid) {
		return new Promise((resolve, reject) => {

			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: list(transact, transactionid)');

			//do not pass in the transact to sequelize for it to use the READ pool, useMaster will force sequelize to use the read pool
			db.E2E_Inspection_Request
				.findAll({
					attributes: [['ID', 'Inspection_Request_ID'], ['Program', 'Program'], ['Aircraft_Type', 'Aircraft_Type'], ['Applicable_MSN_Range', 'Applicable_MSN_Range'],
					['Priority', 'Priority'], ['Stress_Leader', 'Stress_Leader'], ['NDI_compiler', 'NDI_compiler'], ['Test_Leader', 'Test_Leader'], ['Requestor', 'Requestor'], ['NDT_Story_ID', 'NDT_Story_ID']],
					useMaster: false
				}
				)
				.then((res) => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: list(transact, transactionid): returning result');
					resolve(res);
				})
				.catch((error) => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: list(transact, transactionid): returning error: ' + error);
					reject(error);
				});
		});
	}//end list

    /**
     * Set the deleted_at with a datetime to indicate the records have been removed
     * @param {Integer} id
     * @param {sequelize.transaction} transact 
     * @param {String} transactionid 
     */
	remove(id, transact, transactionid) {
		return new Promise((resolve, reject) => {
			console.log('(' + transactionid + '): entered app/controllers/api/e2e/inspectionrequest/lib/index.js: remove(id, transact, transactionid)');
			console.log('(' + transactionid + '): parameter app/controllers/api/e2e/inspectionrequest/lib/index.js: remove(id,transact, transactionid): ' + id);

			//convert id to array for inspection request
			var arrayIR_id = id.split(',');
			db.E2E_Inspection_Request
				.destroy({
					transaction: transact,
					useMaster: true,
					where: {
						ID: arrayIR_id
					}
				})
			//remove inspection request id from inspection area using arrayIR_id
			db.E2E_Inspection_Area
				.destroy({
					transaction: transact,
					useMaster: true,
					where: {
						Inspection_Request_ID: arrayIR_id,
					}
				})
				// split id of inspection area and remove from inspection area cycle
				// var arrayIA_id = Inspection_Area_ID.split(',');
				// db.E2E_Inspection_Area_Cycle
				// 	.destroy({
				// 		transaction: transact,
				// 		useMaster: true,
				// 		where: {
				// 			Inspection_Area_ID: arrayIA_id
				// 		}
				// 	})
				// remove from inspection area finding type
				// db.E2E_Inspection_Area_Cycle
				// 	.destroy({
				// 		transaction: transact,
				// 		useMaster: true,
				// 		where: {
				// 			Inspection_Area_ID: arrayIA_id
				// 		}
				// 	})
				// split id of inspection area cycle and remove from inspection finding
				// var arrayIAC_id = Inspection_Area__Cycle_ID.split(',');
				// db.E2E_Inspection_Finding
				// 	.destroy({
				// 		transaction: transact,
				// 		useMaster: true,
				// 		where: {
				// 			Inspection_Area_Cycle_ID: arrayIAC_id
				// 		}
				// 	})

				.then((res) => {
					console.log('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: remove(id, transact, transactionid): returning result');
					resolve(res);
				})
				.catch((error) => {
					console.error('(' + transactionid + '): app/controllers/api/e2e/inspectionrequest/lib/index.js: remove(id, transact, transactionid): returning error: ' + error);
					reject(error);
				});
		});
	}

	/**
     * Check if any of the madatory fields are empty. If yes, return 1, otherwise 0
     * @param {JSON} inspectionrequest 
     * @param {String} transactionid 
     */
	checkEmptyFields(inspectionrequest) {
		return new Promise((resolve, reject) => {
			var anyemptyfields = false;

			inspectionrequest.map(a => {
				// if (a.Program == null){
				//     reject({'message':'Program is mandatory field'});
				// }
				// if (a.Aircraft_Type == null){
				//     reject({'message':'Aircraft_Type is mandatory field'});
				// }
				// if (a.Applicable_MSN_Range == null){
				//     reject({'message':'Applicable_MSN_Range is mandatory field'});
				// }
				if (a.NDT_Story_ID == null) {
					reject({ 'message': 'NDT_Story_ID is mandatory field' });
				}
			});

			resolve(0);
		});
	}


	retrieveReferenceValues(inspectionrequest) {
		return new Promise((resolve, reject) => {
			var self = this;
			var error_detected = false;//track if any of the promises contain message which indicates an error
			const new_Result = Promise.all(inspectionrequest.map((inspectionrequestobj) => {

				return Promise.resolve()
					.then(() => {
						return self.performInspectionRequestLookup(inspectionrequestobj).then(function (result_inspectionrequest) {
							inspectionrequestobj = result_inspectionrequest;

							//check if company contains a model
							if (inspectionrequestobj.NDT_Inspection_Area != null) {

								return Promise.resolve()
									.then(() => {

										/* 
										Get candidates from current subject, or just an empty array if no valid candidates data present 
										*/
										const inspectionarea_array = Array.isArray(inspectionrequestobj.NDT_Inspection_Area) && inspectionrequestobj.NDT_Inspection_Area.length > 0 ? inspectionrequestobj.NDT_Inspection_Area : [];

										/*
										Map candidates (if any) to this Promise.all() 
										*/
										return Promise.all(inspectionarea_array.map((inspectionarea) => {
											return self.performInspectionAreaLookup(inspectionarea).then(function (result_inspectionarea) {
												/*
												Assign the score returned from getTotalMarksPerCandidate() for current candidate object
												*/
												inspectionarea = result_inspectionarea;

												if (inspectionarea.message != null) {
													error_detected = true;
												}

												if (inspectionarea.NDT_Inspection_Area_Cycle != null) {

													return Promise.resolve()
														.then(() => {

															/* 
															Get candidates from current subject, or just an empty array if no valid candidates data present 
															*/
															const inspectionareacycle_array = Array.isArray(inspectionarea.NDT_Inspection_Area_Cycle) && inspectionarea.NDT_Inspection_Area_Cycle.length > 0 ? inspectionarea.NDT_Inspection_Area_Cycle : [];

															/*
															Map candidates (if any) to this Promise.all() 
															*/
															return Promise.all(inspectionareacycle_array.map((inspectionareacycle) => {
																return self.performInspectionAreaCycleLookup(inspectionareacycle).then(function (result_inspectionareacycle) {
																	/*
																	Assign the score returned from getTotalMarksPerCandidate() for current candidate object
																	*/
																	inspectionareacycle = result_inspectionareacycle;

																	if (inspectionareacycle.message != null) {
																		error_detected = true;
																	}

																	if (inspectionareacycle.NDT_Inspection_Finding != null) {

																		return Promise.resolve()
																			.then(() => {

																				/* 
																				Get candidates from current subject, or just an empty array if no valid candidates data present 
																				*/
																				const inspectionfinding_array = Array.isArray(inspectionareacycle.NDT_Inspection_Finding) && inspectionareacycle.NDT_Inspection_Finding.length > 0 ? inspectionareacycle.NDT_Inspection_Finding : [];

																				/*
																				Map candidates (if any) to this Promise.all() 
																				*/
																				return Promise.all(inspectionfinding_array.map((inspectionfinding) => {
																					return self.performInspectionFindingLookup(inspectionfinding).then(function (result_inspectionfinding) {
																						/*
																						Assign the score returned from getTotalMarksPerCandidate() for current candidate object
																						*/
																						inspectionfinding = result_inspectionfinding;
																						if (inspectionfinding.message != null) {
																							error_detected = true;
																						}

																						return inspectionfinding;

																					})
																				}));
																			})
																			.then((result_inspectionfinding_array) => {

																				//console.log('Check this out', result);
																				inspectionareacycle.NDT_Inspection_Finding = result_inspectionfinding_array;

																				return inspectionareacycle;
																			}).catch((error_inspectionfinding_array) => {

																				reject(error_inspectionfinding_array);
																			});

																	} else {
																		return inspectionareacycle;
																	}

																})
															}));
														})
														.then((result_inspectionareacycle_array) => {

															//console.log('Check this out', result);
															inspectionarea.NDT_Inspection_Area_Cycle = result_inspectionareacycle_array;
															return inspectionarea;
														}).catch((error_inspectionareacycle_array) => {

															reject(error_inspectionareacycle_array);
														});

												} else {
													return inspectionarea;
												}


											})
										}));
									})
									.then((result_inspectionarea_array) => {

										//console.log('Check this out', result);
										inspectionrequestobj.NDT_Inspection_Area = result_inspectionarea_array;
										return inspectionrequestobj;

									}).catch((error_inspectionarea_array) => {

										reject(error_inspectionarea_array);
									});
							} else {
								return inspectionrequestobj;
							}
						})
					})

				//for every array that requires lookup, use this template for the then template
				/*
				return Promise.resolve()
				.then(() => {
			
					const registration = Array.isArray(mod.Registration) && mod.Registration.length > 0 ? mod.Registration : [];
	
					return Promise.all(registration.map((rego) => {
						return self.performRegoLookup(rego).then(function(ress) {
							
							rego = ress;
							
							return rego;
							
						})
					}));
				})
				.then((result_rego) => {
			
					mod.Registration = result_rego;
					return mod;
				});
				*/

			})).then((result_inspectionrequest_array) => {
				if (error_detected == false) {
					resolve(result_inspectionrequest_array);
				} else {
					reject(result_inspectionrequest_array);
				}

			}).catch((error_inspectionrequest_array) => {

				reject(error_inspectionrequest_array);
			});//end const new_Result

		});

	}//end retrieveReferenceValues

	/**
	 * Select all the reference required for Inspection Request
	 * - NDT_Story
	 * @param {*} inspectionrequest 
	 */
	performInspectionRequestLookup(inspectionrequest) {
		console.log('enter performInspectionRequestLookup');
		return new Promise((resolve, reject) => {
			try {
				var atLeastOneLookupRequired = false;
				//formulare the SQL query
				var sql_query = ' SELECT ' +
					' @NDT_Story ;';

				var replace_query_id = ' (SELECT ID from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename_ID ';
				var replace_query_value = ' (SELECT Value from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename ';

				if (inspectionrequest.NDT_Story != null) {
					if (inspectionrequest.NDT_Story.trim() != '') {
						var value = inspectionrequest.NDT_Story;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'NDT_Story';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@NDT_Story', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@NDT_Story', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@NDT_Story', '');
					}
				} else {
					sql_query = sql_query.replace('@NDT_Story', '');
				}

				if (atLeastOneLookupRequired == true) {
					var promises_inner = [];
					//prepare a promise
					var prom = new Promise((resolved, rejected) => {
						db.sequelize.query(
							sql_query,
							{
								type: db.sequelize.QueryTypes.SELECT,
								useMaster: false
							})
							.then((res) => {
								if (res.length > 0) {
									delete inspectionrequest.NDT_Story;
									inspectionrequest.NDT_Story_ID = res[0].NDT_Story_ID;
									inspectionrequest.NDT_Story = res[0].NDT_Story;
									resolved(inspectionrequest);
								} else {
									inspectionrequest.message = 'NDT_Story value cannot be found in the database.';
									resolved(inspectionrequest);
								}
							})
							.catch((error) => {
								console.log(error);
								rejected(error);
							});
					});
					promises_inner.push(prom);

					//execute all the promise
					if (promises_inner.length > 0) {
						//console.log('promises_inner.length: '+promises_inner.length);
						return Promise.all(promises_inner)
							.then(function (ress) {
								resolve(ress[0]);
							}).catch((errors) => {
								reject(errors);
							}); //end return Promise.all(promises_inner)
					} else {
						resolve(inspectionrequest);
					}
				} else {
					resolve(inspectionrequest);
				}

			} catch (error) {
				console.error(error);
				resolve(error);
			}

		});

	}//end performInspectionRequestLookup

	/**
	 * Select all the reference required for Inspection Area
	 * - Area_Inspectability
	 * - Side
	 * - Inspection_Type 
	 * - Assembly_Condition
	 * - Inspection_Method
	 * - Finding_Type (multi-valued) (Expected_finding_type)
	 * @param {*} inspectionarea 
	 */
	performInspectionAreaLookup(inspectionarea) {
		console.log('enter performInspectionAreaLookup');
		return new Promise((resolve, reject) => {
			try {
				var atLeastOneLookupRequired = false;
				//formulare the SQL query
				var sql_query = ' SELECT ' +
					' @Area_Inspectability @Side @Inspection_Type @Assembly_Condition @Inspection_Method @Finding_Type ;';

				var replace_query_id = ' (SELECT ID from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename_ID ';
				var replace_query_value = ' (SELECT Value from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename ';

				var replace_query_findingtype = '(SELECT GROUP_CONCAT(CONCAT(\'{\"Finding_Type_ID\":\', ID, \', \"Finding_Type\":\"\',Value,\'\"}\'))) Finding_Type FROM E2E_Finding_Type where deleted_at is null and lcase(Value) in (@replace) ;';


				if (inspectionarea.Area_Inspectability != null) {
					if (inspectionarea.Area_Inspectability.trim() != '') {
						var value = inspectionarea.Area_Inspectability;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Area_Inspectability';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Area_Inspectability', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Area_Inspectability', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Area_Inspectability', '');
					}


				} else {
					sql_query = sql_query.replace('@Area_Inspectability', '');
				}

				if (inspectionarea.Side != null) {
					if (inspectionarea.Side.trim() != '') {
						var value = inspectionarea.Side;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Side';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Side', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Side', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Side', '');
					}


				} else {
					sql_query = sql_query.replace('@Side', '');
				}

				if (inspectionarea.Inspection_Type != null) {
					if (inspectionarea.Inspection_Type.trim() != '') {
						var value = inspectionarea.Inspection_Type;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Inspection_Type';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Inspection_Type', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Inspection_Type', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Inspection_Type', '');
					}


				} else {
					sql_query = sql_query.replace('@Inspection_Type', '');
				}

				if (inspectionarea.Assembly_Condition != null) {
					if (inspectionarea.Assembly_Condition.trim() != '') {
						var value = inspectionarea.Assembly_Condition;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Assembly_Condition';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Assembly_Condition', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Assembly_Condition', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Assembly_Condition', '');
					}


				} else {
					sql_query = sql_query.replace('@Assembly_Condition', '');
				}

				if (inspectionarea.Inspection_Method != null) {
					if (inspectionarea.Inspection_Method.trim() != '') {
						var value = inspectionarea.Inspection_Method;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Inspection_Method';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Inspection_Method', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Inspection_Method', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Inspection_Method', '');
					}


				} else {
					sql_query = sql_query.replace('@Inspection_Method', '');
				}

				if (inspectionarea.Expected_Finding_Type != null) {//this parameter is a multi-valued
					if (inspectionarea.Expected_Finding_Type.length > 0) {
						var value = inspectionarea.Expected_Finding_Type;
						//convert array into a string
						value = value.toString();

						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						replace_query_findingtype = replace_query_findingtype.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Finding_Type', replace_query_findingtype);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Finding_Type', ', ' + replace_query_findingtype);
						}//end if (atLeastOneLookupRequired == false)
					} else {
						sql_query = sql_query.replace('@Finding_Type', '');
					}

				} else {
					sql_query = sql_query.replace('@Finding_Type', '');
				}

				if (atLeastOneLookupRequired == true) {
					var promises_inner = [];
					//prepare a promise
					var prom = new Promise((resolved, rejected) => {
						db.sequelize.query(
							sql_query,
							{
								type: db.sequelize.QueryTypes.SELECT,
								useMaster: false
							})
							.then((res) => {
								if (res.length > 0) {
									var error_message = '';

									if (inspectionarea.Area_Inspectability != null) {
										if (inspectionarea.Area_Inspectability.trim() != '') {
											if (res[0].Area_Inspectability_ID != null) {
												delete inspectionarea.Area_Inspectability;
												inspectionarea.Area_Inspectability_ID = res[0].Area_Inspectability_ID;
												inspectionarea.Area_Inspectability = res[0].Area_Inspectability;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Area_Inspectability';
												} else {
													error_message = 'Area_Inspectability';
												}

											}
										}

									}

									if (inspectionarea.Side != null) {
										if (inspectionarea.Side.trim() != '') {
											if (res[0].Side_ID != null) {
												delete inspectionarea.Side;
												inspectionarea.Side_ID = res[0].Side_ID;
												inspectionarea.Side = res[0].Side;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Side';
												} else {
													error_message = 'Side';
												}

											}
										}

									}

									if (inspectionarea.Inspection_Type != null) {
										if (inspectionarea.Inspection_Type.trim() != '') {
											if (res[0].Inspection_Type_ID != null) {
												delete inspectionarea.Inspection_Type;
												inspectionarea.Inspection_Type_ID = res[0].Inspection_Type_ID;
												inspectionarea.Inspection_Type = res[0].Inspection_Type;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Inspection_Type';
												} else {
													error_message = 'Inspection_Type';
												}

											}
										}

									}

									if (inspectionarea.Assembly_Condition != null) {
										if (inspectionarea.Assembly_Condition.trim() != '') {
											if (res[0].Assembly_Condition_ID != null) {
												delete inspectionarea.Assembly_Condition;
												inspectionarea.Assembly_Condition_ID = res[0].Assembly_Condition_ID;
												inspectionarea.Assembly_Condition = res[0].Assembly_Condition;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Assembly_Condition';
												} else {
													error_message = 'Assembly_Condition';
												}

											}
										}

									}

									if (inspectionarea.Inspection_Method != null) {
										if (inspectionarea.Inspection_Method.trim() != '') {
											if (res[0].Inspection_Method_ID != null) {
												delete inspectionarea.Inspection_Method;
												inspectionarea.Inspection_Method_ID = res[0].Inspection_Method_ID;
												inspectionarea.Inspection_Method = res[0].Inspection_Method;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Inspection_Method';
												} else {
													error_message = 'Inspection_Method';
												}

											}
										}

									}

									if (inspectionarea.Expected_Finding_Type != null) {
										if (inspectionarea.Expected_Finding_Type.length > 0) {
											if (res[0].Finding_Type != null) {

												inspectionarea.Expected_Finding_Type = JSON.parse('[' + res[0].Finding_Type + ']');
											} else {
												if (error_message != '') {
													error_message = error_message + ', Expected_Finding_Type';
												} else {
													error_message = 'Expected_Finding_Type';
												}

											}
										}

									}

									if (error_message.length <= 0) {//no error is found
										resolved(inspectionarea);
									} else {//error is found
										inspectionarea.message = error_message + ' value(s) cannot be found in the database.';
										resolved(inspectionarea);
									}
								} else {
									inspectionarea.message = 'Referenced values (Area_Inspectability, Side, Inspection_Type, Assembly_Condition, Inspection_Method, Expected_Finding_Type) cannot be found in the database.';
									resolved(inspectionarea);
								}
							})
							.catch((error) => {
								console.log(error);
								rejected(error);
							});
					});
					promises_inner.push(prom);

					//execute all the promise
					if (promises_inner.length > 0) {
						//console.log('promises_inner.length: '+promises_inner.length);
						return Promise.all(promises_inner)
							.then(function (ress) {
								resolve(ress[0]);
							}).catch((errors) => {
								reject(errors);
							}); //end return Promise.all(promises_inner)
					} else {
						resolve(inspectionarea);
					}
				} else {
					resolve(inspectionarea);
				}


			} catch (error) {
				console.error(error);
				reject(error);
			}

		});

	}//end performInspectionAreaLookup


	/**
	 * Select all the reference required for Inspection Area Cycle
	 * - Equipment_Type (Type_of_Equipment)
	 * - Airline (Airlines)
	 * @param {*} inspectionarea 
	 */
	performInspectionAreaCycleLookup(inspectionareacycle) {
		console.log('enter performInspectionAreaCycleLookup');
		return new Promise((resolve, reject) => {
			try {
				var atLeastOneLookupRequired = false;
				//formulare the SQL query
				var sql_query = ' SELECT ' +
					' @Equipment_Type @Airline ;';

				var replace_query_id = ' (SELECT ID from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename_ID ';
				var replace_query_value = ' (SELECT Value from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename ';

				if (inspectionareacycle.Type_of_Equipment != null) {
					if (inspectionareacycle.Type_of_Equipment.trim() != '') {
						var value = inspectionareacycle.Type_of_Equipment;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Equipment_Type';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Equipment_Type', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Equipment_Type', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Equipment_Type', '');
					}


				} else {
					sql_query = sql_query.replace('@Equipment_Type', '');
				}

				if (inspectionareacycle.Airlines != null) {
					if (inspectionareacycle.Airlines.trim() != '') {
						var value = inspectionareacycle.Airlines;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Airline';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Airline', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Airline', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Airline', '');
					}

				} else {
					sql_query = sql_query.replace('@Airline', '');
				}

				if (atLeastOneLookupRequired == true) {
					var promises_inner = [];
					//prepare a promise
					var prom = new Promise((resolved, rejected) => {
						db.sequelize.query(
							sql_query,
							{
								type: db.sequelize.QueryTypes.SELECT,
								useMaster: false
							})
							.then((res) => {
								if (res.length > 0) {
									var error_message = '';

									if (inspectionareacycle.Type_of_Equipment != null) {
										if (inspectionareacycle.Type_of_Equipment.trim() != '') {
											if (res[0].Equipment_Type_ID != null) {
												delete inspectionareacycle.Type_of_Equipment;
												inspectionareacycle.Equipment_Type_ID = res[0].Equipment_Type_ID;
												inspectionareacycle.Type_of_Equipment = res[0].Equipment_Type;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Type_of_Equipment';
												} else {
													error_message = 'Type_of_Equipment';
												}

											}
										}

									}

									if (inspectionareacycle.Airlines != null) {
										if (inspectionareacycle.Airlines.trim() != '') {
											if (res[0].Airline_ID != null) {
												delete inspectionareacycle.Airlines;
												inspectionareacycle.Airline_ID = res[0].Airline_ID;
												inspectionareacycle.Airlines = res[0].Airline;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Airlines';
												} else {
													error_message = 'Airlines';
												}

											}
										}
									}

									if (error_message.length <= 0) {//no error is found
										resolved(inspectionareacycle);
									} else {//error is found
										inspectionareacycle.message = error_message + ' value(s) cannot be found in the database.';
										resolved(inspectionareacycle);
									}
								} else {
									inspectionareacycle.message = 'Referenced values (Area_Inspectability, Side, Inspection_Type, Assembly_Condition, Inspection_Method, Expected_Finding_Type) cannot be found in the database.';
									resolved(inspectionareacycle);
								}
							})
							.catch((error) => {
								console.log(error);
								rejected(error);
							});
					});
					promises_inner.push(prom);

					//execute all the promise
					if (promises_inner.length > 0) {
						//console.log('promises_inner.length: '+promises_inner.length);
						return Promise.all(promises_inner)
							.then(function (ress) {
								resolve(ress[0]);
							}).catch((errors) => {
								reject(errors);
							}); //end return Promise.all(promises_inner)
					} else {
						resolve(inspectionareacycle);
					}
				} else {
					resolve(inspectionareacycle);
				}

			} catch (error) {
				console.error(error);
				resolve(error);
			}

		});

	}//end performInspectionAreaCycleLookup

	/**
	 * Select all the reference required for Inspection Finding
	 * - Finding_Type
	 * - Part_Criticity
	 * - Repair_Type
	 * - Finding_Category
	 * - Environment_Category
	 * - EASA_AMC_2020_Level
	 * @param {*} inspectionfinding 
	 */
	performInspectionFindingLookup(inspectionfinding) {
		console.log('enter performInspectionFindingLookup');
		return new Promise((resolve, reject) => {
			try {
				var atLeastOneLookupRequired = false;
				//formulare the SQL query
				var sql_query = ' SELECT ' +
					' @Finding_Type @Part_Criticity @Repair_Type @Finding_Category @Environment_Category @EASA_AMC_2020_Level ;';

				var replace_query_id = ' (SELECT ID from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename_ID ';
				var replace_query_value = ' (SELECT Value from E2E_@tablename WHERE deleted_at is null AND lcase(Value) in (@replace)) as @tablename ';

				if (inspectionfinding.Finding_Type != null) {
					if (inspectionfinding.Finding_Type.trim() != '') {
						var value = inspectionfinding.Finding_Type;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Finding_Type';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Finding_Type', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Finding_Type', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Finding_Type', '');
					}


				} else {
					sql_query = sql_query.replace('@Finding_Type', '');
				}

				if (inspectionfinding.Part_Criticity != null) {
					if (inspectionfinding.Part_Criticity.trim() != '') {
						var value = inspectionfinding.Part_Criticity;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Part_Criticity';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Part_Criticity', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Part_Criticity', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Part_Criticity', '');
					}


				} else {
					sql_query = sql_query.replace('@Part_Criticity', '');
				}

				if (inspectionfinding.Repair_Type != null) {
					if (inspectionfinding.Repair_Type.trim() != '') {
						var value = inspectionfinding.Repair_Type;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Repair_Type';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Repair_Type', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Repair_Type', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Repair_Type', '');
					}


				} else {
					sql_query = sql_query.replace('@Repair_Type', '');
				}

				if (inspectionfinding.Finding_Category != null) {
					if (inspectionfinding.Finding_Category.trim() != '') {
						var value = inspectionfinding.Finding_Category;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Finding_Category';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Finding_Category', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Finding_Category', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Finding_Category', '');
					}


				} else {
					sql_query = sql_query.replace('@Finding_Category', '');
				}

				if (inspectionfinding.Environment_Category != null) {
					if (inspectionfinding.Environment_Category.trim() != '') {
						var value = inspectionfinding.Environment_Category;
						if (value.split(',').length > 1) {
							value = '\'' + value.replace(/,/g, "\',\'") + '\'';
						} else {
							value = '\'' + value + '\'';
						}

						var tablename = 'Environment_Category';

						var replace_query_id_clone = replace_query_id.replace('@replace', value.toLowerCase());
						var replace_query_value_clone = replace_query_value.replace('@replace', value.toLowerCase());

						if (atLeastOneLookupRequired == false) {
							sql_query = sql_query.replace('@Environment_Category', replace_query_id_clone + ', ' + replace_query_value_clone);
							atLeastOneLookupRequired = true;
						} else {
							sql_query = sql_query.replace('@Environment_Category', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
						}//end if (atLeastOneLookupRequired == false)

						//replace @tablename with the tablename
						sql_query = sql_query.replace(/@tablename/g, tablename);
					} else {
						sql_query = sql_query.replace('@Environment_Category', '');
					}


				} else {
					sql_query = sql_query.replace('@Environment_Category', '');
				}

				if (inspectionfinding.EASA_AMC_2020_Level != null) {

					var tablename = 'EASA_AMC_2020_Level';

					var replace_query_id_clone = replace_query_id.replace('@replace', inspectionfinding.EASA_AMC_2020_Level);
					var replace_query_value_clone = replace_query_value.replace('@replace', inspectionfinding.EASA_AMC_2020_Level);

					if (atLeastOneLookupRequired == false) {
						sql_query = sql_query.replace('@EASA_AMC_2020_Level', replace_query_id_clone + ', ' + replace_query_value_clone);
						atLeastOneLookupRequired = true;
					} else {
						sql_query = sql_query.replace('@EASA_AMC_2020_Level', ', ' + replace_query_id_clone + ', ' + replace_query_value_clone);
					}//end if (atLeastOneLookupRequired == false)

					//replace @tablename with the tablename
					sql_query = sql_query.replace(/@tablename/g, tablename);

				} else {
					sql_query = sql_query.replace('@EASA_AMC_2020_Level', '');
				}

				if (atLeastOneLookupRequired == true) {
					var promises_inner = [];
					//prepare a promise
					var prom = new Promise((resolved, rejected) => {
						db.sequelize.query(
							sql_query,
							{
								type: db.sequelize.QueryTypes.SELECT,
								useMaster: false
							})
							.then((res) => {
								if (res.length > 0) {
									var error_message = '';

									if (inspectionfinding.Finding_Type != null) {
										if (inspectionfinding.Finding_Type.trim() != '') {
											if (res[0].Finding_Type_ID != null) {
												delete inspectionfinding.Finding_Type;
												inspectionfinding.Finding_Type_ID = res[0].Finding_Type_ID;
												inspectionfinding.Finding_Type = res[0].Finding_Type;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Finding_Type';
												} else {
													error_message = 'Finding_Type';
												}

											}
										}

									}

									if (inspectionfinding.Part_Criticity != null) {
										if (inspectionfinding.Part_Criticity.trim() != '') {
											if (res[0].Part_Criticity_ID != null) {
												delete inspectionfinding.Part_Criticity;
												inspectionfinding.Part_Criticity_ID = res[0].Part_Criticity_ID;
												inspectionfinding.Part_Criticity = res[0].Part_Criticity;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Part_Criticity';
												} else {
													error_message = 'Part_Criticity';
												}

											}
										}

									}

									if (inspectionfinding.Repair_Type != null) {
										if (inspectionfinding.Repair_Type.trim() != '') {
											if (res[0].Repair_Type_ID != null) {
												delete inspectionfinding.Repair_Type;
												inspectionfinding.Repair_Type_ID = res[0].Repair_Type_ID;
												inspectionfinding.Repair_Type = res[0].Repair_Type;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Repair_Type';
												} else {
													error_message = 'Repair_Type';
												}

											}
										}

									}

									if (inspectionfinding.Finding_Category != null) {
										if (inspectionfinding.Finding_Category.trim() != '') {
											if (res[0].Finding_Category_ID != null) {
												delete inspectionfinding.Finding_Category;
												inspectionfinding.Finding_Category_ID = res[0].Finding_Category_ID;
												inspectionfinding.Finding_Category = res[0].Finding_Category;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Finding_Category';
												} else {
													error_message = 'Finding_Category';
												}

											}
										}

									}

									if (inspectionfinding.Environment_Category != null) {
										if (inspectionfinding.Environment_Category.trim() != '') {
											if (res[0].Environment_Category_ID != null) {
												delete inspectionfinding.Environment_Category;
												inspectionfinding.Environment_Category_ID = res[0].Environment_Category_ID;
												inspectionfinding.Environment_Category = res[0].Environment_Category;
											} else {
												if (error_message != '') {
													error_message = error_message + ', Environment_Category';
												} else {
													error_message = 'Environment_Category';
												}

											}
										}

									}

									if (inspectionfinding.EASA_AMC_2020_Level != null) {
										if (res[0].EASA_AMC_2020_Level_ID != null) {
											delete inspectionfinding.EASA_AMC_2020_Level;
											inspectionfinding.EASA_AMC_2020_Level_ID = res[0].EASA_AMC_2020_Level_ID;
											inspectionfinding.EASA_AMC_2020_Level = res[0].EASA_AMC_2020_Level;
										} else {
											if (error_message != '') {
												error_message = error_message + ', EASA_AMC_2020_Level';
											} else {
												error_message = 'EASA_AMC_2020_Level';
											}

										}

									}

									if (error_message.length <= 0) {//no error is found
										resolved(inspectionfinding);
									} else {//error is found
										inspectionfinding.message = error_message + ' value(s) cannot be found in the database.';
										resolved(inspectionfinding);
									}
								} else {
									inspectionfinding.message = 'Referenced values (Finding_Type, Part_Criticity, Repair_Type, Finding_Category, Environment_Category, EASA_AMC_2020_Level) cannot be found in the database.';
									resolved(inspectionfinding);
								}
							})
							.catch((error) => {
								console.log(error);
								rejected(error);
							});
					});
					promises_inner.push(prom);

					//execute all the promise
					if (promises_inner.length > 0) {
						//console.log('promises_inner.length: '+promises_inner.length);
						return Promise.all(promises_inner)
							.then(function (ress) {
								resolve(ress[0]);
							}).catch((errors) => {
								reject(errors);
							}); //end return Promise.all(promises_inner)
					} else {
						resolve(inspectionfinding);
					}
				} else {
					resolve(inspectionfinding);
				}

			} catch (error) {
				console.error(error);
				resolve(error);
			}

		});

	}//end performInspectionFindingLookup
}

module.exports = InspectionRequest;
