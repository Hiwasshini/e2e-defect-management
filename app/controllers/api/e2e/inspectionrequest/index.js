'use strict';

const InspectionRequest = require('./lib');

const inspectionrequest = new InspectionRequest();
const classfilepath = 'app/controllers/api/e2e/inspectionrequest/index.js';
const appRoot = require('app-root-path');
const db      = require(`${appRoot}/app/models`);
/**
 * @swagger
 * definitions:
 *   InspectionRequest:
 *     type: object
 *     required:
 *       - inspectionrequestid
 *       - inspectionrequestvalue
 *     properties:
 *       inspectionrequestid:
 *         type: integer
 *       inspectionrequestvalue:
 *         type: string
 */


module.exports = (app) => {
  /**
   * @swagger
   * /api/e2e/inspectionrequest:
   *   post:
   *     summary: Add a new Inspection Request and other objects
   *     description: Add a new Inspection Request as a JSON object
   *     tags:
   *       - InspectionRequest
   *     produces:
   *       - application/json
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Request and other object that needs to be added to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionRequest"
   *     responses:
   *       200:
   *         description: "successful operation"
   */
  app.post('/api/e2e/inspectionrequest', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
       //extract the transaction ID
       var transactionid = t.id;
       console.log('('+ transactionid +'): entered POST /api/e2e/inspectionrequest');
 
      return(
        inspectionrequest
              .add(req.body,t,transactionid)
              .then((data) => {
                  t.commit();
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionrequest: transaction committed');
                  console.log('('+ transactionid +'): POST /api/e2e/inspectionrequest: returned result');
                  res.send(data);
              })
              .catch((error) => {
                  t.rollback();
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionrequest: rollback');
                  console.error('('+ transactionid +'): POST /api/e2e/inspectionrequest: error: ' + error);
                  res.status(400).send(error);
              })
      )
    });//end transaction

  });//end app.post

  /**
   * @swagger
   * /api/e2e/inspectionrequest/all:
   *   get:
   *     summary: List all Inspection Request (not recommended)
   *     description: List all Inspection Request as an JSON array
   *     tags:
   *       - InspectionRequest
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           type: array
   *           items:
   *             "$ref": "#/definitions/InspectionRequest"
   */
  app.get('/api/e2e/inspectionrequest/all', (req, res) => {
    db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered GET /api/e2e/inspectionrequest/all');

      inspectionrequest
      .list(t,transactionid)
      .then((data) => {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest/all: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest/all: returned result');
        res.send(data);
        
      })
      .catch((error) => {
        t.rollback();
        console.error('('+ transactionid +'): GET /api/e2e/inspectionrequest/all: rollback');
        console.error('('+ transactionid +'): GET /api/e2e/inspectionrequest/all: error: ' + error);
        res.status(400).send(error);
      });
    });//end transaction	  
  });//end app.get (all)

  /**
   * @swagger
   * /api/e2e/inspectionrequest:
   *   get:
   *     summary: Get a Inspection Request based on parameters
   *     description: Get a Inspection Request based on parameters.
   *     tags:
   *       - InspectionRequest
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: inspectionrequestid
   *         in: path
   *         description: "Inspection Request ID"
   *         required: true
   *         type: integer
   *       - name: inspectionrequestvalue
   *         in: path
   *         description: "Inspection Request Program"
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: "successful operation"
   *         schema:
   *           "$ref": "#/definitions/Application"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.get('/api/e2e/inspectionrequest', (req, res) => {
   // console.log(req.query.startdate);
   db.sequelize.transaction().then(function (t) {
     //extract the transaction ID
     var transactionid = t.id;
     console.log('('+ transactionid +'): entered GET /api/e2e/inspectionrequest');

     inspectionrequest
    .get(req.query,1,transactionid)
    .then((data) => {

      if (data <= 0) {
        t.rollback();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest: rollback');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest: no result');
        res.sendStatus(404);
      } else {
        t.commit();
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest: transaction committed');
        console.log('('+ transactionid +'): GET /api/e2e/inspectionrequest: returned result');
        res.send(data);
      }
    })
    .catch((error) => {
      t.rollback();
      console.error('('+ transactionid +'): GET /api/e2e/inspectionrequest: rollback');
      console.error('('+ transactionid +'): GET /api/e2e/inspectionrequest: error: ' + error);
      res.status(400).send(error);
    });
   });//end transaction	  
   
  });//end app.get
    
  /**
   * @swagger
   * /api/e2e/inspectionrequest/{id}:
   *   delete:
   *     summary: Removes a Inspection Request
   *     description: Removes a Inspection Request
   *     tags:
   *       - InspectionRequest
   *     parameters:
   *       - name: Inspection_Request_id
   *         in: path
   *         description: "Inspection Request id"
   *         required: true
   *         type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.delete('/api/e2e/inspectionrequest/:id', (req, res) => {
	  db.sequelize.transaction().then(function (t) {

    //extract the transaction ID
    var transactionid = t.id;
    console.log('('+ transactionid +'): entered DELETE /api/e2e/inspectionrequest');

		  return inspectionrequest
		      .remove(req.params.id,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: rollback');
                console.log('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: no result');
		            res.sendStatus(404);
		          } else {
              t.commit();
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: transaction committed');
              console.log('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: rollback');
            console.error('('+ transactionid +'): DELETE /api/e2e/inspectionrequest: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.delete

  /**
   * @swagger
   * /api/e2e/inspectionrequest:
   *   patch:
   *     summary: Update a Inspection Request
   *     description: Update a Inspection Request
   *     tags:
   *       - InspectionRequest
   *     parameters:
   *       - in: body
   *         name: body
   *         description: "Inspection Request object that needs to be updated to the database"
   *         required: true
   *         schema:
   *           "$ref": "#/definitions/InspectionRequest"
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: "successful operation"
   *       404:
   *         description: "not found"
   *       400:
   *         description: "bad request"
   */
  app.patch('/api/e2e/inspectionrequest', (req, res) => {
	  db.sequelize.transaction().then(function (t) {
      //extract the transaction ID
      var transactionid = t.id;
      console.log('('+ transactionid +'): entered PATCH /api/e2e/inspectionrequest');

		  return inspectionrequest
		      .update(req.body,t,transactionid)
		      .then((data) => {
		    	  if (data <= 0) {
                t.rollback();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: rollback');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: no result');
		            res.sendStatus(404);
		          } else {
                t.commit();
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: transaction committed');
                console.log('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: returned result');
		            res.send({
		              success : true
		            });
		          }
		      }).catch((error) => {
		    	  console.error(error);
            t.rollback();
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: rollback');
            console.error('('+ transactionid +'): PATCH /api/e2e/inspectionrequest: error: ' + error);
		    	  res.status(400).send(error);
		      });

	  });//end of transaction
	  
  });//end app.patch
  
  
};
