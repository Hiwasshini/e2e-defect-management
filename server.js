'use strict';

var http = require('http');
var https = require('https');
const redirectHttps = require('redirect-https');
const express    = require('express');
const swagger    = require('./lib/swagger');
const bodyParser = require('body-parser');
const db         = require('./app/models');
const config     = require('./lib/config')();
const app        = express();
// var Greenlock = require('greenlock');
const classfilepath = 'server.js';

//logging settings
var commonModule = require('./common/common.js');//common module that contains common functions
const uuidv1 = require('uuid/v1');
const path = require('path');
// const winston = require('winston');
// require('winston-daily-rotate-file');
// const logger = winston.createLogger({
//   transports: [
//     // colorize the output to the console
//     new (winston.transports.Console)({
//       timestamp: new Date(),
//       colorize: true,
//       level: 'info'
//     }),
//     new (winston.transports.DailyRotateFile)({
//       filename: path.join(config.log.directory,"hof-iot-api.log"),
//       datePattern: 'yyyy-MM-dd.',
//       prepend: true,
//       timestamp: new Date(),
//       level: config.log.level,
//       maxFiles: 30,
//       maxsize: 1024 * 1024 * 100
//     })
//   ]
// });


 const PROD = (process.env.ENCRYPT == 'true') || false;

/** Use when HTTPS is up (**DEPRECATED)**/
// const lex = require('greenlock-express').create({
// 	server: PROD ? 'https://acme-staging-v02.api.letsencrypt.org/directory' : 'staging',


// 	// If you wish to replace the default plugins, you may do so here
// 	//
// 	challenges: { 'http-01': require('le-challenge-fs').create({ webrootPath: '/tmp/acme-challenges' }) },
// 	store: require('le-store-certbot').create({ webrootPath: '/tmp/acme-challenges' }),

// 	// You probably wouldn't need to replace the default sni handler
// 	// See https://git.coolaj86.com/coolaj86/le-sni-auto if you think you do
// 	//, sni: require('le-sni-auto').create({})

// 	approveDomains: approveDomains
// });


/** Use when HTTPS is up **/
// var lex = Greenlock.create({
//   version: 'draft-12'
// , server: 'https://acme-v02.api.letsencrypt.org/directory'

//   // Use the approveDomains callback to set per-domain config
//   // (default: approve any domain that passes self-test of built-in challenges)
// , approveDomains: approveDomains

// , maintainerEmail: 'hof.airbus@airbus.com'

//   // If you wish to replace the default account and domain key storage plugin
// , store: require('le-store-fs').create({
//   webrootPath: '/tmp/acme-challenges'
//   })
// });

//  var http01 = require('le-challenge-fs').create({ webrootPath: '/tmp/acme-challenges' });
// function approveDomains(opts, certs, cb) {
//   // This is where you check your database and associated
//   // email addresses with domains and agreements and such

//   // If you wish to replace the default challenge plugin, you may do so here
//   opts.challenges = { 'http-01': http01 };

//   // The domains being approved for the first time are listed in opts.domains
//   // Certs being renewed are listed in certs.altnames
//   if (certs) {
//     opts.domains = [config.ssl.domains];
//   }
//   else {
//     opts.email = config.ssl.email;
//     opts.agreeTos = true;
//   }

//   // NOTE: you can also change other options such as `challengeType` and `challenge`
//   // opts.challengeType = 'http-01';
//   // opts.challenge = require('le-challenge-fs').create({});

//   cb(null, { options: opts, certs: certs });
// }

app.set('view engine', 'html');
app.set('views', 'public');
app.set('port', config.api.port);

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

/** Use when HTTPS is up**/
//handles acme-challenge and redirects to https
// http.createServer(lex.middleware(redirectHttps())).listen(80, function () {
// 	  console.log("Listening for ACME http-01 challenges on", this.address());
// }).on('error', function () {console.log});

// /** Use when HTTPS is up**/
// app.server = https.createServer(lex.httpsOptions, lex.middleware(app)).listen(443, function () {
//   console.log("Listening for ACME tls-sni-01 challenges and serve app on", this.address());
// });

/** Disable when HTTPS is up**/
//app.server = http.createServer(app);

// var io = require('socket.io').listen(app.server);

// database
// db.sequelize.sync({ force : config.db.wipe }).then(() => {
//   console.log('Database synced' +  // eslint-disable-line no-console
//     `${config.db.wipe ? ' - data it\'s wiped & schema recreated' : ''}`);
// });

// body parser
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({
  limit: '100mb',
  extended : true
}));

// enable CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, api_key, Authorization, Referer');
  next();
});

// init swagger
// if (config.environment === 'local' || config.environment === 'dev') {
//   swagger(app);
// }

// init server
var server = app.listen(config.api.port, () => {
   console.log(`listening on port ${config.api.port}`); // eslint-disable-line no-console
 });

 
// load API routes
require('./app/controllers')(app);

// global.logger = logger;
global.uuidv1 = uuidv1;

//const sls = require('serverless-http');
//
//const handler = sls(app);
//
//module.exports.handler = async (event,context)=>{
//  console.log("events in app js: =========================")
//  console.log(event);
//  
//  console.log("context in app js: ========================")
//  console.log(context);
//
//	// if(event.Records!=null)
//	// {
//	// 	console.log(event.Records);
//	// 	var params = event;
//  //   var queue = params.Records[0].eventSourceARN
//  //   queue = queue.split(":");
//  //   params.Records[0].Queue = queue[queue.length-1];
//	// 	console.log(params);
//	// }
//
//   context.callbackWaitsForEmptyEventLoop = false;
//  return await handler(event, context);
//};
