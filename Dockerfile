FROM node:boron

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production
# Bundle app source

#COPY ./node_modules/ node_modules/
#RUN ls -la /usr/src/app/node_modules/*

COPY . .

ADD node_modules .

RUN ["chmod", "+x", "/usr/src/app/scripts/start.sh"]

EXPOSE 80 443
CMD [ "npm", "start" ]