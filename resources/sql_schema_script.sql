CREATE TABLE `E2E_Airline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(200) NOT NULL COMMENT 'Airline name value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Airline';

CREATE TABLE `E2E_Area_Inspectability` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Area inspectability value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Enable to state if the inspection can be performed or not (action on the leader)';

CREATE TABLE `E2E_Assembly_Condition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Assembly Condition value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Store the Assembly_Condition';

CREATE TABLE `E2E_EASA_AMC_2020_Level` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` int(11) NOT NULL COMMENT 'EASA AMC 2020 level  value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Classification of the corrosion severity';

CREATE TABLE `E2E_Environment_Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Environment category  value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Classification of the area on aircraft depending on their risk to be exposed to corrosion';

CREATE TABLE `E2E_Equipment_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(255) NOT NULL COMMENT 'Equipment Type value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Equipment Type list';

CREATE TABLE `E2E_Finding_Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Finding category value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='Category of finding (TD = tear Down, ND = Natural Damage, AD = Artificial Damage, FA = Failure Analysis) where the aircraft has been operated';

CREATE TABLE `E2E_Finding_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(255) NOT NULL COMMENT 'Finding value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1 COMMENT='Findings Type';

CREATE TABLE `E2E_Inspection_Area` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Area_Description` longtext COMMENT 'Free enable to textually describe the area to inspect',
  `Area_Inspectability_ID` int(11) NOT NULL DEFAULT '1' COMMENT 'Enable to state if the inspection can be performed or not (action on the leader)',
  `Side_ID` int(11) DEFAULT NULL COMMENT 'Side of the inspection to be performed, for external or internal side',
  `Assembly_Condition_ID` int(11) NOT NULL DEFAULT '2' COMMENT 'Is the part is assembled or not, help to plan disassembling if necessary',
  `Inspection_Effort` varchar(255) DEFAULT NULL COMMENT 'Effort in terms of workload to manage priority or subcontrating',
  `Material` varchar(255) DEFAULT NULL COMMENT 'Material',
  `Inspection_Type_ID` int(11) NOT NULL DEFAULT '1' COMMENT 'Category of the inspection to be perform (GVI = Global_Visual_Inpsection, DVI = Detailed_Visual_Inspection)',
  `Inspection_Method_ID` int(11) NOT NULL DEFAULT '13' COMMENT 'Inspection method to be used',
  `Inspection_Procedure` longtext COMMENT 'Inspection procedure',
  `Inspection_Request_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Request table',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_area_inspectability_id` (`Area_Inspectability_ID`),
  KEY `idx_side_id` (`Side_ID`),
  KEY `idx_assembly_condition_id` (`Assembly_Condition_ID`),
  KEY `idx_inspection_effort` (`Inspection_Effort`),
  KEY `idx_material` (`Material`),
  KEY `idx_inspection_type_id` (`Inspection_Type_ID`),
  KEY `idx_inspection_method_id` (`Inspection_Method_ID`),
  KEY `idx_created_at` (`created_at`),
  KEY `idx_updated_at` (`updated_at`),
  KEY `idx_deleted_at` (`deleted_at`),
  KEY `idx_inspection_request_id` (`Inspection_Request_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Inspection Area';

CREATE TABLE `E2E_Inspection_Area__Finding_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Inspection_Area_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Area table',
  `Finding_Type_ID` int(11) NOT NULL COMMENT 'FK to Finding_Type table',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_inspection_area_id__finding_type_id` (`Inspection_Area_ID`,`Finding_Type_ID`),
  KEY `idx_inspection_area_id` (`Inspection_Area_ID`),
  KEY `idx_finding_type_id` (`Finding_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Inspection_Area and Finding_Type FK table';

CREATE TABLE `E2E_Inspection_Area_Cycle` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Aircraft_Model` varchar(100) NOT NULL COMMENT 'Store the Aircraft Model. i.e. A320-100',
  `MSN` int(11) NOT NULL COMMENT 'Aircraft Manufacturer Serial Number unique to the Aircraft Type',
  `Entry_Into_Service` datetime NOT NULL COMMENT 'Entry into Service DateTime',
  `Manufacturing_Date` datetime NOT NULL COMMENT 'Manufacturing Date',
  `Airline_ID` int(11) DEFAULT NULL COMMENT 'FK to Airline table',
  `Geographical_Area` varchar(255) DEFAULT NULL,
  `Flying_Cycle` int(11) NOT NULL COMMENT 'Flying Cycle',
  `Flying_Hours` int(11) NOT NULL COMMENT 'Flying Hours',
  `Inspection_Campaign` varchar(255) NOT NULL COMMENT 'Category of inspection campaing (fatigue phase, A, B or C check, date',
  `Campaign_Date` datetime NOT NULL COMMENT 'Campaign Date',
  `Equipment_Type_ID` int(11) NOT NULL COMMENT 'FK to Equipment_Type table',
  `Name_of_Equipment` varchar(255) DEFAULT NULL COMMENT 'Name of Equipment',
  `Equipment_Serial_Number` varchar(255) DEFAULT NULL COMMENT 'Equipment Serial Number',
  `Calibration_Due_Date` datetime DEFAULT NULL COMMENT 'Calibration Due Date',
  `Calibration_Specimen` longtext COMMENT 'Calibration Specimen',
  `Calibration_Procedure` longtext COMMENT 'Calibration Procedure',
  `Inspection_Area_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Area table',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_area_inspectability_id` (`Aircraft_Model`),
  KEY `idx_side_id` (`Flying_Cycle`),
  KEY `idx_assembly_condition_id` (`Flying_Hours`),
  KEY `idx_inspection_campaign` (`Inspection_Campaign`),
  KEY `idx_campaign_date` (`Campaign_Date`),
  KEY `idx_equipment_type_id` (`Equipment_Type_ID`),
  KEY `idx_inspection_area_id` (`Inspection_Area_ID`),
  KEY `idx_aircraft_model` (`Aircraft_Model`),
  KEY `idx_msn` (`MSN`),
  KEY `idx_entry_into_service` (`Entry_Into_Service`),
  KEY `idx_manufacturing_date` (`Manufacturing_Date`),
  KEY `idx_airline_id` (`Airline_ID`),
  KEY `idx_geographical_area` (`Geographical_Area`),
  KEY `idx_flying_cycle` (`Flying_Cycle`),
  KEY `idx_flying_hours` (`Flying_Hours`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Inspection Area Cycle';

CREATE TABLE `E2E_Inspection_Finding` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `IP_Name` varchar(255) NOT NULL DEFAULT 'IP' COMMENT 'Name',
  `Finding_Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Finding Date',
  `Flying_Cycle_Real` int(11) NOT NULL DEFAULT '0' COMMENT 'Real flying cycle, the one for the inspection (which miught be different from the request',
  `Finding_Category_ID` int(11) NOT NULL DEFAULT '1' COMMENT 'FK to Finding_Category table. Category of finding (TD = tear Down, ND = Natural Damage, AD = Artificial Damage, FA = Failure Analysis) where the aircraft has been operated',
  `Number_Spot` int(11) NOT NULL DEFAULT '1' COMMENT 'Here when 1 marker for several findings',
  `Length` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'length of the defect (mm)',
  `Width` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'width of the defect (mm)',
  `Surface` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Surface of the defect (mm2)',
  `Orientation` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT 'Orientation of the defect (especially for cracking) Angle (deg)',
  `Angle_Reference` varchar(255) DEFAULT NULL COMMENT 'Here to explain in which direction is the 0°',
  `Depth` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Depth mm\n',
  `Starting_Depth` decimal(10,2) NOT NULL COMMENT 'Depth mm',
  `Ending_Depth` decimal(10,2) NOT NULL COMMENT 'Depth mm',
  `Layer` varchar(255) DEFAULT NULL COMMENT 'Free text to explain where the defect is (in which part)',
  `Remaining_Thickness` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'What is the remaining thickness below corrosion mm',
  `Rotation_Angle` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT 'for rotating fastener type of defect Angle (deg)',
  `Comments` longtext COMMENT 'free text',
  `Repair_Type_ID` int(11) NOT NULL DEFAULT '1' COMMENT 'FK to Repair_Type table. Type of repair applied after the finding detection (probably not exhaustive today)',
  `Repair_Reference` varchar(255) DEFAULT NULL COMMENT 'Number refering to the repair method',
  `Part_Number` varchar(255) DEFAULT NULL COMMENT 'Part Number',
  `Part_Serial_Number` varchar(255) DEFAULT NULL COMMENT 'Part Serial Number',
  `Environment_Category_ID` int(11) DEFAULT NULL COMMENT 'FK to Environment_Category table. Classification of the area on aircraft depending on their risk to be exposed to corrosion',
  `EASA_AMC_2020_Level_ID` int(11) DEFAULT NULL COMMENT 'FK to EASA_AMC_2020_Level table. Classification of the corrosion severity',
  `SSI_Reference` varchar(255) DEFAULT NULL COMMENT 'Geometrical region of the aircraft, link to the maintenance plan',
  `RMT_File` varchar(255) DEFAULT NULL COMMENT 'techrequest or other tool dossier number (in-service entry)',
  `RDAS_Reference` varchar(255) DEFAULT NULL COMMENT 'RDAS Reference',
  `Corrosion_Material_1` varchar(255) DEFAULT NULL COMMENT 'For assembly, material n°1',
  `Corrosion_Material_2` varchar(255) DEFAULT NULL COMMENT 'For assembly, material n°1',
  `Corrosion_Fastener_Specification` varchar(255) DEFAULT NULL COMMENT 'For assembly, material of the fastener',
  `Part_Criticity_ID` int(11) DEFAULT NULL COMMENT 'FK to Part_Criticity table. Classification of the part criticity from a structure point of view is subjected to corrosion',
  `Inspection_Area_Cycle_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Area_Cycle table',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_ip_name` (`IP_Name`),
  KEY `idx_finding_date` (`Finding_Date`),
  KEY `idx_flying_cycle_real` (`Flying_Cycle_Real`),
  KEY `idx_finding_category_id` (`Finding_Category_ID`),
  KEY `idx_number_spot` (`Number_Spot`),
  KEY `idx_length` (`Length`),
  KEY `idx_width` (`Width`),
  KEY `idx_surface` (`Surface`),
  KEY `idx_orientation` (`Orientation`),
  KEY `idx_angle_reference` (`Angle_Reference`),
  KEY `idx_depth` (`Depth`),
  KEY `idx_starting_depth` (`Starting_Depth`),
  KEY `idx_ending_Depth` (`Ending_Depth`),
  KEY `idx_layer` (`Layer`),
  KEY `idx_remaining_thickness` (`Remaining_Thickness`),
  KEY `idx_rotation_angle` (`Rotation_Angle`),
  KEY `idx_repair_type_id` (`Repair_Type_ID`),
  KEY `idx_repair_reference` (`Repair_Reference`),
  KEY `idx_part_number` (`Part_Number`),
  KEY `idx_part_serial_number` (`Part_Serial_Number`),
  KEY `idx_environment_category_id` (`Environment_Category_ID`),
  KEY `idx_easa_amc_2020_level_id` (`EASA_AMC_2020_Level_ID`),
  KEY `idx_ssi_reference` (`SSI_Reference`),
  KEY `idx_rmt_file` (`RMT_File`),
  KEY `idx_rdas_reference` (`RDAS_Reference`),
  KEY `idx_corrosion_material_1` (`Corrosion_Material_1`),
  KEY `idx_corrosion_material_2` (`Corrosion_Material_2`),
  KEY `idx_corrosion_fastener_specification` (`Corrosion_Fastener_Specification`),
  KEY `idx_part_criticity_id` (`Part_Criticity_ID`),
  KEY `idx_inspection_area_cycle_id` (`Inspection_Area_Cycle_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Inspection Finding';

CREATE TABLE `E2E_Inspection_Finding__Finding_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Inspection_Finding_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Finding table',
  `Finding_Type_ID` int(11) NOT NULL COMMENT 'FK to Finding_Type table',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_inspection_finding_id__finding_type_id` (`Inspection_Finding_ID`,`Finding_Type_ID`),
  KEY `idx_inspection_finding_id` (`Inspection_Finding_ID`),
  KEY `idx_finding_type_id` (`Finding_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Inspection_Finding and Finding_Type FK table';

CREATE TABLE `E2E_Inspection_Finding_Attachment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Inspection_Finding_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Finding table',
  `FileKey` varchar(255) NOT NULL COMMENT 'Store the FileKey as listed in the S3 bucket',
  `Remarks` longtext COMMENT 'Store any additional remarks',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_inspection_finding_id` (`Inspection_Finding_ID`),
  KEY `idx_filekey` (`FileKey`),
  KEY `idx_created_at` (`created_at`),
  KEY `uni_filekey` (`FileKey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Inspection Finding Attachment';

CREATE TABLE `E2E_Inspection_Method` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(255) NOT NULL COMMENT 'Inspection method value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='Inspection method to be used';

CREATE TABLE `E2E_Inspection_Request` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Program` varchar(100) NOT NULL COMMENT 'Program',
  `Aircraft_Type` varchar(100) NOT NULL COMMENT 'Aircraft_type i.e. A320',
  `Applicable_MSN_Range` varchar(100) NOT NULL DEFAULT 'all' COMMENT 'MSN range concerned by the request, have the conf',
  `Priority` varchar(100) DEFAULT NULL COMMENT 'Help to define the priority in the request to perform',
  `Stress_Leader` varchar(100) DEFAULT NULL COMMENT 'Author of the request (for structural test & tear down',
  `NDI_Compiler` varchar(100) DEFAULT NULL COMMENT 'NDT engineer leader on the request',
  `Test_Leader` varchar(100) DEFAULT NULL COMMENT 'Responsible of the test (managing the stops, etc…)',
  `Requestor` varchar(100) NOT NULL COMMENT 'Owner of the request/ who asked for it',
  `NDT_Story_ID` int(11) DEFAULT NULL COMMENT 'Segregation of the users by story',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_program` (`Program`),
  KEY `idx_applicable_msn_range` (`Applicable_MSN_Range`),
  KEY `idx_priority` (`Priority`),
  KEY `idx_stress_leader` (`Stress_Leader`),
  KEY `idx_ndi_compiler` (`NDI_Compiler`),
  KEY `idx_test_leader` (`Test_Leader`),
  KEY `idx_requestor` (`Requestor`),
  KEY `idx_ndt_story_id` (`NDT_Story_ID`),
  KEY `idx_created_at` (`created_at`),
  KEY `idx_updated_at` (`updated_at`),
  KEY `idx_deleted_at` (`deleted_at`),
  KEY `idx_aircraft_type` (`Aircraft_Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Store the Inspection Request';

CREATE TABLE `E2E_Inspection_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(255) NOT NULL COMMENT 'Inspection type value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Category of the inspection to be perform (GVI = Global_Visual_Inpsection, DVI = Detailed_Visual_Inspection)';

CREATE TABLE `E2E_Log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Username` varchar(100) NOT NULL COMMENT 'The username that performed this request',
  `Inspection_Request_ID` int(11) NOT NULL COMMENT 'FK to Inspection_Request table',
  `Log_Type_ID` int(11) NOT NULL COMMENT 'FK to Log_Type table',
  `Log_Content` json DEFAULT NULL COMMENT 'Store the log content based on the Log_Type template',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_log_type_id` (`Log_Type_ID`),
  KEY `idx_created_at` (`created_at`),
  KEY `idx_username` (`Username`),
  KEY `idx_inspection_request_id` (`Inspection_Request_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Track event/activities by the user';

CREATE TABLE `E2E_Log_Display_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Name` varchar(200) NOT NULL COMMENT 'Store the Log Display Type Name',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Store the various log display types';

CREATE TABLE `E2E_Log_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Name` varchar(200) NOT NULL COMMENT 'Store the Log Type Name',
  `Alias` varchar(200) NOT NULL COMMENT 'Store the no space, no uppercase, no special character, no underscore of name',
  `Description` longtext COMMENT 'Description for log type',
  `Template` json DEFAULT NULL COMMENT 'Store all the recommended fields for the logging',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_alias` (`Alias`),
  KEY `idx_name` (`Name`),
  KEY `idx_alias` (`Alias`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Store the various log types';

CREATE TABLE `E2E_Log_Type__Log_Display_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Log_Type_ID` int(11) NOT NULL COMMENT 'FK to FS_List_Log_Type table',
  `Log_Display_Type_ID` int(11) NOT NULL COMMENT 'FK to FS_List_Log_Display_Type table',
  `LogOutputTemplate` longtext NOT NULL COMMENT 'Store the template to output the log values in a readable form',
  `IsForDisplay` int(1) NOT NULL DEFAULT '1' COMMENT 'Whether this log type can be allowed to be shown to requestor',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_log_type_id` (`Log_Type_ID`),
  KEY `idx_log_display_type_id` (`Log_Display_Type_ID`),
  KEY `idx_isfordisplay` (`IsForDisplay`),
  KEY `uni_log_type_id__log_display_type_id` (`Log_Type_ID`,`Log_Display_Type_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COMMENT='Store the log type display type';

CREATE TABLE `E2E_NDT_Story` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(255) NOT NULL COMMENT 'NDT story value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Segregation of the users by story';

CREATE TABLE `E2E_Part_Criticity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(10) NOT NULL COMMENT 'Part criticity value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Classification of the part criticity from a structure point of view is subjected to corrosion';

CREATE TABLE `E2E_Repair_Type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Repair type value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `uni_value` (`Value`),
  KEY `idx_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Type of repair applied after the finding detection (probably not exhaustive today)';

CREATE TABLE `E2E_Side` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK to this table',
  `Value` varchar(100) NOT NULL COMMENT 'Side value',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idx_value` (`Value`),
  KEY `uni_value` (`Value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Side of the inspection to be performed, for external or internal side';


INSERT e2e_defect_management_dev.`E2E_Airline` SELECT * FROM e2e_defect_management.`E2E_Airline`;
INSERT e2e_defect_management_dev.`E2E_Area_Inspectability` SELECT * FROM e2e_defect_management.`E2E_Area_Inspectability`;
INSERT e2e_defect_management_dev.`E2E_Assembly_Condition` SELECT * FROM e2e_defect_management.`E2E_Assembly_Condition`;
INSERT e2e_defect_management_dev.`E2E_EASA_AMC_2020_Level` SELECT * FROM e2e_defect_management.`E2E_EASA_AMC_2020_Level`;
INSERT e2e_defect_management_dev.`E2E_Environment_Category` SELECT * FROM e2e_defect_management.`E2E_Environment_Category`;
INSERT e2e_defect_management_dev.`E2E_Equipment_Type` SELECT * FROM e2e_defect_management.`E2E_Equipment_Type`;
INSERT e2e_defect_management_dev.`E2E_Finding_Category` SELECT * FROM e2e_defect_management.`E2E_Finding_Category`;
INSERT e2e_defect_management_dev.`E2E_Finding_Type` SELECT * FROM e2e_defect_management.`E2E_Finding_Type`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Area` SELECT * FROM e2e_defect_management.`E2E_Inspection_Area`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Area__Finding_Type` SELECT * FROM e2e_defect_management.`E2E_Inspection_Area__Finding_Type`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Area_Cycle` SELECT * FROM e2e_defect_management.`E2E_Inspection_Area_Cycle`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Finding` SELECT * FROM e2e_defect_management.`E2E_Inspection_Finding`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Finding__Finding_Type` SELECT * FROM e2e_defect_management.`E2E_Inspection_Finding__Finding_Type`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Finding_Attachment` SELECT * FROM e2e_defect_management.`E2E_Inspection_Finding_Attachment`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Method` SELECT * FROM e2e_defect_management.`E2E_Inspection_Method`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Request` SELECT * FROM e2e_defect_management.`E2E_Inspection_Request`;
INSERT e2e_defect_management_dev.`E2E_Inspection_Type` SELECT * FROM e2e_defect_management.`E2E_Inspection_Type`;
INSERT e2e_defect_management_dev.`E2E_Log` SELECT * FROM e2e_defect_management.`E2E_Log`;
INSERT e2e_defect_management_dev.`E2E_Log_Display_Type` SELECT * FROM e2e_defect_management.`E2E_Log_Display_Type`;
INSERT e2e_defect_management_dev.`E2E_Log_Type` SELECT * FROM e2e_defect_management.`E2E_Log_Type`;
INSERT e2e_defect_management_dev.`E2E_Log_Type__Log_Display_Type` SELECT * FROM e2e_defect_management.`E2E_Log_Type__Log_Display_Type`;
INSERT e2e_defect_management_dev.`E2E_NDT_Story` SELECT * FROM e2e_defect_management.`E2E_NDT_Story`;
INSERT e2e_defect_management_dev.`E2E_Part_Criticity` SELECT * FROM e2e_defect_management.`E2E_Part_Criticity`;
INSERT e2e_defect_management_dev.`E2E_Repair_Type` SELECT * FROM e2e_defect_management.`E2E_Repair_Type`;
INSERT e2e_defect_management_dev.`E2E_Side` SELECT * FROM e2e_defect_management.`E2E_Side`;