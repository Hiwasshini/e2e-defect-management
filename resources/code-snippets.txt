Code snippets for the total number of records. Take note how i formulate the SQL query. 
Line 107: 
	' Select SQL_CALC_FOUND_ROWS ' + sql_query + ' ORDER BY '+ param.ordercolumn + ' ' + param.order + ' LIMIT '+ param.index + ',' + param.row +';	SELECT FOUND_ROWS();';
**sql_query now does not contain a "SELECT" clause

Take a look and let me know if u need more explaination.

/**
   * Get a specific TEMPLATE Finding
   *
   * @param {Integer} templatefindingid - TEMPLATE_Finding ID
   * @param {Integer} templatefindingtitle - TEMPLATE_Finding JobCard Title
   * @param {Integer} templatefindingreferenceno - TEMPLATE_Finding Reference Number
   * @param {Integer} templatefindingclone - Clone template to become another template finding (0) or clone to become finding (1)
   * @param {integer} index - Starting index to retrieve the records (default:0)
   * @param {integer} row - Number of rows to retrieve from the starting index (default:30)
   * @param {String} order - Order the records either by Ascending or Descending (default:DESC)
   * @param {String} ordercolumn - Order the records by which column
   * @returns {Object}
   */
get(param,search) {
    return new Promise((resolve, reject) => {
    	if (param.index == null){
    		param.index = commonModule.getLimitDefaults().index;
    	}else{
    		if (param.index > 0){
    			param.index = param.index -1;
    		}
    	}

    	if (param.row == null){
    		param.row = commonModule.getLimitDefaults().row;
    	}
    	
    	if (param.order == null){
    		param.order = commonModule.getLimitDefaults().order;
    	}

    	if (param.ordercolumn == null){
    		param.ordercolumn = 'templatefindingid';
    	}
    	/*else{
    		var value = commonModule.getColumnName(param.ordercolumn);
    		if (value != null){
    			param.ordercolumn = value;
    		}else{
    			param.ordercolumn = 'templatefindingid';
    		}
    		
    	}*/
    	
    	var isTemplate = 'template';
    	if (param.templatefindingclone != null){
    		if (param.templatefindingclone == 1){ //clone clone to become finding
        		isTemplate = '';
        	}
    	}
    	
		//create the overall sql statements
		var sql_query =			
			'		    Title as ' + isTemplate + 'findingtitle, '+
			'		    ID as templatefindingid, ' +
			'		    Description as ' + isTemplate + 'findingdescription, '+
			'		    Raw_Data as ' + isTemplate + 'findingrawdata, '+
			'		    ReferenceNo as ' + isTemplate + 'findingreferenceno, '+
			'		    Remarks as ' + isTemplate + 'findingremarks'+
			'		    from TEMPLATE_Finding '+
			'		where '+
			'		deleted_at is null @templatefindingid @templatefindingtitle @templatefindingreferenceno ';
		
		var replace_query_id = ' and ID in (@replace)'; 
		var replace_query_templatefindingtitle = ' and (@replace) '; 
		var replace_query_templatefindingreferenceno = ' and ReferenceNo in (@replace)';
    	
		if (param.templatefindingid != null){
			sql_query = sql_query.replace('@templatefindingid', replace_query_id.replace('@replace',param.templatefindingid));
		}else{
			sql_query = sql_query.replace('@templatefindingid', '');
		}
		
		if (param.templatefindingtitle != null){
			var value = param.templatefindingtitle;
			if (value.split(',').length > 1){
				value = ' Title like \'%' + value.replace(/,/g, "%\' or  Title like \'%") + '%\'';
				sql_query = sql_query.replace('@templatefindingtitle', replace_query_templatefindingtitle.replace('@replace',value));
    		}else{
    			value = 'Title like \'%' + value + '%\'';
    			sql_query = sql_query.replace('@templatefindingtitle', replace_query_templatefindingtitle.replace('@replace',value));
    		} 
		}else{
			sql_query = sql_query.replace('@templatefindingtitle', '');
		}
		
		if (param.templatefindingreferenceno != null){
			var value = param.templatefindingreferenceno;
			if (value.split(',').length > 1){
				value = '\'' + value.replace(/,/g, "\',\'") + '\'';
    		}else{
    			value = '\'' + value + '\'';
    		}    
			sql_query = sql_query.replace('@templatefindingreferenceno', replace_query_templatefindingreferenceno.replace('@replace',value.toLowerCase()));
		}else{
			sql_query = sql_query.replace('@templatefindingreferenceno', '');
		}
		
		if (search == 0){ //proceed to return the sql string
			sql_query = ' Select ' + sql_query + ' ;';
			resolve(sql_query);      	    		
    	}else if (search == 1 || search == 2){ //proceed to search query with database
    		if (search == 1){//used for actual search requested by user
    			sql_query = ' Select SQL_CALC_FOUND_ROWS ' + sql_query + ' ORDER BY '+ param.ordercolumn + ' ' + param.order + ' LIMIT '+ param.index + ',' + param.row +';	SELECT FOUND_ROWS();';
    		}else if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
    			sql_query = ' Select ' + sql_query + ' ;';
    		}
    		
    		db.sequelize.query(
    				sql_query,
    		{
    			type: db.sequelize.QueryTypes.SELECT
    		})
    		.then((res) => {
    			var jsonobj = null;
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
    				jsonobj = res;
    				//resolve(res);
    			}
    			else if (search == 1){//used for actual search requested by user
    				//total number of rows from search result (without limit)
    				var total_rows = res[1][0]["FOUND_ROWS()"];
    				var loop_end_index;
    				
    				if (param.row < total_rows){
    					loop_end_index = param.row;
    				}else if (total_rows < param.row){
    					loop_end_index = total_rows;
    				}else{
    					loop_end_index = total_rows;
    				}
    				
    				var return_result = {};
    				
    				var final_result = [];
    				for (var i=0;i<loop_end_index;i++){
    					final_result.push(res[0][i]);
    				}
    				
    				return_result.Result = final_result;
    				
    				return_result.Actual_Total_Records_Found = total_rows;

    				
    				jsonobj = return_result.Result;
    				//resolve(return_result);
    				
    			}
    			
    			//put each finding into a finding array
	        	var counter = -1;
	        	jsonobj.map(a =>{
	        		counter++;
	        		var templatefindingplaceholder = {};
	        		
	        		if (param.templatefindingclone != null){
	            		if (param.templatefindingclone == 0 ){
	            			templatefindingplaceholder.templatefinding = [];
	            		}else if (param.templatefindingclone == 1){
	            			templatefindingplaceholder.finding = [];
	            		}else{
	            			templatefindingplaceholder.templatefinding = [];
	            		}
	            	}else{
	            		templatefindingplaceholder.templatefinding = [];
	            	}
	        		
	        		    	    	    	        		
	        		if (a.childtemplatefinding != null){
	        			var inner_counter = -1;
	        			a.childtemplatefinding.map(b =>{
	        				inner_counter++;
	        				var templatefindingplaceholder_inner = {};
	        				templatefindingplaceholder_inner.finding = [];
	        				templatefindingplaceholder_inner.finding.push(b);
	        				a.childtemplatefinding[inner_counter] = templatefindingplaceholder_inner;	  
	        			});
	        		}
	        		if (param.templatefindingclone != null){
	            		if ( (param.templatefindingclone == 0 || param.templatefindingclone == 1) && search != 2){
	            			delete a.templatefindingid;
	            		}
	        		}
	        		
	        		//templatefindingplaceholder.templatefinding.push(a);
	        		if (param.templatefindingclone != null){
	            		if (param.templatefindingclone == 0 ){
	            			templatefindingplaceholder.templatefinding.push(a);
	            		}else if (param.templatefindingclone == 1){
	            			templatefindingplaceholder.finding.push(a);
	            		}else{
	            			templatefindingplaceholder.templatefinding.push(a);
	            		}
	            	}else{
	            		templatefindingplaceholder.templatefinding.push(a);
	            	}
	        		
	        		jsonobj[counter] = templatefindingplaceholder;	  
	    		}).join(",");
    			
    			if (search == 2){//for internal class search retrieval. i.e. objects that need to be retrieved and return as a json string rather than sql string for joining
	        		resolve(jsonobj);
    			}else if (search == 1){
    				return_result.Result = jsonobj;
    				resolve(return_result);
    			}
    							
    		})
    		.catch((error) => {
    			console.log(error);
    			reject(error);
    		}); 
    	}//end if (search == 0)
    		    	
    });//end return new Promise((resolve, reject)	    	

  }