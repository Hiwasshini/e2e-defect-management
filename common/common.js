const classfilepath = 'common/common.js';
const appRoot = require('app-root-path');
const moment = require('moment');
const db      = require(`${appRoot}/app/models`);
const Promise = require('bluebird');
/**
 * Retrieve the object details
 */
module.exports = {
		assetToString: function(asset) {
			var append = "";
			if (asset.Name != null){
				append = append + "Name: " + asset.Name + ";";
			}
			if (asset.SerialNo != null){
				append = append + "SerialNo: " + asset.SerialNo + ";";
			}
			if (asset.Description != null){
				append = append + "Description: " + asset.Description + ";";
			}
			if (asset.Owner_ID != null){
				append = append + "Owner_ID: " + asset.Owner_ID + ";";
			}
			console.log('testing: '+append);
			return append;
		},

	deviceToString: function(asset) {

		var append = "";
		if (asset.SigfoxID != null){
			append = append + "SigfoxID: " + asset.SigfoxID + ";";
		}
		if (asset.Asset_ID != null){
			append = append + "Asset_ID: " + asset.Asset_ID + ";";
		}
		if (asset.Remarks != null){
			append = append + "Remarks: " + asset.Remarks + ";";
		}
		if (asset.RegisteredEmail != null){
			append = append + "RegisteredEmail: " + asset.RegisteredEmail + ";";
		}
		if (asset.AllocationDateIn != null){
			append = append + "AllocationDateIn: " + asset.AllocationDateIn + ";";
		}
		if (asset.AllocationDateOut != null){
			append = append + "AllocationDateOut: " + asset.AllocationDateOut + ";";
		}
		if (asset.IsActive != null){
			append = append + "IsActive: " + asset.IsActive + ";";
		}
		if (asset.Type_ID != null){
			append = append + "Type_ID: " + asset.Type_ID + ";";
		}
		return append;
	},
		
	 /**
	   * Check for duplicate Sigfox data
	   *
	   * @param {Object} sigfox - sigfox JSON object
	   */
		checkDuplicate: function(sigfox) {
	    return new Promise((resolve, reject) => {
	    	//convert the time to UTC  
	    	var d = new Date(0);
	    	d.setUTCSeconds(sigfox.time);
	    	
	    	//applicable if the bufferrelapsedtime is available
	    	var d_end = new Date(0);
	    	d_end.setUTCSeconds(sigfox.time);
	    		    	
	      	var final_query = 	'select a.ID as ID '+  
	      						'from haofu_db.Iot_Sigfox a,haofu_db.Iot_Device b '+
	      						'where a.Device_ID = b.ID and (a.Processed_data != null or a.Processed_data != \'\') ' ;
	      	var filter_1 = ' and sigfoxid=@sigfoxid ';
	      	var filter_2 = ' and `time`=@time ';
	      	var filter_3 = ' and raw_data=@rawdata ';
	      	var filter_4 = ' and `type`=@type ';
	      	var filter_5 = ' and `time` >= @starttime '; //cater for time range search
	      	var filter_6 = ' and `time` <= @endtime '; //cater for time range search
	      	var filter_final = ' and a.deleted_at is null and b.deleted_at is null;';
	      	
	      	if (sigfox.device != null){
	      		final_query = final_query + filter_1.replace('@sigfoxid','\''+sigfox.device+'\'');
	      	}
	      	
	      	if (sigfox.time != null){
	      		if (sigfox.elapsed_time != null){
	      			d.setSeconds(d.getSeconds() - sigfox.elapsed_time) ;
	      			d_end.setSeconds(d_end.getSeconds() - sigfox.elapsed_time) ;
	      		}
	      		if (sigfox.previous_elapsed_time != null){
	      			d.setSeconds(d.getSeconds() - sigfox.previous_elapsed_time) ;
	      			d_end.setSeconds(d_end.getSeconds() - sigfox.previous_elapsed_time) ;
	      		}
	      		
	      		//if there is a buffer elapsed time to be considered then we need to deduct from the time
	      		if (sigfox.bufferelapsedtime != null){
	      			console.log('before start: '+d.toISOString());
	    	      	console.log('before end: '+d_end.toISOString());
	    	      	
	      			d.setSeconds(d.getSeconds() - sigfox.bufferelapsedtime) ;
	      			d_end.setSeconds(d_end.getSeconds() + sigfox.bufferelapsedtime) ;
	      			
	      			console.log('after start: '+d.toISOString());
	    	      	console.log('after end: '+d_end.toISOString());
	      			
	      			final_query = final_query + filter_5.replace('@starttime','\''+d.toISOString()+'\'') + filter_6.replace('@endtime','\''+d_end.toISOString()+'\'');
	      		}else{
	      			final_query = final_query + filter_2.replace('@time','\''+d.toISOString()+'\'');
	      		}	      		
	      		
	      	}
	      	     	
	      	if (sigfox.data != null){
	      		final_query = final_query + filter_3.replace('@rawdata','\''+sigfox.data+'\'');
	      	}
	      	
	      	if (sigfox.type != null){
	      		final_query = final_query + filter_4.replace('@type','\''+sigfox.type+'\'');
	      	}
	      	
	      	final_query = final_query + filter_final;
	      	
	      	console.log(final_query);
	      	//console.log(d.toISOString());
	      	//console.log(d_end.toISOString());
      		db.sequelize.query(
	    			final_query,
	        {
//	            replacements: {
//	            	sigfoxID: sigfox.device,
//	            	time: d.toISOString(),            	
//	            	rawData: sigfox.data,
//	            	type: sigfox.type
////	            	,
////	            	starttime: d.toISOString(),
////	            	endtime: d_end.toISOString()    
//	            },
	            type: db.sequelize.QueryTypes.SELECT
	        })
	        .then((res) => {        	
	        	var json_obj = JSON.parse(JSON.stringify(res));
	        	if (json_obj.length > 1){
	        		console.log('found');
	        	}else{
	        		console.log('not found');
	        	}        	
	        	console.log(res);
			    	resolve(res);
			    })
			    .catch((error) => {
			    	console.log(error);
			    	reject(error);
			    });  
	      	  	
	    });
	  },
	  
//	// Deep extend recursive function
//	  deepExtend: function(firstObject, secondObject, tempObject) {
//	      tempObject = typeof tempObject === "undefined" ? {} : tempObject;
//
//	      // Flattens multidimensional arrays
//	      function flatten(array) {
//	          if (array.length) {
//	              return array.reduce(function(prev, cur) {
//	                  return prev.concat(cur)
//	              })
//	          }
//	      }
//	      // Iterates over both object, extends and merges them
//	      function iterateObjects(object1, object2) {
//	          var keys = Object.keys(object1);
//	          for (var i = 0; i < keys.length; i++) {
//	              var key = keys[i];
//	              var firstObjectValue = object1[key];
//	              var secondObjectValue = object2[key];
//
//	              if (firstObjectValue.constructor === Array) {
//	                  tempObject[key] = []
//	                  // Flatten arrays from each object
//	                  flatten(firstObjectValue) ? tempObject[key].push(flatten(firstObjectValue)) : null;
//	                  flatten(secondObjectValue) ? tempObject[key].push(flatten(secondObjectValue)) : null;
//
//	              } else if (firstObjectValue.constructor === Object) {
//	                  tempObject[key] = deepExtend(firstObjectValue, secondObjectValue, {})
//	              } else {
//	                  tempObject[key] = object2[key] || object1[key]
//	              }
//	          }
//	          return tempObject;
//	      }
//	      // Extend in both orders to find unique keys that appear only in one object
//	      tempObject = iterateObjects(firstObject, secondObject, tempObject);
//	      tempObject = iterateObjects(secondObject, firstObject, tempObject);
//
//	      return tempObject;
//	  }
	  
	  mergeDeep: function(...objects) {
		  const isObject = obj => obj && typeof obj === 'object';
		  console.log('MergeDeep start');
		  console.log(objects);
		  console.log('MergeDeep end');
		  return objects.reduce((prev, obj) => {
		    Object.keys(obj).forEach(key => {
		    	//console.log('key: '+key);
		    	//console.log(prev);
		      const pVal = prev[key];
		      const oVal = obj[key];
		      
		     // console.log(JSON.stringify(pVal));
		      //console.log(JSON.stringify(oVal));
		      
		      var isArray = false;
//		      console.log('previous: '+pVal);
//		      console.log('current: '+oVal);
		      
		      //test whether the value is a array type
		      try{
		    	  var arr_p = JSON.parse(pVal);
		    	  var arr_o = JSON.parse(oVal);
		    	  
//		    	  console.log(pVal);
//		    	  console.log(arr_p.length);
//		    	  console.log(oVal);
//		    	  console.log(arr_o.length);
//		    	  
//		    	  if (Array.isArray(arr_o)){
//		    		  console.log('current is array');
//		    	  }
//		    	  
//		    	  if (Array.isArray(arr_p)){
//		    		  console.log('previous is array');
//		    	  }
		    	  
		    	  if (Array.isArray(arr_p) && Array.isArray(arr_o)) {		    		  
		    		  isArray = true;
		    	  }
		      }catch (err){
		    	  //console.log('encounter error previous: ' + JSON.stringify(pVal));
		    	  //console.log('encounter error current: ' + JSON.stringify(oVal));
		    	  //console.log(err);
		    	  //ignore the error
		      }
		     
		      if (isArray == true) {
		    	  var arr_p = JSON.parse(pVal);
		    	  var arr_o = JSON.parse(oVal);
		    	  
		    	  prev[key] = JSON.stringify(arr_p.concat(...arr_o));
		        
		    	  //console.log('Merged: '+ JSON.stringify(prev[key]));
		      }
		      else if (isObject(pVal) && isObject(oVal) && isArray==false) {
		        prev[key] = mergeDeep(pVal, oVal);
		      }
		      else {
		        prev[key] = oVal;
		      }
		    });
		    
		    return prev;
		  }, {});
		},
		mergeDeeptest: function(prev, obj) {
			  const isObject = obj => obj && typeof obj === 'object';
			  //console.log(prev);
			  //console.log(obj);
			  
			  Object.keys(obj).forEach(key => {
			    	//console.log('key: '+key);
//			    	console.log(pre);
			      const pVal = prev[key];
			      const oVal = obj[key];
			      
			      //console.log(JSON.stringify(pVal));
			      //console.log(JSON.stringify(oVal));
			      
			      var isArray = false;
//			      console.log('previous: '+pVal);
//			      console.log('current: '+oVal);
			      
			      //test whether the value is a array type
			      try{
			    	  var arr_p = JSON.parse(pVal);
			    	  var arr_o = JSON.parse(oVal);
			    	  
//			    	  console.log(pVal);
//			    	  console.log(arr_p.length);
//			    	  console.log(oVal);
//			    	  console.log(arr_o.length);
//			    	  
//			    	  if (Array.isArray(arr_o)){
//			    		  console.log('current is array');
//			    	  }
//			    	  
//			    	  if (Array.isArray(arr_p)){
//			    		  console.log('previous is array');
//			    	  }
			    	  
			    	  if (Array.isArray(arr_p) && Array.isArray(arr_o)) {		    		  
			    		  isArray = true;
			    	  }
			      }catch (err){
			    	  //console.log('encounter error previous: ' + JSON.stringify(pVal));
			    	  //console.log('encounter error current: ' + JSON.stringify(oVal));
			    	  //console.log(err);
			    	  //ignore the error
			      }
			     
			      if (isArray == true) {
			    	  var arr_p = JSON.parse(pVal);
			    	  var arr_o = JSON.parse(oVal);
			    	  
			    	  prev[key] = JSON.stringify(arr_p.concat(...arr_o));
			        
			    	  //console.log('Merged: '+ JSON.stringify(prev[key]));
			      }
			      else if (isObject(pVal) && isObject(oVal) && isArray==false) {
			    	  prev[key] = mergeDeep(pVal, oVal);
			      }
			      else {
			    	  prev[key] = oVal;
			      }
			    });
			  
			  
//			  return objects.reduce((pre, obj) => {
//				  console.log(pre);
//				  console.log(obj);
//			    Object.keys(obj).forEach(key => {
////			    	console.log('key: '+key);
////			    	console.log(pre);
//			      const pVal = pre[key];
//			      const oVal = obj[key];
//			      
//			      console.log(JSON.stringify(pVal));
//			      console.log(JSON.stringify(oVal));
//			      
//			      var isArray = false;
////			      console.log('previous: '+pVal);
////			      console.log('current: '+oVal);
//			      
//			      //test whether the value is a array type
//			      try{
//			    	  var arr_p = JSON.parse(pVal);
//			    	  var arr_o = JSON.parse(oVal);
//			    	  
////			    	  console.log(pVal);
////			    	  console.log(arr_p.length);
////			    	  console.log(oVal);
////			    	  console.log(arr_o.length);
////			    	  
////			    	  if (Array.isArray(arr_o)){
////			    		  console.log('current is array');
////			    	  }
////			    	  
////			    	  if (Array.isArray(arr_p)){
////			    		  console.log('previous is array');
////			    	  }
//			    	  
//			    	  if (Array.isArray(arr_p) && Array.isArray(arr_o)) {		    		  
//			    		  isArray = true;
//			    	  }
//			      }catch (err){
//			    	  console.log('encounter error previous: ' + JSON.stringify(pVal));
//			    	  console.log('encounter error current: ' + JSON.stringify(oVal));
//			    	  console.log(err);
//			    	  //ignore the error
//			      }
//			     
//			      if (isArray == true) {
//			    	  var arr_p = JSON.parse(pVal);
//			    	  var arr_o = JSON.parse(oVal);
//			    	  
//			    	  prev[key] = JSON.stringify(arr_p.concat(...arr_o));
//			        
//			    	  console.log('Merged: '+ JSON.stringify(prev[key]));
//			      }
//			      else if (isObject(pVal) && isObject(oVal) && isArray==false) {
//			        pre[key] = mergeDeep(pVal, oVal);
//			      }
//			      else {
//			        pre[key] = oVal;
//			      }
//			    });
//			    
			  //console.log(JSON.stringify(prev));
			    return prev;
//			  }, {});
			},
		/**
		 * Get current date time in YYYY-MM-DD HH:mm:ss
		 */
		currentDateTime: function() {
			var datetime = new Date();
			datetime = moment(datetime).format('YYYY-MM-DD HH:mm:ss');
			
			return datetime;
		}//end currentDateTime	
}

